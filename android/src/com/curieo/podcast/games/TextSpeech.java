package com.curieo.podcast.games;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import com.curieo.podcast.games.AndroidLauncher;
import com.curieo.podcast.interfaces.TextSpeechCore;

import java.util.Locale;

public class TextSpeech implements TextSpeechCore {
    private TextToSpeech tts;
    private Context appContext;
    private int wins;

    public TextSpeech(Context appContext){
        this.appContext = appContext;
        tts=new TextToSpeech(appContext, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    tts.setLanguage(Locale.US);
                    tts.setSpeechRate((float) 0.75);
                }
            }
        });
    }


    @Override
    public void speak(String text) {
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void pressBack() {
        Log.v("text to interfaces", "here");

        ((AndroidLauncher) appContext).runOnUiThread(new Runnable() {
            public void run() {
                ((AndroidLauncher) appContext).onBackPressed();
            }
        });

    }

    @Override
    public void changeOrientation() {
        ((AndroidLauncher) appContext).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    @Override
    public int getWins() {
        wins = PreferenceManager.getDefaultSharedPreferences(appContext).getInt("wins", 0);
        return wins;
    }

    @Override
    public void writeWins(int wins) {
        PreferenceManager.getDefaultSharedPreferences(appContext).edit().putInt("wins", wins).apply();

    }


}
