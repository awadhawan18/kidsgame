package com.curieo.podcast.games;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.DisplayMetrics;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.curieo.podcast.ExerciseData;
import com.curieo.podcast.KidsGame;
import com.curieo.podcast.onboarding.activity.BlackboardActivity;
import com.google.firebase.FirebaseApp;

import static android.Manifest.permission.RECORD_AUDIO;

public class AndroidLauncher extends AndroidApplication {
	TextSpeech textToSpeech;
	SpeechText speechToText;
	ImageLoader imageLoader;
	KidsGame kidsGame;
    ExerciseData exercise;
    AndroidApplicationConfiguration config;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);

        config = new AndroidApplicationConfiguration();

        RetroHandler urlHandler = new RetroHandler(this);



        /*Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://blackboardradio.kebwfmajiq.ap-south-1.elasticbeanstalk.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        UrlService service = retrofit.create(UrlService.class);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("cat");
        arrayList.add("dog");

        ArrayList<Boolean> correctList = new ArrayList<>();
        correctList.add(true);
        correctList.add(false);

        service.savePost(3,1,arrayList,123,true,correctList).enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {

                if(response.isSuccessful()) {
                    Log.v("ExerciseData",response.body().toString());
                    Toast.makeText(getApplicationContext(),"Eksploooosion Succesful "+ response.body().toString(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                Log.e("Error", "Unable to submit post to API.");
            }
        });*/


        ActivityCompat.requestPermissions(this,
                new String[]{RECORD_AUDIO},
                1);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        textToSpeech = new TextSpeech(this);
        speechToText = new SpeechText(this);
        imageLoader = new ImageLoader(this);
        kidsGame = new KidsGame(textToSpeech, speechToText, imageLoader, urlHandler, width);

        speechToText.setGdx(kidsGame);
        initialize(kidsGame, config);




	}

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {

            case 1: {

                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(RECORD_AUDIO)) {
                            showMessageOKCancel("You need to allow access to this permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(new String[]{RECORD_AUDIO},
                                                        1);

                                            }
                                        }
                                    }, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                        }
                                    });

                        }
                    }
                }

            }
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener, DialogInterface.OnClickListener cancelListener) {
        new AlertDialog.Builder(AndroidLauncher.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", cancelListener)
                .create()
                .show();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Closing Activity")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getApplicationContext(), BlackboardActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }


}
