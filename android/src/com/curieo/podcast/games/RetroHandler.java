package com.curieo.podcast.games;


import android.content.Context;
import android.util.Log;

import com.curieo.podcast.ExerciseData;
import com.curieo.podcast.UrlService;
import com.curieo.podcast.interfaces.UrlHandler;
import com.curieo.podcast.listeners.OnExerciseLoaded;
import com.curieo.podcast.onboarding.data.SessionManager;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroHandler implements UrlHandler {
    ExerciseData exercise;
    boolean exerciseDownloaded = false;
    public OnExerciseLoaded mOnExerciseLoadedListener;
    SessionManager sessionManager;
    private Context context;


    public RetroHandler(Context context) {
        this.context = context;
    }

    @Override
    public void callServer() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://blackboardradio.kebwfmajiq.ap-south-1.elasticbeanstalk.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        sessionManager = new SessionManager(context);
        UrlService service = retrofit.create(UrlService.class);
        Call<ExerciseData> call = service.getJsonData(sessionManager.getCurrentKid(), "json");

        call.enqueue(new Callback<ExerciseData>() {
            @Override
            public void onResponse(Call<ExerciseData> call, Response<ExerciseData> response) {
                Log.e("RetroHandler", "onResponse: " + new GsonBuilder().setPrettyPrinting().create().toJson(response));

                setExercise(response.body());

                if (mOnExerciseLoadedListener != null) {
                    mOnExerciseLoadedListener.setScreen();
                }

            }

            @Override
            public void onFailure(Call<ExerciseData> call, Throwable t) {
                // Log error here since request failed
                Log.e("Retro error", t.toString());
            }
        });

    }

    @Override
    public void postServer() {

    }

    @Override
    public void setOnExerciseLoadedListener(OnExerciseLoaded listener) {
        mOnExerciseLoadedListener = listener;
    }

    @Override
    public ExerciseData getExercise() {
        return exercise;
    }

    public void setExercise(ExerciseData object) {
        exercise = new ExerciseData(object);
        Log.v("instance", "yay");
        Log.v("object", exercise.toString());


    }
}
