package com.curieo.podcast.radio.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.curieo.podcast.R;
import com.curieo.podcast.radio.home.EpisodesActivity;
import com.curieo.podcast.radio.models.Channel;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class HorizontalViewAdapter extends RecyclerView.Adapter<HorizontalViewAdapter.MyViewHolder> {
    RequestOptions requestOptions;
    private List<Channel> channelList;
    private Context mContext;
    public HorizontalViewAdapter(Context mContext, List<Channel> channelList) {
        this.channelList = channelList;
        this.mContext = mContext;

        requestOptions = new RequestOptions();
        requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(30));

    }

    @Override
    public HorizontalViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_card, parent, false);
        return new HorizontalViewAdapter.MyViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(final HorizontalViewAdapter.MyViewHolder holder, final int position) {
        final Channel channel = channelList.get(position);
        holder.channelName.setText(channel.getName());

        Glide.with(mContext).load(channel.getChannelImageUrl()).apply(requestOptions).into(holder.channelImage);

        if (channel.getChannelSection() != null)
            //    holder.channelsection.setText(channel.getChannelSection().get(0));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, EpisodesActivity.class);
                intent.putExtra("id", channel.getID());
                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return channelList.size();

    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView channelName, channelsection;
        private ImageView channelImage;
        private Context mContext;

        public MyViewHolder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            channelName = itemView.findViewById(R.id.channel_name);
            channelImage = itemView.findViewById(R.id.channel_image);
            //      channelsection = itemView.findViewById(R.id.channel_category);


        }

    }

}
