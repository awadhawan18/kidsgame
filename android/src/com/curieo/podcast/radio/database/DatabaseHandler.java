package com.curieo.podcast.radio.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.curieo.podcast.radio.models.Channel;
import com.curieo.podcast.radio.models.EpisodeOld;
import com.curieo.podcast.radio.models.Feed;
import com.curieo.podcast.radio.models.Tag;
import com.curieo.podcast.radio.record.OnDatabaseChangedListener;

import java.util.ArrayList;
import java.util.List;


public class DatabaseHandler extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "music";


    // table names
    private static final String TABLE_EPISODES = "episodes";
    private static final String TABLE_CHANNELS = "channels";
    private static final String TABLE_TAGS = "tags";

    // episodes Table Columns names
    private static final String EPISODE_ID = "episode_id";
    private static final String EPISODE_NAME = "episode_name";
    private static final String EPISODE_DESC = "episode_desc";
    private static final String EPISODE_URL = "episode_url";
    private static final String EPISODE_FAVOURITE = "favourite";
    private static final String EPISODE_CATEGORY = "episode_category";
    private static final String EPISODE_TAGS = "episode_tags";


    private static final String KEY_TRENDING = "trending";
    private static final String KEY_POPULAR = "popular";

    // Channel table columns
    private static final String CHANNEL_NAME = "channel_name";
    private static final String CHANNEL_ID = "channel_id";
    private static final String CHANNEL_IMAGE_URL = "channel_image_url";
    private static final String CHANNEL_TAGS = "channel_tag";
    private static final String CHANNEL_SECTION = "channel_category";
    private static final String CHANNEL_DESC = "channel_description";
    private static final String CHANNEL_LOCATION = "channel_location";
    private static final String SUBSCRIBE = "subscribe";
    private  static  final  String CHANNEL_GROUP = "channel_group";
    // Tags table columns
    private static final String TAG_NAME = "tag_name";
    private static final String TAG_ID = "tag_id";


    public static OnDatabaseChangedListener mDatabaseChangedListener;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static void setOnDatabaseChangedListener(OnDatabaseChangedListener onDatabaseChangedListener) {
        mDatabaseChangedListener = onDatabaseChangedListener;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CHANNELS_TABLE = "CREATE TABLE " + TABLE_CHANNELS + "("
                + CHANNEL_ID + " INTEGER PRIMARY KEY autoincrement," + CHANNEL_NAME + " TEXT,"
                + CHANNEL_IMAGE_URL + " TEXT," + CHANNEL_TAGS + " TEXT," + CHANNEL_SECTION + " TEXT, " + CHANNEL_GROUP + " TEXT, "
                + CHANNEL_DESC + " TEXT," + CHANNEL_LOCATION + " TEXT,"
                + SUBSCRIBE + " INTEGER DEFAULT 0" + ")";
        db.execSQL(CREATE_CHANNELS_TABLE);

        String CREATE_EPISODES_TABLE = "CREATE TABLE " + TABLE_EPISODES + "("
                + EPISODE_ID + " INTEGER PRIMARY KEY autoincrement," + EPISODE_NAME + " TEXT,"
                + EPISODE_DESC + " TEXT," + EPISODE_URL + " TEXT,"
                + EPISODE_FAVOURITE + " INTEGER DEFAULT 0, "
                //+ KEY_TRENDING + " INTEGER DEFAULT 0, " + KEY_POPULAR + " INTEGER DEFAULT 0, "
                + EPISODE_CATEGORY + " TEXT," + EPISODE_TAGS + " TEXT,"
                + CHANNEL_ID + " INT, "
                + "FOREIGN KEY(" + CHANNEL_ID + ") REFERENCES "
                + TABLE_CHANNELS + "(episode_id) " + ")";
        db.execSQL(CREATE_EPISODES_TABLE);

        String CREATE_TAGS_TABLE = "CREATE TABLE " + TABLE_TAGS + "("
                + TAG_ID + " INTEGER PRIMARY KEY autoincrement," + TAG_NAME + " TEXT"
                + ")";
        db.execSQL(CREATE_TAGS_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EPISODES);

        // Create tables again
        onCreate(db);
    }

    // Adding new episode
    public void addEpisode(Feed episode) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(EPISODE_NAME, episode.getTitle()); // episode Name
        values.put(EPISODE_URL, episode.getPodcastUrl()); // episode url
        values.put(EPISODE_FAVOURITE, episode.isFavourite());
        values.put(EPISODE_TAGS, episode.getStringTags());
        values.put(EPISODE_CATEGORY, episode.getEpisodeCategory());
        values.put(EPISODE_DESC, episode.getDescription());
        values.put(CHANNEL_ID, episode.getChannel().getID());

        // Inserting Row
        db.insert(TABLE_EPISODES, null, values);
        if (mDatabaseChangedListener != null) {
            mDatabaseChangedListener.onEpisodeUpdated();
        }
        db.close(); // Closing database connection
    }

    public void removeEpisodeWithId(int id) {
        SQLiteDatabase db = getWritableDatabase();
        String[] whereArgs = {String.valueOf(id)};
        db.delete(TABLE_EPISODES, "episode_id=?", whereArgs);
    }

    public List<Integer> getFavouriteEpisodeIds() {
        List<Integer> episodeList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EPISODES + " WHERE " + EPISODE_FAVOURITE + " = " + 1;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Integer episode = Integer.valueOf(cursor.getString(0));

                episodeList.add(episode);
            } while (cursor.moveToNext());
        }

        return episodeList;
    }

    public Feed getFavouriteEpisodeAt(int position) {
        String selectQuery = "SELECT  * FROM " + TABLE_EPISODES + " WHERE " + EPISODE_FAVOURITE + " = " + 1;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToPosition(position)) {
            Feed episode = new Feed();
            episode.setTitle(cursor.getString(cursor.getColumnIndex(EPISODE_NAME)));
            episode.setDescription(cursor.getString(cursor.getColumnIndex(EPISODE_DESC)));
            episode.setPodcastUrl(cursor.getString(cursor.getColumnIndex(EPISODE_URL)));
            episode.setAsFavourite();
            return episode;
        }

        return null;
    }


    public void updateEpisode(Feed feed) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(EPISODE_NAME, feed.getTitle());
        values.put(EPISODE_DESC, feed.getTitle());
        values.put(EPISODE_URL, feed.getPodcastUrl());
        values.put(EPISODE_FAVOURITE, feed.isFavourite());
        values.put(EPISODE_CATEGORY, feed.getEpisodeCategory());
        values.put(EPISODE_TAGS, feed.getStringTags());
        values.put(CHANNEL_ID, feed.getChannel().getID());
        // updating row
        db.update(TABLE_EPISODES, values, EPISODE_ID + " = ?",
                new String[]{String.valueOf(feed.getId())});

        if (mDatabaseChangedListener != null) {
            mDatabaseChangedListener.onEpisodeUpdated();
        }

    }

    public int getFavouriteEpisodeCount() {
        String countQuery = "SELECT  * FROM " + TABLE_EPISODES + " WHERE " + EPISODE_FAVOURITE + " = " + 1;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();
        return count;
    }

    void addChannel(Channel channel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CHANNEL_ID, channel.getID());
        values.put(CHANNEL_NAME, channel.getName());
        values.put(CHANNEL_IMAGE_URL, channel.getChannelImageUrl());
        values.put(CHANNEL_DESC, channel.getDescription());
        values.put(CHANNEL_TAGS, channel.getStringTags());
        values.put(CHANNEL_SECTION, channel.getStringSection());
        values.put(CHANNEL_GROUP, channel.getStringGroup());
        values.put(CHANNEL_LOCATION, channel.getLocation());
        values.put(SUBSCRIBE, channel.isSubscribed());
        // Inserting Row
        db.insert(TABLE_CHANNELS, null, values);
        db.close(); // Closing database connection
    }

    void addTag(Tag tag) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TAG_NAME, tag.getTagName());

        // Inserting Row
        db.insert(TABLE_TAGS, null, values);
        db.close(); // Closing database connection
    }

    Feed getEpisode(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_EPISODES, new String[]{EPISODE_ID,
                        EPISODE_NAME, EPISODE_URL, EPISODE_FAVOURITE}, EPISODE_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Feed episode = new Feed();

        episode.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(EPISODE_ID))));
        episode.setDescription(cursor.getString(cursor.getColumnIndex(EPISODE_DESC)));
        episode.favourite(Integer.parseInt(cursor.getString(cursor.getColumnIndex(EPISODE_FAVOURITE))));
        episode.setPodcastUrl(cursor.getString(cursor.getColumnIndex(EPISODE_URL)));
        episode.setEpisodeCategory(cursor.getString(cursor.getColumnIndex(EPISODE_CATEGORY)));
        episode.setTitle(cursor.getString(cursor.getColumnIndex(EPISODE_NAME)));
        return episode;

    }

    Channel getChannel(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CHANNELS, new String[]{CHANNEL_ID,
                        CHANNEL_NAME, CHANNEL_IMAGE_URL, CHANNEL_TAGS, CHANNEL_SECTION, CHANNEL_DESC, CHANNEL_LOCATION, SUBSCRIBE}, CHANNEL_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        return new Channel(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3),
                cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7),
                Integer.parseInt(cursor.getString(8)));
    }

    // Getting All episodes
    public List<EpisodeOld> getAllEpisodes() {
        List<EpisodeOld> episodeList = new ArrayList<EpisodeOld>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EPISODES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                EpisodeOld episode = new EpisodeOld(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1), cursor.getString(2), Integer.parseInt(cursor.getString(3)),
                        Integer.parseInt(cursor.getString(4)), Integer.parseInt(cursor.getString(5)));

                episodeList.add(episode);
            } while (cursor.moveToNext());
        }

        return episodeList;
    }

    public List<Channel> getAllChannels() {
        List<Channel> channelList = new ArrayList<Channel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CHANNELS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Channel channel = new Channel(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1), cursor.getString(2), cursor.getString(3),
                        cursor.getString(4), cursor.getString(5), cursor.getString(6),cursor.getString(7),
                        Integer.parseInt(cursor.getString(8)));

                channelList.add(channel);
            } while (cursor.moveToNext());
        }

        return channelList;
    }

    public List<Channel> getAllSubscribedChannel() {
        List<Channel> channelList = new ArrayList<Channel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CHANNELS + " WHERE " + SUBSCRIBE + " = " + 1;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Channel channel = new Channel(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1), cursor.getString(2), cursor.getString(3),
                        cursor.getString(4), cursor.getString(5), cursor.getString(6),cursor.getString(7),
                        Integer.parseInt(cursor.getString(7)));

                channelList.add(channel);
            } while (cursor.moveToNext());
        }

        return channelList;
    }

    public List<Tag> getAllTags() {
        List<Tag> tagList = new ArrayList<Tag>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_TAGS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Tag tag = new Tag(Integer.parseInt(cursor.getString(0)),cursor.getString(1));

                tagList.add(tag);
            } while (cursor.moveToNext());
        }

        return tagList;
    }

    public List<EpisodeOld> getAllEpisodesFromChannel(int id) {
        List<EpisodeOld> episodeList = new ArrayList<EpisodeOld>();

        String selectQuery = "SELECT  * FROM " + TABLE_EPISODES + " WHERE " + CHANNEL_ID + " = " + id;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                EpisodeOld episode = new EpisodeOld(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1), cursor.getString(2), Integer.parseInt(cursor.getString(3)),
                        Integer.parseInt(cursor.getString(4)), Integer.parseInt(cursor.getString(5)));

                episodeList.add(episode);
            } while (cursor.moveToNext());
        }

        return episodeList;
    }

    //returns favourite episodes list
    public List<EpisodeOld> getAllFavouriteEpisodes() {
        List<EpisodeOld> episodeList = new ArrayList<EpisodeOld>();

        String selectQuery = "SELECT  * FROM " + TABLE_EPISODES + " WHERE " + EPISODE_FAVOURITE + " = " + 1;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                EpisodeOld episode = new EpisodeOld(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1), cursor.getString(2), Integer.parseInt(cursor.getString(3)),
                        Integer.parseInt(cursor.getString(4)), Integer.parseInt(cursor.getString(5)));

                episodeList.add(episode);
            } while (cursor.moveToNext());
        }

        return episodeList;
    }

    public List<EpisodeOld> getAllTrendingEpisodes() {
        List<EpisodeOld> episodeList = new ArrayList<EpisodeOld>();

        String selectQuery = "SELECT  * FROM " + TABLE_EPISODES + " WHERE " + KEY_TRENDING + " = " + 1;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                EpisodeOld episode = new EpisodeOld(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1), cursor.getString(2), Integer.parseInt(cursor.getString(3)),
                        Integer.parseInt(cursor.getString(4)), Integer.parseInt(cursor.getString(5)));

                episodeList.add(episode);
            } while (cursor.moveToNext());
        }

        return episodeList;
    }

    public List<EpisodeOld> getAllPopularEpisodes() {
        List<EpisodeOld> episodeList = new ArrayList<EpisodeOld>();

        String selectQuery = "SELECT  * FROM " + TABLE_EPISODES + " WHERE " + KEY_POPULAR + " = " + 1;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                EpisodeOld episode = new EpisodeOld(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1), cursor.getString(2), Integer.parseInt(cursor.getString(3)),
                        Integer.parseInt(cursor.getString(4)), Integer.parseInt(cursor.getString(5)));

                episodeList.add(episode);
            } while (cursor.moveToNext());
        }

        return episodeList;
    }


    // Updating single episode
    public int updateEpisode(EpisodeOld episode) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(EPISODE_NAME, episode.getName());
        values.put(EPISODE_URL, episode.getUrl());
        values.put(EPISODE_FAVOURITE, episode.isFavourite());
        values.put(KEY_POPULAR, episode.isPopular());
        values.put(KEY_TRENDING, episode.isTrending());
        // updating row
        return db.update(TABLE_EPISODES, values, EPISODE_ID + " = ?",
                new String[]{String.valueOf(episode.getID())});
    }

    public void deteteSong(EpisodeOld episode) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_EPISODES, EPISODE_ID + " = ?",
                new String[]{String.valueOf(episode.getID())});
        db.close();
    }


    public long getEpisodesCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        long count = DatabaseUtils.queryNumEntries(db, TABLE_EPISODES);
        db.close();
        return count;
    }
}
