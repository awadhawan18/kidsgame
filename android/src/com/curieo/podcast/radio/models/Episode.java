package com.curieo.podcast.radio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Episode {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("channel_id")
    @Expose
    private Integer channelId;
    @SerializedName("channel")
    @Expose
    private Channel channel;
    @SerializedName("tags")
    @Expose
    private List<String> tags = null;
    @SerializedName("episode_category")
    @Expose
    private String episodeCategory;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("podcast_url")
    @Expose
    private String podcastUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getChannelId() {
        return channelId;
    }

    public void setChannelId(Integer channelId) {
        this.channelId = channelId;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getEpisodeCategory() {
        return episodeCategory;
    }

    public void setEpisodeCategory(String episodeCategory) {
        this.episodeCategory = episodeCategory;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPodcastUrl() {
        return podcastUrl;
    }

    public void setPodcastUrl(String podcastUrl) {
        this.podcastUrl = podcastUrl;
    }

}