package com.curieo.podcast.radio.models;

public class EpisodeOld {

    private int id;
    private int channel_id;
    private String name;
    private String url;
    private int favourite;
    private int trending;
    private int popular;

    // Empty constructor
    public EpisodeOld() {

    }

    public EpisodeOld(String name, String url) {
        this.name = name;
        this.url = url;
        favourite = 0;
        trending = 0;
        popular = 0;
    }

    public EpisodeOld(int id, String name, String url) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.favourite = 0;
        this.trending = 0;
        this.popular = 0;
    }

    public EpisodeOld(String name, String url, int channel_id) {
        this.channel_id = channel_id;
        this.name = name;
        this.url = url;
        this.favourite = 0;
        this.trending = 0;
        this.popular = 0;
    }

    public EpisodeOld(String name, String url, int channel_id, int favourite, int trending, int popular) {
        this.channel_id = channel_id;
        this.name = name;
        this.url = url;
        this.favourite = favourite;
        this.trending = trending;
        this.popular = popular;
    }

    public EpisodeOld(int id, String name, String url, int favourite, int trending, int popular) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.favourite = favourite;
        this.trending = trending;
        this.popular = popular;
    }

    public int getID() {
        return this.id;
    }

    // setting id
    public void setID(int id) {
        this.id = id;
    }

    public int getChannelID() {
        return this.channel_id;
    }

    public void setChannel_id(int id) {
        this.id = id;
    }

    // getting name
    public String getName() {
        return this.name;
    }

    // setting name
    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setFavourite(int val) {
        this.favourite = val;
    }

    public int isFavourite() {
        return this.favourite;
    }

    public void setTrending(int val) {
        this.trending = val;
    }

    public int isTrending() {
        return this.trending;
    }

    public void setPopular(int val) {
        this.popular = val;
    }

    public int isPopular() {
        return this.popular;
    }
}