package com.curieo.podcast.radio.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Channel implements Parcelable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("tags")
    @Expose
    private List<String> tags = null;


    @SerializedName("channel_section")
    @Expose
    private List<String> channelSection = null;


    @SerializedName("channel_group")
    @Expose
    private List<String> channelGroup = null;

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("channel_image_url")
    @Expose
    private String channelImageUrl;

    @SerializedName("location")
    @Expose
    private String location;

    private int subscribed;

    public final static Parcelable.Creator<Channel> CREATOR = new Parcelable.Creator<Channel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Channel createFromParcel(Parcel in) {
            return new Channel(in);
        }

        public Channel[] newArray(int size) {
            return (new Channel[size]);
        }

    };

    protected Channel(Parcel in) {
        this.id = ((int) in.readValue((int.class.getClassLoader())));
        this.tags = new ArrayList<>();
        this.channelSection = new ArrayList<>();
        this.channelGroup = new ArrayList<>();
        in.readList(this.tags, (java.lang.String.class.getClassLoader()));
        in.readList(this.channelSection, (java.lang.String.class.getClassLoader()));
        in.readList(this.channelGroup, (java.lang.String.class.getClassLoader()));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.channelImageUrl = ((String) in.readValue((String.class.getClassLoader())));
        this.location = ((String) in.readValue((String.class.getClassLoader())));
        this.subscribed = ((int) in.readValue((int.class.getClassLoader())));
    }


    public List<String> getChannelGroup() {
        return channelGroup;
    }

    public void setChannelGroup(List<String> channelGroup) {
        this.channelGroup = channelGroup;
    }

    public Channel() {

    }

    public Channel(String name, String image) {
        this.name = name;
        this.channelImageUrl = image;
        this.subscribed = 0;
    }

    public Channel(int id, String name, String image) {
        this.id = id;
        this.name = name;
        this.channelImageUrl = image;
        this.subscribed = 0;
    }

    public Channel(int id, String name, String image, String tags, int subscribed) {
        this.id = id;
        this.name = name;
        this.channelImageUrl = image;
        this.tags = Arrays.asList(tags.split("\\s*,\\s*"));
        this.subscribed = subscribed;
    }

    public Channel(String name, String image, String tags, int subscribed) {
        this.name = name;
        this.channelImageUrl = image;
        this.tags = Arrays.asList(tags.split("\\s*,\\s*"));
        this.subscribed = subscribed;
    }

    public Channel(int id, String channelImageUrl, String tags, String name,
                   String channelsection, String channelgroup, String description, String location, int subscribed) {
        super();
        this.id = id;
        this.tags = Arrays.asList(tags.split("\\s*,\\s*"));
        this.channelSection = Arrays.asList(channelsection.split("\\s*,\\s*"));
        this.channelGroup = Arrays.asList(channelgroup.split("\\s*,\\s*"));
        this.name = name;
        this.description = description;
        this.channelImageUrl = channelImageUrl;
        this.location = location;
        this.subscribed = subscribed;
    }

    public int getID() {
        return this.id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public List<String> getChannelSection() {
        return channelSection;
    }

    public String getStringSection() {
        StringBuilder sb = new StringBuilder();
        String loopDelim = "";

        for (String s : channelSection) {
            sb.append(loopDelim);
            sb.append(s);

            loopDelim = ",";
        }
        return sb.toString();
    }

    public  String getStringGroup(){
        StringBuilder sb = new StringBuilder();
        String loopDelim = "";

        for (String s : channelGroup) {
            sb.append(loopDelim);
            sb.append(s);

            loopDelim = ",";
        }
        return sb.toString();

    }

    public void setChannelSection(List<String> channelSection) {
        this.channelSection = channelSection;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChannelImageUrl() {
        return this.channelImageUrl;
    }

    public void setChannelImageUrl(String image) {
        this.channelImageUrl = image;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<String> getTags() {
        return this.tags;
    }

    public String getStringTags() {
        StringBuilder sb = new StringBuilder();
        String loopDelim = "";

        for (String s : tags) {
            sb.append(loopDelim);
            sb.append(s);

            loopDelim = ",";
        }
        return sb.toString();
    }

    public void setTags(String tags) {
        this.tags = Arrays.asList(tags.split("\\s*,\\s*"));
    }

    public void setSubscribed(int val) {
        this.subscribed = val;
    }

    public int isSubscribed() {
        return this.subscribed;
    }

    public void unsubscribe() {
        this.subscribed = 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeList(tags);
        dest.writeList(channelSection);
        dest.writeValue(name);
        dest.writeValue(description);
        dest.writeValue(channelImageUrl);
        dest.writeValue(location);
        dest.writeValue(subscribed);
    }

    public int describeContents() {
        return 0;
    }


}
