package com.curieo.podcast.radio.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Feed implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("channel")
    @Expose
    private Channel channel;
    @SerializedName("tags")
    @Expose
    private List<String> tags = null;
    @SerializedName("episode_category")
    @Expose
    private String episodeCategory;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("podcast_url")
    @Expose
    private String podcastUrl;
    private int favourite;




    public final static Parcelable.Creator<Feed> CREATOR = new Creator<Feed>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Feed createFromParcel(Parcel in) {
            return new Feed(in);
        }

        public Feed[] newArray(int size) {
            return (new Feed[size]);
        }

    };

    protected Feed(Parcel in) {
        this.id = ((int) in.readValue((int.class.getClassLoader())));
        this.channel = ((Channel) in.readValue((Channel.class.getClassLoader())));
        in.readList(this.tags, (java.lang.String.class.getClassLoader()));
        this.episodeCategory = ((String) in.readValue((String.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.podcastUrl = ((String) in.readValue((String.class.getClassLoader())));
        this.favourite = ((int) in.readValue((int.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public Feed() {
    }

    public Feed(int id, Channel channel, List<String> tags, String episodeCategory, String title, String description, String podcastUrl) {
        super();
        this.id = id;
        this.channel = channel;
        this.tags = tags;
        this.episodeCategory = episodeCategory;
        this.title = title;
        this.description = description;
        this.podcastUrl = podcastUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getStringTags() {
        StringBuilder sb = new StringBuilder();
        String loopDelim = "";

        for (String s : tags) {
            sb.append(loopDelim);
            sb.append(s);

            loopDelim = ",";
        }
        return sb.toString();
    }

    public String getEpisodeCategory() {
        return episodeCategory;
    }

    public void setEpisodeCategory(String episodeCategory) {
        this.episodeCategory = episodeCategory;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPodcastUrl() {
        return podcastUrl;
    }

    public void setPodcastUrl(String podcastUrl) {
        this.podcastUrl = podcastUrl;
    }

    public void setAsFavourite() {
        this.favourite = 1;
    }

    public void favourite(int i) {
        this.favourite = i;
    }

    public void removeFavourite() {
        this.favourite = 0;
    }

    public int isFavourite() {
        return this.favourite;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(channel);
        dest.writeList(tags);
        dest.writeValue(episodeCategory);
        dest.writeValue(title);
        dest.writeValue(description);
        dest.writeValue(podcastUrl);
        dest.writeValue(favourite);
    }

    public int describeContents() {
        return 0;
    }

}
