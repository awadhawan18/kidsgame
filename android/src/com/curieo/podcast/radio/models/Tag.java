package com.curieo.podcast.radio.models;

public class Tag {
    private String tagName;
    private int id;

    public Tag(int id, String tagName) {
        this.id = id;
        this.tagName = tagName;

    }

    public Tag(String tagName) {
        this.tagName = tagName;
    }

    public String getTagName() {
        return this.tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
