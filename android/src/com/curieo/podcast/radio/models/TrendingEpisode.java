package com.curieo.podcast.radio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrendingEpisode {

    @SerializedName("episode")
    @Expose
    private Episode episode;

    public Episode getEpisode() {
        return episode;
    }

    public void setEpisode(Episode episode) {
        this.episode = episode;
    }

}