package com.curieo.podcast.radio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abhi on 18-Apr-18.
 */

public class ActionResponse {

    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("user")
    @Expose
    private int user;
    @SerializedName("episode")
    @Expose
    private int episode;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getEpisode() {
        return episode;
    }

    public void setEpisode(int episode) {
        this.episode = episode;
    }

}
