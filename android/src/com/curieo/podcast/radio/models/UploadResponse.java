package com.curieo.podcast.radio.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UploadResponse {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("audio_url")
    @Expose
    private String audioUrl;
    @SerializedName("audio_name")
    @Expose
    private String audioName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("date_published")
    @Expose
    private String datePublished;
    @SerializedName("kid_id")
    @Expose
    private int kidId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public String getAudioName() {
        return audioName;
    }

    public void setAudioName(String audioName) {
        this.audioName = audioName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(String datePublished) {
        this.datePublished = datePublished;
    }

    public int getKidId() {
        return kidId;
    }

    public void setKidId(int kidId) {
        this.kidId = kidId;
    }

}