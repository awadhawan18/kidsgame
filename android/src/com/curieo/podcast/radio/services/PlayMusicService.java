package com.curieo.podcast.radio.services;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.RemoteViews;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.NotificationTarget;
import com.curieo.podcast.R;
import com.curieo.podcast.radio.CurrentStatus;
import com.curieo.podcast.radio.RadioActivity;

import java.io.IOException;

import static com.curieo.podcast.radio.CurrentStatus.currentEpisode;

public class PlayMusicService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener,
        AudioManager.OnAudioFocusChangeListener {
    private static String ACTION_PAUSE = "pause";
    private static String ACTION_Play = "play";
    private static String ACTION_RESUME = "resume";
    private static String ACTION_OPEN_PLAYER = "open_player";
    private static MediaPlayer mediaPlayer;
    private static int playbackPosition = 0;
    private static int mediaFileLengthInMilliseconds;
    private static Notification notification;
    private static NotificationManager nm;
    private static RemoteViews notificationView;
    private static String CHANNEL_ID = "Channel";
    private static NotificationChannel mChannel;
    private static AudioManager audioManager;
    private static TelephonyManager mgr;
    private static NotificationTarget notificationTarget;

    private final Handler handler = new Handler();

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onCreate() {  //initializing media player and notifications
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnCompletionListener(this);
        nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

        //notification channel needed for oreo and above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            mChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_ID, NotificationManager.IMPORTANCE_HIGH);
            nm.createNotificationChannel(mChannel);
            notification = new Notification.Builder(getApplicationContext(), CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_launcher_background)
                    .build();
        } else {
            notification = new Notification.Builder(getApplicationContext())
                    .setSmallIcon(R.drawable.ic_launcher_background)
                    .build();
        }

        notificationView = new RemoteViews(getPackageName(),
                R.layout.notification_layout);
        notificationView.setTextViewText(R.id.episode_name, "SONG NAME");
        notification.contentView = notificationView;
        notificationTarget = new NotificationTarget(
                this,
                50, 50,
                R.id.notifiation_image,
                notificationView,
                notification,
                1,
                null);


        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mBroadcastReceiver, new IntentFilter("Broadcast"));
        mgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if (mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        CurrentStatus.isServiceRunning = true;
        if (intent.getAction().equals(ACTION_PAUSE)) {
            pauseSong();
            publishPausePlayResult(getApplicationContext());
        } else if (intent.getAction().equals(ACTION_Play)) {
            if (currentEpisode != null) {
                CurrentStatus.currentSong = currentEpisode.getTitle();
                playNewSong(currentEpisode.getPodcastUrl());

            }

            publishPausePlayResult(getApplicationContext());
        } else if (intent.getAction().equals(ACTION_RESUME)) {
            resumeSong();
            publishPausePlayResult(getApplicationContext());
        }

        //start sticky means service will be explicity started and stopped
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //stopping the player when service is destroyed
        mediaPlayer.stop();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mBroadcastReceiver);
        if (mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        }
    }

    public void showPauseNotification() {
        Intent pauseIntent = new Intent(this, PlayMusicService.class);
        pauseIntent.setAction(ACTION_PAUSE);
        PendingIntent ppauseIntent = PendingIntent.getService(this, 0,
                pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        notificationView.setImageViewResource(R.id.pause_play_button, R.drawable.button_pause);
        notificationView.setTextViewText(R.id.episode_name, currentEpisode.getTitle());
        notification.flags = Notification.FLAG_ONGOING_EVENT;
        notificationView.setOnClickPendingIntent(R.id.pause_play_button,
                ppauseIntent);

        Intent notificationIntent = new Intent(this, RadioActivity.class);
        notificationIntent.setAction(ACTION_OPEN_PLAYER);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        if (currentEpisode.getChannel() != null) {
            Glide.with(getApplicationContext())
                    .asBitmap()
                    .load(currentEpisode.getChannel().getChannelImageUrl())
                    .into(notificationTarget);
        }


        notification.contentIntent = contentIntent;

        nm.notify(1, notification);

    }

    public void showPlayNotification() {

        Intent playIntent = new Intent(this, PlayMusicService.class);
        playIntent.setAction(ACTION_RESUME);
        PendingIntent pplayIntent = PendingIntent.getService(this, 0,
                playIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        notificationView.setImageViewResource(R.id.pause_play_button, R.drawable.button_play);
        notificationView.setTextViewText(R.id.episode_name, currentEpisode.getTitle());
        notificationView.setOnClickPendingIntent(R.id.pause_play_button,
                pplayIntent);

        Intent notificationIntent = new Intent(this, RadioActivity.class);
        notificationIntent.setAction(ACTION_OPEN_PLAYER);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        if (currentEpisode.getChannel() != null) {
            Glide.with(getApplicationContext())
                    .asBitmap()
                    .load(currentEpisode.getChannel().getChannelImageUrl())
                    .into(notificationTarget);
        }

        notification.contentIntent = contentIntent;
        notification.flags = Notification.FLAG_AUTO_CANCEL;
        nm.notify(1, notification);

    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        this.mediaPlayer.start();
        showPauseNotification();
        mediaFileLengthInMilliseconds = mediaPlayer.getDuration();
        primarySeekBarProgressUpdater();

    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        showPlayNotification();
    }

    private void primarySeekBarProgressUpdater() {
        publishSeekBarResult(getApplicationContext(), (int) (float) mediaPlayer.getCurrentPosition(), mediaFileLengthInMilliseconds);

        if (mediaPlayer.isPlaying()) {
            Runnable notification = new Runnable() {
                public void run() {
                    primarySeekBarProgressUpdater();
                }
            };
            handler.postDelayed(notification, 1000);
        }
    }

    public static void publishSeekBarResult(Context context, int progress, int length) {
        Intent intent = new Intent("Broadcast");
        intent.putExtra("INTENT_TYPE", "SEEKBAR_RESULT");
        intent.putExtra("FILE_LENGTH", length);
        intent.putExtra("PERCENTAGE", progress);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void publishFileLengthResult(Context context, int length) {
        Intent intent = new Intent("Broadcast");
        intent.putExtra("INTENT_TYPE", "FILE_LENGTH");
        intent.putExtra("LENGTH", length);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void publishPausePlayResult(Context context) {
        Intent intent = new Intent("Broadcast");
        intent.putExtra("INTENT_TYPE", "PAUSE_RESULT");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String intentType = intent.getStringExtra("INTENT_TYPE");
            if (intentType.equalsIgnoreCase("SEEK_TO")) {
                int percentage = intent.getIntExtra("PERCENTAGE", 0);
                playbackPosition = (mediaFileLengthInMilliseconds / 100) * percentage;
                mediaPlayer.seekTo(playbackPosition);
                mediaPlayer.start();

            } else if (intentType.equals("PAUSE_PLAY_RESULT")) {
                if (CurrentStatus.isSongPlaying) {
                    pauseSong();
                } else {
                    resumeSong();
                }
            }

        }
    };

    private void pauseSong() {
        if (mediaPlayer.isPlaying()) {
            CurrentStatus.isSongPlaying = false;
            showPlayNotification();
            mediaPlayer.pause();
            playbackPosition = mediaPlayer.getCurrentPosition();
        }

    }

    private void playNewSong(String url) {

        try {
            if (mediaPlayer != null) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
            }
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepareAsync();
            CurrentStatus.isSongPlaying = true;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void resumeSong() {
        if (!mediaPlayer.isPlaying()) {
            mediaPlayer.seekTo(playbackPosition);
            mediaPlayer.start();
            CurrentStatus.isSongPlaying = true;
            showPauseNotification();
            primarySeekBarProgressUpdater();

        }
    }


    @Override
    public void onAudioFocusChange(int focusChange) {
        if (focusChange <= 0) {
            pauseSong();
        } else {
            //resumeSong();
        }
        /*switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                resumeSong();
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                pauseSong();
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                break;
            default:
        }*/
    }

    PhoneStateListener phoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            if (state == TelephonyManager.CALL_STATE_RINGING) {
                pauseSong();
            } else if (state == TelephonyManager.CALL_STATE_IDLE) {
                resumeSong();
            } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                //A call is dialing, active or on hold
            }
            super.onCallStateChanged(state, incomingNumber);
        }
    };
}
