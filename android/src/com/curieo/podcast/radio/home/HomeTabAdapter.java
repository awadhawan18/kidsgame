package com.curieo.podcast.radio.home;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.curieo.podcast.R;
import com.curieo.podcast.radio.adapters.HorizontalViewAdapter;
import com.curieo.podcast.radio.home.channels.ChannelsActivity;
import com.curieo.podcast.radio.models.Channel;
import com.curieo.podcast.radio.models.Tag;

import java.util.ArrayList;
import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class HomeTabAdapter extends RecyclerView.Adapter<HomeTabAdapter.MyViewHolder> {
    private List<Tag> tagsList;
    private List<Channel> channelList;
    private Context mContext;


    public HomeTabAdapter(Context mContext, List<Tag> tagsList, List<Channel> channelList) {
        this.channelList = channelList;
        this.tagsList = tagsList;
        this.mContext = mContext;
    }

    @Override
    public HomeTabAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_scrollview, parent, false);
        return new HomeTabAdapter.MyViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(final HomeTabAdapter.MyViewHolder holder, final int position) {
        final ArrayList<Channel> horizontalChannels = new ArrayList<>();
        final Tag tag = tagsList.get(position);
        for (Channel channel : channelList) {


            if (channel.getChannelGroup().contains(tag.getTagName())) {
                horizontalChannels.add(channel);
            }
        }

        holder.tagName.setText(tag.getTagName().toUpperCase());
        holder.recyclerView.setLayoutManager(holder.llm);

        holder.adapter = new HorizontalViewAdapter(mContext, horizontalChannels);
        holder.recyclerView.setAdapter(holder.adapter);

        holder.viewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ChannelsActivity.class);
                intent.putExtra("tag", tag.getTagName());
                intent.putParcelableArrayListExtra("channels", horizontalChannels);
                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext,EpisodesActivity.class);
                intent.putExtra("id",channel.getID());
                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });*/

    }


    @Override
    public int getItemCount() {
        return tagsList.size();

    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tagName, viewAll;
        private ImageView channelImage;
        private RecyclerView recyclerView;
        private HorizontalViewAdapter adapter;
        private LinearLayoutManager llm;
        private Context mContext;

        public MyViewHolder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            tagName = itemView.findViewById(R.id.tag);
            viewAll = itemView.findViewById(R.id.view_all);
            recyclerView = itemView.findViewById(R.id.recyclerview);
            llm = new LinearLayoutManager(this.mContext);
            llm.setOrientation(LinearLayoutManager.HORIZONTAL);

        }

    }

}
