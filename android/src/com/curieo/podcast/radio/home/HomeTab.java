package com.curieo.podcast.radio.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.curieo.podcast.R;
import com.curieo.podcast.UrlService;
import com.curieo.podcast.radio.database.DatabaseHandler;
import com.curieo.podcast.radio.models.Channel;
import com.curieo.podcast.radio.models.Tag;
import com.curieo.podcast.radio.models.TrendingEpisode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
/*import static com.badlogic.gdx.Input.Keys.R;*/


public class HomeTab extends Fragment {
    ViewPager mViewPager;
    HomeTrendingSwipeAdapter mHomeTrendingSwipeAdapter;
    private DatabaseHandler db;
    private LinearLayoutManager llm;
    private RecyclerView recyclerView;
    private HomeTabAdapter adapter;
    private List<Channel> channels;
    private List<String> stringTags;
    private List<Tag> grouplist;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.home_tab, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        mViewPager = getView().findViewById(R.id.image_pager);

        TabLayout tabLayout = getView().findViewById(R.id.dots);
        tabLayout.setupWithViewPager(mViewPager, true);

        db = new DatabaseHandler(getContext());

        recyclerView = getView().findViewById(R.id.home_recyclerview);
        llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        UrlService service = retrofit.create(UrlService.class);


        Call<List<TrendingEpisode>> episodeTrending = service.getTrendingEpisodes("json");
        episodeTrending.enqueue(new Callback<List<TrendingEpisode>>() {
            @Override
            public void onResponse(Call<List<TrendingEpisode>> call, Response<List<TrendingEpisode>> response) {
                if (response.isSuccessful() && response.body().size() > 0) {
                    List<TrendingEpisode> episode_s = response.body();
                    mHomeTrendingSwipeAdapter = new HomeTrendingSwipeAdapter(getActivity(), response.body());
                    mViewPager.setAdapter(mHomeTrendingSwipeAdapter);
                } else {
                    Log.e("HomeTab", "No episode exist ");
                }

            }

            @Override
            public void onFailure(Call<List<TrendingEpisode>> call, Throwable t) {

            }
        });


        Call<List<Channel>> call = service.getChannels("json");

        channels = new ArrayList<>();
        stringTags = new ArrayList<>();
        grouplist = new ArrayList<>();

        call.enqueue(new Callback<List<Channel>>() {
            @Override
            public void onResponse(Call<List<Channel>> call, Response<List<Channel>> response) {
                channels = response.body();
                for (Channel channel : channels) {

                    List<String> channelGroups = channel.getChannelGroup();
                    for (String tag : channelGroups) {
                        if (!stringTags.contains(tag)) {
                            stringTags.add(tag);
                            grouplist.add(new Tag(tag));
                        }
                    }

                }
                Log.v("tags", stringTags.toString());
                for (Tag tag : grouplist) {
                    Log.e("", "onResponse: " + tag.getTagName());
                }
                adapter = new HomeTabAdapter(getContext(), grouplist, channels);
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<List<Channel>> call, Throwable t) {
                Log.v("fucking hell", t.toString());
            }
        });

    }
}




/*        grouplist = db.getAllTags();
        channelList = db.getAllChannels();
        listOfChannelList = new ArrayList<>();
        for (Tag tag : grouplist) {
            String tagName = tag.getTagName();
            List<Channel> newChannelList = new ArrayList<>();
            for (Channel channel : channelList) {
                if (channel.getTags().contains(tagName)) {
                    newChannelList.add(channel);
                }
            }
            listOfChannelList.add(newChannelList);
        }
        adapter = new HomeTabAdapter(getContext(), grouplist, listOfChannelList);
        recyclerView.setAdapter(adapter);*/

