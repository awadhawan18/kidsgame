package com.curieo.podcast.radio.home.channels;

import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.curieo.podcast.R;
import com.curieo.podcast.radio.models.Channel;

import java.util.ArrayList;

/*import static com.badlogic.gdx.Input.Keys.R;*/




public class ChannelsActivity extends AppCompatActivity {
    private LinearLayoutManager llm;
    private RecyclerView recyclerView;
    private ChannelsViewAdapter adapter;

    private ArrayList<Channel> channelsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channels);

        String title = getIntent().getExtras().getString("tag");
        channelsList = getIntent().getParcelableArrayListExtra("channels");

       Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(title.toUpperCase());
        toolbar.setTitleTextColor(getResources().getColor(R.color.white_text));
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        recyclerView = findViewById(R.id.recyclerview);
        llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);

        adapter = new ChannelsViewAdapter(getApplicationContext(), channelsList);


        recyclerView.setAdapter(adapter);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


}
