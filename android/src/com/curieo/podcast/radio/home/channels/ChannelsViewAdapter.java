package com.curieo.podcast.radio.home.channels;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.curieo.podcast.R;
import com.curieo.podcast.radio.home.EpisodesActivity;
import com.curieo.podcast.radio.models.Channel;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class ChannelsViewAdapter extends RecyclerView.Adapter<ChannelsViewAdapter.MyViewHolder> {
    private List<Channel> channelList;
    private Context mContext;

    public ChannelsViewAdapter(Context mContext, List<Channel> channelList) {
        this.channelList = channelList;
        this.mContext = mContext;

    }

    @Override
    public ChannelsViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.channel_card, parent, false);
        return new ChannelsViewAdapter.MyViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(final ChannelsViewAdapter.MyViewHolder holder, final int position) {
        final Channel channel = channelList.get(position);
        holder.channelName.setText(channel.getName());
        Glide.with(mContext).load(channel.getChannelImageUrl()).into(holder.channelImage);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, EpisodesActivity.class);
                intent.putExtra("id", channel.getID());
                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return channelList.size();

    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView channelName;
        private ImageView channelImage;
        private Context mContext;

        public MyViewHolder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            channelName = itemView.findViewById(R.id.channel_name);
            channelImage = itemView.findViewById(R.id.channel_image);

        }

    }

}
