package com.curieo.podcast.radio.home;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.curieo.podcast.R;
import com.curieo.podcast.UrlService;
import com.curieo.podcast.radio.customviews.CustomViewPager;
import com.curieo.podcast.radio.adapters.ViewPagerAdapter;
import com.curieo.podcast.radio.models.Feed;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EpisodesActivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private CustomViewPager viewPager;
    private ImageView channelImage;
    private List<Feed> feeds;
    private ArrayList<String> categoryTags;
    private Map<String, ArrayList<Feed>> categoryFeeds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.channel_view);


        final ImageView mImageView = (ImageView) findViewById(R.id.channel_image1);
        //mImageView.setImageResource(R.drawable.button_play);
        TextView mTextView = (TextView) findViewById(R.id.suscribe);
        ImageButton mImageButton = (ImageButton) findViewById(R.id.susbutton);
        TextView mTextView2 = (TextView) findViewById(R.id.head2);
        TextView mTextView3 = (TextView) findViewById(R.id.desc2);

        int id = getIntent().getIntExtra("id", 1);
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        /*channelImage = findViewById(R.id.channel_image);*/

        viewPager = findViewById(R.id.episodes_viewpager);
        viewPager.setOffscreenPageLimit(5);

        tabLayout = findViewById(R.id.episodes_tabLayout);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position, false);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        UrlService service = retrofit.create(UrlService.class);
        Call<List<Feed>> call = service.getEpisodes(id, "json");

        categoryFeeds = new HashMap<>();
        categoryTags = new ArrayList<>();

        call.enqueue(new Callback<List<Feed>>() {
            @Override
            public void onResponse(Call<List<Feed>> call, Response<List<Feed>> response) {
                feeds = response.body();
                if (feeds != null) {
                    if (!feeds.isEmpty()) {
                      //  toolbar.setTitle(feeds.get(0).getChannel().getName());
                        Glide.with(getApplicationContext()).load(feeds.get(0).getChannel().getChannelImageUrl())
                                .into(mImageView);
                    }

                }
                for (Feed feed : feeds) {
                    String episodeCategory = feed.getEpisodeCategory();
                    List<String> channelCategories = feed.getChannel().getChannelSection();
                    //if(channelCategories.contains(episodeCategory)){
                    if (categoryFeeds.containsKey(episodeCategory)) {
                        categoryFeeds.get(episodeCategory).add(feed);
                    } else {
                        ArrayList<Feed> episodeList = new ArrayList<>();
                        episodeList.add(feed);
                        categoryFeeds.put(episodeCategory, episodeList);
                        categoryTags.add(episodeCategory);

                    }

                    //}

                }
                Log.v("tags", categoryTags.toString());

                setupViewPager(viewPager);

            }

            @Override
            public void onFailure(Call<List<Feed>> call, Throwable t) {
                Log.v("fucking hell", t.toString());
            }
        });

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        for (String key : categoryFeeds.keySet()) {
            FragmentEpisode tab = new FragmentEpisode().newInstance(categoryFeeds.get(key));
            adapter.addFragment(tab, key);
        }

        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        //Register Broadcast receiver
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mBroadcastReceiver, new IntentFilter("Broadcast"));
    }

    @Override
    public void onPause() {
        super.onPause();
        //Unregister Broadcast receiver
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mBroadcastReceiver);
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            /*String intentType = intent.getStringExtra("INTENT_TYPE");
            if(intentType.equalsIgnoreCase("SEEKBAR_RESULT")){
                int percentage = intent.getIntExtra("PERCENTAGE",0);
                seekBarProgress.setProgress(percentage);
            }
            if(intentType.equalsIgnoreCase("PLAY_RESULT") || intentType.equalsIgnoreCase("PAUSE_RESULT")){
                String stringExtra = intent.getStringExtra("SONG_NAME");
                songName.setText(stringExtra);
                Log.v("song",stringExtra);
            }*/
        }
    };

    // checks is play service is running or not
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void publishResult(Context context, int percentage) {
        Intent intent = new Intent("Broadcast");
        intent.putExtra("INTENT_TYPE", "SEEK_TO");
        intent.putExtra("PERCENTAGE", percentage);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }


}
