package com.curieo.podcast.radio.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.curieo.podcast.R;
import com.curieo.podcast.radio.database.DatabaseHandler;
import com.curieo.podcast.radio.models.Feed;
import com.curieo.podcast.radio.personal.EpisodesViewAdapter;

import java.util.ArrayList;


public class FragmentEpisode extends Fragment {
    private LinearLayoutManager llm;
    private RecyclerView recyclerView;
    private EpisodesViewAdapter adapter;
    private DatabaseHandler db;
    private final String KEY = "LIST";
    private ArrayList<Feed> episodes;

    public FragmentEpisode newInstance(ArrayList<Feed> feeds) {
        FragmentEpisode f = new FragmentEpisode();
        Bundle b = new Bundle();
        b.putParcelableArrayList(KEY, feeds);
        f.setArguments(b);

        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        episodes = getArguments().getParcelableArrayList(KEY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_episodes, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        recyclerView = getView().findViewById(R.id.recyclerview);
        llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        DividerItemDecoration vDivider = new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL);
        DividerItemDecoration hDivider = new DividerItemDecoration(getContext(),DividerItemDecoration.HORIZONTAL);
      /*  recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.HORIZONTAL));*/
      recyclerView.removeItemDecoration(hDivider);
        recyclerView.removeItemDecoration(vDivider);

        if (episodes != null) {
            adapter = new EpisodesViewAdapter(getContext(), episodes);
            recyclerView.setAdapter(adapter);
        }


    }

    @Override
    public void onResume() {
        super.onResume();

    }
}
