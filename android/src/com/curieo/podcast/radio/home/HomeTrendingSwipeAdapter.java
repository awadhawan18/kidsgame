package com.curieo.podcast.radio.home;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.curieo.podcast.R;
import com.curieo.podcast.radio.models.TrendingEpisode;

import java.util.List;

/*import static com.badlogic.gdx.Input.Keys.R;*/

public class HomeTrendingSwipeAdapter extends PagerAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<TrendingEpisode> feeds;

    public HomeTrendingSwipeAdapter(Context context, List<TrendingEpisode> feeds) {
        mContext = context;
        this.feeds = feeds;
    }

    @Override
    public int getCount() {
        return feeds.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = mInflater.inflate(R.layout.swipe_layout1, container, false);
        ImageView imageView = view.findViewById(R.id.swipeimg);
        Glide.with(mContext).load(feeds.get(position).getEpisode().getChannel().getChannelImageUrl()).into(imageView);
        TextView head = view.findViewById(R.id.head1);
        TextView descp = view.findViewById(R.id.desc);
        head.setText(feeds.get(position).getEpisode().getTitle());
        descp.setText(feeds.get(position).getEpisode().getDescription());
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

}
