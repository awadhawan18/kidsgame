package com.curieo.podcast.radio.personal;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.curieo.podcast.R;
import com.curieo.podcast.UrlService;
import com.curieo.podcast.radio.models.ActionResponse;
import com.curieo.podcast.radio.CurrentStatus;
import com.curieo.podcast.radio.database.DatabaseHandler;
import com.curieo.podcast.radio.models.Feed;
import com.curieo.podcast.radio.record.OnDatabaseChangedListener;
import com.curieo.podcast.radio.services.PlayMusicService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.curieo.podcast.radio.CurrentStatus.serviceIntent;


public class FavouriteEpisodesViewAdapter extends RecyclerView.Adapter<FavouriteEpisodesViewAdapter.MyViewHolder>
        implements OnDatabaseChangedListener {
    private DatabaseHandler db;
    private List<Feed> episodeList;
    private Context mContext;
    private List<Feed> likesList;
    private UrlService service;

    public FavouriteEpisodesViewAdapter(Context mContext) {
        this.mContext = mContext;
        db = new DatabaseHandler(this.mContext);
        DatabaseHandler.setOnDatabaseChangedListener(this);


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.episode_card, parent, false);
        return new MyViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final Feed episode = getItem(position);
        if (episode != null) {

            holder.episodeName.setText(episode.getTitle());
            holder.favourite.setImageResource(R.drawable.heart_filled);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    playNewSong(episode);

                }
            });
            holder.favourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.favourite.setImageResource(R.drawable.heart_filled);
                    Call<ActionResponse> call = service.postLike(1, "LIKE", episode.getId());
                    call.enqueue(new Callback<ActionResponse>() {
                        @Override
                        public void onResponse(Call<ActionResponse> call, Response<ActionResponse> response) {

                        }

                        @Override
                        public void onFailure(Call<ActionResponse> call, Throwable t) {
                            Log.v("fucking hell", t.toString());
                        }
                    });


                }
            });
        }


        /*holder.favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (episode.isFavourite() == 0) {
                    holder.favourite.setImageResource(R.drawable.heart_filled);
                    episode.setFavourite(1);
                } else {
                    holder.favourite.setImageResource(R.drawable.heart_empty);
                    episode.setFavourite(0);
                }
                db.updateEpisode(episode);
            }
        });*/

    }

    public Feed getItem(int position) {
        return db.getFavouriteEpisodeAt(position);
    }

    @Override
    public int getItemCount() {
        return db.getFavouriteEpisodeCount();
    }

    @Override
    public void onEpisodeUpdated() {
        notifyDataSetChanged();
    }

    private void playNewSong(Feed episode) {
        CurrentStatus.currentPlaylist = episodeList;
        serviceIntent = new Intent(mContext, PlayMusicService.class);
        serviceIntent.setAction("play");
        CurrentStatus.currentEpisode = episode;
        if (CurrentStatus.isServiceRunning) {
            mContext.stopService(serviceIntent);
            CurrentStatus.isServiceRunning = false;
            mContext.startService(serviceIntent);

        } else {
            CurrentStatus.bottomPanelVisible = true;
            mContext.startService(serviceIntent);
        }

    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView episodeName;
        private ImageButton favourite;
        private ImageView episodeImage;
        private Context mContext;

        public MyViewHolder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            episodeName = itemView.findViewById(R.id.episode_name);
            favourite = itemView.findViewById(R.id.heart);
            episodeImage = itemView.findViewById(R.id.episode_image);


        }

    }

}
