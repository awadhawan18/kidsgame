package com.curieo.podcast.radio.personal;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.curieo.podcast.R;
import com.curieo.podcast.UrlService;
import com.curieo.podcast.radio.models.Channel;
import com.curieo.podcast.radio.database.DatabaseHandler;
import com.curieo.podcast.radio.models.Feed;
import com.curieo.podcast.onboarding.data.SessionManager;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MyListTab extends Fragment {
    private LinearLayoutManager llm;
    private RecyclerView recyclerView;
    private EpisodesViewAdapter adapter;
    private DatabaseHandler db;
    SessionManager sessionManager;
    private List<Feed> feeds;
    private List<Feed> likesList;
    private List<Channel> channels;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.my_list_tab, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        sessionManager = new SessionManager(getActivity());

        db = new DatabaseHandler(getContext());

        recyclerView = getView().findViewById(R.id.recyclerview);
        llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
    /*    recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));*/
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), 0));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Log.e("MyListTab", "onActivityCreated: " + sessionManager.getParent());
        UrlService service = retrofit.create(UrlService.class);
        Call<List<Feed>> call = service.getFeeds(sessionManager.getParent(), "json");

        channels = new ArrayList<>();

        call.enqueue(new Callback<List<Feed>>() {
            @Override
            public void onResponse(Call<List<Feed>> call, Response<List<Feed>> response) {
                Log.e("MyListTab", "onResponse: " + new GsonBuilder().setPrettyPrinting().create().toJson(response));

                feeds = response.body();
                for (Feed feed : feeds) {
                    int id = feed.getChannel().getID();
                    boolean flag = false;
                    for (Channel channel : channels) {
                        if (channel.getID() == id) {
                            flag = true;
                        }
                    }
                    if (!flag) {
                        channels.add(feed.getChannel());
                    }
                }
                adapter = new EpisodesViewAdapter(getContext(), feeds);
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<List<Feed>> call, Throwable t) {
                Log.v("fucking hell", t.toString());
            }
        });

        SessionManager sessionManager = new SessionManager(getActivity());

        Call<List<Feed>> likes = service.getLikedEpisodes(sessionManager.getParent(), "json");

        likes.enqueue(new Callback<List<Feed>>() {
            @Override
            public void onResponse(Call<List<Feed>> call, Response<List<Feed>> response) {
                likesList = response.body();
                List<Integer> dbLikes = db.getFavouriteEpisodeIds();

            }

            @Override
            public void onFailure(Call<List<Feed>> call, Throwable t) {
                Log.v("fucking hell", t.toString());
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

    }
}
