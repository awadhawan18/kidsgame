package com.curieo.podcast.radio.personal;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.curieo.podcast.R;
import com.curieo.podcast.UrlService;
import com.curieo.podcast.radio.models.ActionResponse;
import com.curieo.podcast.radio.CurrentStatus;
import com.curieo.podcast.radio.database.DatabaseHandler;
import com.curieo.podcast.radio.models.Feed;
import com.curieo.podcast.radio.services.PlayMusicService;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import static com.curieo.podcast.radio.CurrentStatus.serviceIntent;


public class EpisodesViewAdapter extends RecyclerView.Adapter<EpisodesViewAdapter.MyViewHolder> {
    private DatabaseHandler db;
    private List<Feed> episodeList;
    private Context mContext;
    private List<Feed> likesList;
    private UrlService service;

    public EpisodesViewAdapter(Context mContext, List<Feed> episodesList) {
        this.episodeList = episodesList;
        this.mContext = mContext;
        db = new DatabaseHandler(this.mContext);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mContext.getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(UrlService.class);

        /*Call<List<Feed>> likes = service.getLikedEpisodes("json");

        likes.enqueue(new Callback<List<Feed>>() {
            @Override
            public void onResponse(Call<List<Feed>> call, Response<List<Feed>> response) {
                likesList = response.body();
            }

            @Override
            public void onFailure(Call<List<Feed>> call, Throwable t) {
                Log.v("fucking hell", t.toString());
            }
        });*/

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.episode_card, parent, false);
        return new MyViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Feed episode = episodeList.get(position);
        holder.episodeName.setText(episode.getTitle());
        Glide.with(mContext).load(episode.getChannel().getChannelImageUrl()).into(holder.episodeImage);
        /*if (episode.isFavourite() == 1) {
            holder.favourite.setImageResource(R.drawable.heart_filled);
        }*/
        holder.favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.favourite.setImageResource(R.drawable.heart_filled);
                Call<ActionResponse> call = service.postLike(1, "LIKE", episode.getId());
                call.enqueue(new Callback<ActionResponse>() {
                    @Override
                    public void onResponse(Call<ActionResponse> call, Response<ActionResponse> response) {

                    }

                    @Override
                    public void onFailure(Call<ActionResponse> call, Throwable t) {
                        Log.v("fucking hell", t.toString());
                    }
                });


            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playNewSong(episode);

            }
        });

        /*holder.favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (episode.isFavourite() == 0) {
                    holder.favourite.setImageResource(R.drawable.heart_filled);
                    episode.setFavourite(1);
                } else {
                    holder.favourite.setImageResource(R.drawable.heart_empty);
                    episode.setFavourite(0);
                }
                db.updateEpisode(episode);
            }
        });*/

    }


    @Override
    public int getItemCount() {
        return episodeList.size();
    }

    private void playNewSong(Feed episode) {
        CurrentStatus.currentPlaylist = episodeList;
        serviceIntent = new Intent(mContext, PlayMusicService.class);
        serviceIntent.setAction("play");
        CurrentStatus.currentEpisode = episode;
        if (CurrentStatus.isServiceRunning) {
            mContext.stopService(serviceIntent);
            CurrentStatus.isServiceRunning = false;
            mContext.startService(serviceIntent);

        } else {
            CurrentStatus.bottomPanelVisible = true;
            mContext.startService(serviceIntent);
        }

    }

    private void publishPausePlayResult(Context context) {
        Intent intent = new Intent("Broadcast");
        intent.putExtra("INTENT_TYPE", "TOGGLE_PAUSE_PLAY");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    // checks is play service is running or not
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView episodeName;
        private ImageButton favourite;
        private ImageView episodeImage;
        private Context mContext;

        public MyViewHolder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            episodeName = itemView.findViewById(R.id.episode_name);
            favourite = itemView.findViewById(R.id.heart);
            episodeImage = itemView.findViewById(R.id.episode_image);


        }

    }


    /*private void pauseSong(final int position){

        CurrentStatus.isSongPlaying = true;
        lastPosition = position;
        Intent serviceIntent = new Intent(mContext, PlayMusicService.class);
        serviceIntent.setAction("pause");
        mContext.startService(serviceIntent);

    }

    private void resumeSong(){

        CurrentStatus.isSongPlaying = false;
        Intent serviceIntent = new Intent(mContext, PlayMusicService.class);
        serviceIntent.setAction("resume");
        mContext.startService(serviceIntent);

    }*/
}
