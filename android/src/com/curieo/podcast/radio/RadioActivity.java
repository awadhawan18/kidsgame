package com.curieo.podcast.radio;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.curieo.podcast.R;
import com.curieo.podcast.radio.adapters.ViewPagerAdapter;
import com.curieo.podcast.radio.customviews.CustomViewPager;
import com.curieo.podcast.radio.database.DatabaseHandler;
import com.curieo.podcast.radio.home.HomeTab;
import com.curieo.podcast.radio.personal.EpisodesTab;
import com.curieo.podcast.radio.record.YouTab;
import com.curieo.podcast.radio.services.PlayMusicService;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.concurrent.TimeUnit;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.curieo.podcast.radio.CurrentStatus.currentEpisode;
import static com.curieo.podcast.radio.CurrentStatus.serviceIntent;


public class RadioActivity extends AppCompatActivity {
    private static String ACTION_OPEN_PLAYER = "open_player";
    private static int resumecalls = 0;
    private TabLayout tabLayout;
    private CustomViewPager viewPager;
    private HomeTab homeTab;
    private YouTab youTab;
    private EpisodesTab episodesTab;
    private SeekBar seekBarProgress;
    private TextView episodeName, episodeDescription, startLength, endLength;
    private ImageView episodeImage, expandedPanelImage;
    private ImageButton pauseButton, previousButton, nextButton, fullPlayerPrevButton, fullPlayerPauseButton, fullPlayerNextButton;
    private SlidingUpPanelLayout slidingUpPanelLayout;
    private String action;
    private int[] navIcons = {
            R.drawable.home_tab_icon,
            R.drawable.you_tab_icon,
            R.drawable.episode_tab_icon
    };

    private int[] navIconsActive = {
            R.drawable.home_tab_active_icon,
            R.drawable.you_active_icon,
            R.drawable.episodes_active_icon

    };

    private String[] navLabels = {
            "Home",
            "You",
            "Episodes"
    };
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String intentType = intent.getStringExtra("INTENT_TYPE");
            if (intentType.equalsIgnoreCase("SEEKBAR_RESULT")) {
                int progress = intent.getIntExtra("PERCENTAGE", 0);
                int length = intent.getIntExtra("FILE_LENGTH", 0);
                if (length != 0) {
                    int percentage = (progress / length) * 100;
                    seekBarProgress.setProgress(percentage);
                }
                long endMinutes = TimeUnit.MILLISECONDS.toMinutes(length);
                long endSeconds = TimeUnit.MILLISECONDS.toSeconds(length)
                        - TimeUnit.MINUTES.toSeconds(endMinutes);
                endLength.setText(String.format("%02d:%02d", endMinutes, endSeconds));
                long startMinutes = TimeUnit.MILLISECONDS.toMinutes(progress);
                long startSeconds = TimeUnit.MILLISECONDS.toSeconds(progress)
                        - TimeUnit.MINUTES.toSeconds(startMinutes);
                startLength.setText(String.format("%02d:%02d", startMinutes, startSeconds));
            }
            if (intentType.equals("PAUSE_OR_PLAY") || intentType.equals("PAUSE_RESULT")) {
                CurrentStatus.bottomPanelVisible = true;
                setMusicBar();
            }

        }
    };

    public static void publishResult(Context context, int percentage) {
        Intent intent = new Intent("Broadcast");
        intent.putExtra("INTENT_TYPE", "SEEK_TO");
        intent.putExtra("PERCENTAGE", percentage);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DatabaseHandler db = new DatabaseHandler(this);
        viewPager = findViewById(R.id.viewpager);
        viewPager.setPagingEnabled(false);
        viewPager.setOffscreenPageLimit(3);

        tabLayout = findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position, false);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        setupViewPager(viewPager);

        slidingUpPanelLayout = findViewById(R.id.sliding_layout);
        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);


        final LinearLayout bottomPanel = findViewById(R.id.bottom_panel_episode);
        final LinearLayout bottomPanelMediaControls = findViewById(R.id.media_controls);
        final RelativeLayout expandedPanel = findViewById(R.id.expanded_state);

        slidingUpPanelLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                /*Log.v("Panel state",String.valueOf(slidingUpPanelLayout.getPanelState()));
                Log.v("previous state",String.valueOf(previousState));
                Log.v("new state",String.valueOf(newState));*/
                if (newState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    bottomPanel.setVisibility(View.GONE);
                    bottomPanelMediaControls.setVisibility(View.GONE);
                    expandedPanel.setVisibility(View.VISIBLE);
                } else if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    bottomPanel.setVisibility(View.VISIBLE);
                    bottomPanelMediaControls.setVisibility(View.VISIBLE);
                    expandedPanel.setVisibility(View.GONE);
                }

            }
        });


        seekBarProgress = findViewById(R.id.seekbar_test_play);
        seekBarProgress.setMax(99); // It means 100%
        seekBarProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (CurrentStatus.isServiceRunning) {
                    int percentage = seekBar.getProgress();
                    publishResult(getApplicationContext(), percentage);
                }
            }
        });


        pauseButton = findViewById(R.id.pause_play_button);
        previousButton = findViewById(R.id.previous_button);
        nextButton = findViewById(R.id.next_button);

        startLength = findViewById(R.id.startText);
        endLength = findViewById(R.id.endText);

        fullPlayerNextButton = findViewById(R.id.expanded_next);
        fullPlayerPauseButton = findViewById(R.id.expanded_play_pause);
        fullPlayerPrevButton = findViewById(R.id.expanded_prev);

        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CurrentStatus.isServiceRunning) {
                    if (CurrentStatus.isSongPlaying) {
                        pauseButton.setImageResource(R.drawable.button_play);
                        fullPlayerPauseButton.setImageResource(R.drawable.button_play);
                    } else {
                        pauseButton.setImageResource(R.drawable.button_pause);
                        fullPlayerPauseButton.setImageResource(R.drawable.button_pause);
                    }

                    publishPausePlayResult(getApplicationContext());

                }


            }
        });

        fullPlayerPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CurrentStatus.isServiceRunning) {
                    if (CurrentStatus.isSongPlaying) {
                        pauseButton.setImageResource(R.drawable.button_play);
                        fullPlayerPauseButton.setImageResource(R.drawable.button_play);
                    } else {
                        pauseButton.setImageResource(R.drawable.button_pause);
                        fullPlayerPauseButton.setImageResource(R.drawable.button_pause);
                    }

                    publishPausePlayResult(getApplicationContext());

                }


            }
        });

        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playPrevEpisode();
            }
        });

        fullPlayerPrevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playPrevEpisode();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playNextEpisode();
            }
        });

        fullPlayerNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playNextEpisode();
            }
        });

        episodeName = findViewById(R.id.episodeName);
        episodeDescription = findViewById(R.id.episode_desc);
        episodeName.setText(CurrentStatus.currentSong);
        episodeImage = findViewById(R.id.episode_image);
        expandedPanelImage = findViewById(R.id.expanded_panel_image);

        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, 1);

        action = getIntent().getAction();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {

            case 1: {

                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
                            showMessageOKCancel("You need to allow access to this permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE},
                                                        1);

                                            }
                                        }
                                    }, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                        }
                                    });

                        }
                    }
                }

            }
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener, DialogInterface.OnClickListener cancelListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", cancelListener)
                .create()
                .show();
    }

    // checks if play service is running or not
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void publishPausePlayResult(Context context) {
        Intent intent = new Intent("Broadcast");
        intent.putExtra("INTENT_TYPE", "PAUSE_PLAY_RESULT");
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        homeTab = new HomeTab();
        youTab = new YouTab();
        episodesTab = new EpisodesTab();

        adapter.addFragment(homeTab, "Home");
        adapter.addFragment(episodesTab, "Episodes");
        adapter.addFragment(youTab, "Record");

        viewPager.setAdapter(adapter);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            LinearLayout tab = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.nav_tab, null);

            // get child TextView and ImageView from this layout for the icon and label
            TextView tab_label = tab.findViewById(R.id.nav_label);
            ImageView tab_icon = tab.findViewById(R.id.nav_icon);

            tab_label.setText(navLabels[i]);

            // set the home to be active at first
            if (i == 0) {
                tab_label.setTextColor(getResources().getColor(R.color.active_tab_color));
                tab_icon.setImageResource(navIconsActive[i]);
            } else {
                tab_icon.setImageResource(navIcons[i]);
            }


            // finally publish this custom view to navigation tab
            tabLayout.getTabAt(i).setCustomView(tab);

        }
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View tabView = tab.getCustomView();

                // get inflated children Views the icon and the label by their id
                TextView tab_label = tabView.findViewById(R.id.nav_label);
                ImageView tab_icon = tabView.findViewById(R.id.nav_icon);
                // change the label color, by getting the color resource value
                tab_label.setTextColor(getResources().getColor(R.color.active_tab_color));

                tab_icon.setImageResource(navIconsActive[tab.getPosition()]);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View tabView = tab.getCustomView();
                TextView tab_label = tabView.findViewById(R.id.nav_label);
                ImageView tab_icon = tabView.findViewById(R.id.nav_icon);

                // back to the black color
                tab_label.setTextColor(getResources().getColor(R.color.black_text));

                tab_icon.setImageResource(navIcons[tab.getPosition()]);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        //Register Broadcast receiver
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mBroadcastReceiver, new IntentFilter("Broadcast"));
        setMusicBar();
        if (action != null) {
            if (action.equalsIgnoreCase(ACTION_OPEN_PLAYER)) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                resumecalls++;
                if (resumecalls == 2) {
                    action = null;
                    resumecalls = 0;
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //Unregister Broadcast receiver
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mBroadcastReceiver);
    }

    private void setMusicBar() {
        if (CurrentStatus.bottomPanelVisible) {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            if (action != null) {
                if (action.equalsIgnoreCase(ACTION_OPEN_PLAYER)) {
                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                }
            }
        }
        if (CurrentStatus.currentEpisode != null) {
            episodeName.setText(CurrentStatus.currentEpisode.getTitle());
            episodeDescription.setText(currentEpisode.getDescription());
            if (currentEpisode.getChannel() != null) {
                Glide.with(getApplicationContext()).load(currentEpisode.getChannel().getChannelImageUrl()).into(episodeImage);
                Glide.with(getApplicationContext()).load(currentEpisode.getChannel().getChannelImageUrl()).into(expandedPanelImage);
            }
        } else episodeName.setText(CurrentStatus.currentSong);
        if (CurrentStatus.isSongPlaying) {
            pauseButton.setImageResource(R.drawable.button_pause);
        } else {
            pauseButton.setImageResource(R.drawable.button_play);
        }
    }

    private void playNextEpisode() {
        if (CurrentStatus.currentPlaylist != null) {
            int index = CurrentStatus.currentPlaylist.indexOf(CurrentStatus.currentEpisode);
            if (index < CurrentStatus.currentPlaylist.size() - 1) {
                index++;
                serviceIntent = new Intent(getApplicationContext(), PlayMusicService.class);
                serviceIntent.setAction("play");
                CurrentStatus.currentEpisode = CurrentStatus.currentPlaylist.get(index);
                if (CurrentStatus.isServiceRunning) {
                    getApplicationContext().stopService(serviceIntent);
                    CurrentStatus.isServiceRunning = false;
                    getApplicationContext().startService(serviceIntent);

                } else {
                    CurrentStatus.bottomPanelVisible = true;
                    getApplicationContext().startService(serviceIntent);
                }
            }

        }
    }

    private void playPrevEpisode() {
        if (CurrentStatus.currentPlaylist != null) {
            int index = CurrentStatus.currentPlaylist.indexOf(CurrentStatus.currentEpisode);
            if (index > 0) {
                index--;
                serviceIntent = new Intent(getApplicationContext(), PlayMusicService.class);
                serviceIntent.setAction("play");
                CurrentStatus.currentEpisode = CurrentStatus.currentPlaylist.get(index);
                if (CurrentStatus.isServiceRunning) {
                    getApplicationContext().stopService(serviceIntent);
                    CurrentStatus.isServiceRunning = false;
                    getApplicationContext().startService(serviceIntent);

                } else {
                    CurrentStatus.bottomPanelVisible = true;
                    getApplicationContext().startService(serviceIntent);
                }
            }

        }
    }

}
