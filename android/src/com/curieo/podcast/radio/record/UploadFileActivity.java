package com.curieo.podcast.radio.record;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.curieo.podcast.R;
import com.curieo.podcast.onboarding.MySpinnerAdapter;
import com.curieo.podcast.onboarding.data.KidUser;
import com.curieo.podcast.onboarding.data.UserDatabaseHandler;

import java.util.ArrayList;
import java.util.List;

public class UploadFileActivity extends AppCompatActivity {

    UserDatabaseHandler userDatabaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_file);
        userDatabaseHandler = new UserDatabaseHandler(this);
        List<String> spinnerArray = new ArrayList<String>();
        List<KidUser> kidUsers = userDatabaseHandler.getAllKids();
        for (int i = 0; i < userDatabaseHandler.getkidCount(); i++) {
            spinnerArray.add(kidUsers.get(i).getKidName());

        }
        MySpinnerAdapter
                adapter = new MySpinnerAdapter(
                this, R.layout.spinner_school_item, spinnerArray);
    }



}
