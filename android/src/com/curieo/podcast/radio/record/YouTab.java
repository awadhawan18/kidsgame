package com.curieo.podcast.radio.record;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.curieo.podcast.R;
import com.curieo.podcast.radio.adapters.ViewPagerAdapter;
import com.curieo.podcast.radio.database.DatabaseHandler;
import com.curieo.podcast.radio.models.EpisodeOld;
import com.curieo.podcast.radio.personal.EpisodesViewAdapter;

import java.util.List;


public class YouTab extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private DraftsTab draftsTab;
    private RecordTab recordTab;

    private LinearLayoutManager llm;
    private RecyclerView recyclerView;
    private EpisodesViewAdapter adapter;
    private DatabaseHandler db;
    private List<EpisodeOld> episodes;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.you_tab, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        viewPager = getView().findViewById(R.id.you_tab_viewpager);
        viewPager.setOffscreenPageLimit(2);

        tabLayout = getView().findViewById(R.id.you_tabLayout);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position, false);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        setupViewPager(viewPager);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());

        draftsTab = new DraftsTab();
        recordTab = new RecordTab();

        adapter.addFragment(recordTab, "record");
        adapter.addFragment(draftsTab, "Drafts");

        viewPager.setAdapter(adapter);
    }
}
