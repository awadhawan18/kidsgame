package com.curieo.podcast.radio.record;


public interface OnDatabaseChangedListener {
    void onEpisodeUpdated();
}
