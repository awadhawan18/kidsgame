package com.curieo.podcast.radio.record;


public interface OnRecordDatabaseChangedListener {
    void onNewDatabaseEntryAdded();

    void onDatabaseEntryRenamed();
}
