/*
 * Copyright (c) 2014.
 * Main Method Incorporated.
 */

package com.curieo.podcast.onboarding;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Manages storing and retrieval of Preferences
 * Created by evan on 8/24/14.
 */
public class AppPrefHelper {
    private static final String PROPERTY_USER_HAS_ONBOARDED = "UserHasOnboarded";
    private static final String PREFERENCES = "BBRPreferences";
    private static AppPrefHelper sInstance;
    private SharedPreferences mPreferences;

    /**
     * Creates a new PreferenceManager
     *
     * @param context context
     */
    private AppPrefHelper(Context context) {
        mPreferences = context.getSharedPreferences(PREFERENCES, 0);

    }

    public static AppPrefHelper getInstance(Context context) {

        if (sInstance == null) {
            sInstance = new AppPrefHelper(context);
        }
        return sInstance;
    }

    public boolean hasUserOnboarded() {
        return mPreferences.getBoolean(PROPERTY_USER_HAS_ONBOARDED, false);
    }

    public void setUserHasOnboarded() {
        mPreferences.edit().putBoolean(PROPERTY_USER_HAS_ONBOARDED, true).apply();
    }

}
