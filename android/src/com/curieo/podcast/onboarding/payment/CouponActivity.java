package com.curieo.podcast.onboarding.payment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.curieo.podcast.games.AndroidLauncher;
import com.curieo.podcast.R;
import com.curieo.podcast.UrlService;
import com.curieo.podcast.onboarding.activity.BlackboardActivity;
import com.curieo.podcast.onboarding.data.KidUser;
import com.curieo.podcast.onboarding.data.SessionManager;
import com.curieo.podcast.onboarding.data.UserDatabaseHandler;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class CouponActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "CouponActivity";
    Button makePaymentButton;
    TextView addCouponTextview;


    SessionManager sessionManager;
    List<Coupon> couponList;

    UserDatabaseHandler userDatabaseHandler;
    ;
    ImageView backButton;
    Dialog dialog;
    Typeface typeface;

    KidUser kidUser;
    Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon);

        userDatabaseHandler = new UserDatabaseHandler(this);
        makePaymentButton = (Button) findViewById(R.id.start_trial);
        backButton = (ImageView) findViewById(R.id.back_button_image);
        backButton.setOnClickListener(this);
        intent = getIntent();
        kidUser = (KidUser) intent.getSerializableExtra("KidUser");
        couponList = new ArrayList<>();
        typeface = Typeface.createFromAsset(getResources().getAssets(), "asparagussprouts.ttf");
        ViewPager pager = (ViewPager) findViewById(R.id.photos_viewpager);
        CouponViewPagerAdapter couponViewPagerAdapter = new CouponViewPagerAdapter(this);
        pager.setAdapter(couponViewPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(pager, true);

        addCouponTextview = (TextView) findViewById(R.id.coupon);

        addCouponTextview.setTypeface(typeface);
        makePaymentButton.setTypeface(typeface);
        sessionManager = new SessionManager(this);

        makePaymentButton.setOnClickListener(this);
        addCouponTextview.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        boolean number = false;
        switch (v.getId()) {
            case R.id.start_trial:
                Intent intent = new Intent(this, AndroidLauncher.class);
                startActivity(intent);
                break;
            case R.id.coupon:
                showdialog();
                break;

            case R.id.back_button_image:
                super.onBackPressed();

                break;

            default:
                // Toast.makeText(this, "Do nothing ", Toast.LENGTH_SHORT).show();


        }
    }

    private static class CouponViewPagerAdapter extends PagerAdapter {


        Context ctx;
        private LayoutInflater layoutInflater;
        private int[] image_resources = {R.drawable.onboarding1, R.drawable.onboarding2, R.drawable.onboarding3};

        @Override
        public int getCount() {
            return image_resources.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }


        public CouponViewPagerAdapter(Context context) {
            this.ctx = context;

        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View itemview = layoutInflater.inflate(R.layout.swipe_layout, container, false);
            ImageView imageView = (ImageView) itemview.findViewById(R.id.slider_imageview);
            imageView.setImageResource(image_resources[position]);
            container.addView(itemview);


            return itemview;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout) object);
        }


    }

    public void showdialog() {

        dialog = new Dialog(this, android.R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.coupon_code_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP)
                    finish();
                return false;
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final TextInputEditText couponEdittext = (TextInputEditText) dialog.findViewById(R.id.edittext_couponcode);
        couponEdittext.setTypeface(typeface);


        final Button cancelButton = (Button) dialog.findViewById(R.id.cancel_button);
        cancelButton.setTypeface(typeface);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == cancelButton) {

                    dialog.dismiss();

                }
            }
        });
        Button applyButton = (Button) dialog.findViewById(R.id.apply_button);
        applyButton.setTypeface(typeface);
        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                makePaymentButton.setText("Continue");
                Iterator<Coupon> iterator = couponList.iterator();
                sessionManager.setUserType("paid");
                if (couponEdittext.getText().toString().isEmpty()) {

                    Toast.makeText(CouponActivity.this, "Enter the coupon  ", Toast.LENGTH_SHORT).show();
                } else {
                    Log.e(TAG, "onClick: " + sessionManager.getCurrentKid() + " " + couponEdittext.getText().toString().trim().toUpperCase());
                    if (kidUser == null)
                        checkCode(sessionManager.getCurrentKid(), couponEdittext.getText().toString().trim().toUpperCase());
                    else
                        checkCode(kidUser.getId(), couponEdittext.getText().toString().trim().toUpperCase());

                }
            }
        });


        dialog.getWindow().setLayout(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
        if (!(CouponActivity.this).isFinishing()) {

            dialog.show();    //show dialog
        }


    }


    public void checkCode(final int kid_id, String code) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        UrlService service = retrofit.create(UrlService.class);
        Call<Coupon> call = service.giveUserFullAccessCall(kid_id, code);
        call.enqueue(new Callback<Coupon>() {
            @Override
            public void onResponse(Call<Coupon> call, Response<Coupon> response) {
                Log.e(TAG, "onResponse: " + new GsonBuilder().setPrettyPrinting().create().toJson(response));
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getAccessLevel().equals("FULL")) {
                        userDatabaseHandler.setUserType(kid_id);


                        Toast.makeText(CouponActivity.this, "Successfully applied Coupon.", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(CouponActivity.this, BlackboardActivity.class));

                    } else {
                        dialog.dismiss();
                        Toast.makeText(CouponActivity.this, "Incorrect Coupon", Toast.LENGTH_SHORT).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<Coupon> call, Throwable t) {

            }
        });
    }


}