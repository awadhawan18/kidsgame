package com.curieo.podcast.onboarding.payment;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by saransh on 1/11/17.
 */

public class CouponDatabaseHandler extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;

    public static final String COUPON_DATABASE_NAME = "couponDatabase";
    private static final String COUPON_TABLE = "couponTable";
    private static final String KEY_ID = "id";
    private static final String KEY_COUPON = "code";
    private static final String KEY_ISUSED = "used";
    private static final String KEY_ACCESS = "accessLevel";
    private static final String KID_ID = "kidId";
    private static final String COUPON_USED = "";


    public CouponDatabaseHandler(Context context) {
        super(context, COUPON_DATABASE_NAME, null, DATABASE_VERSION);
    }

    public CouponDatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_GAME_TABLE = "CREATE TABLE " + COUPON_TABLE + "( " + KEY_ID + " TEXT, " + KEY_COUPON + " TEXT, " + KEY_ISUSED + " TEXT " + " )";
        db.execSQL(CREATE_GAME_TABLE);


    }








    public void addCoupon(Coupon object) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(KEY_COUPON, object.getCode());
        contentValues.put(KEY_ISUSED, object.getUsed());
        db.insert(COUPON_TABLE, null, contentValues);
    }
/*

    public int updateCoupon(Coupon coupon) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_COUPON, coupon.getCode());
        contentValues.put(KEY_ISUSED, coupon.getUsed());
        return sqLiteDatabase.update(COUPON_TABLE, contentValues, KEY_ID + " = ?", new String[]{coupon.get_id()});
    }

*/

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + COUPON_TABLE);
        onCreate(db);
    }


    public int getCouponCount() {
        String countQuery = "SELECT  * FROM " + COUPON_TABLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;

    }

//
//    public List<Coupon> getAllCoupon() {
//        List<Coupon> couponsList = new ArrayList<>();
//        String coupon = "SELECT * FROM " + COUPON_TABLE;
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(coupon, null);
//        if (cursor.moveToFirst()) {
//            do {
//                Coupon coupon1 = new Coupon(cursor.getString(0), cursor.getString(1), cursor.getInt(2) > 0);
//                couponsList.add(coupon1);
//            } while (cursor.moveToNext());
//        }
//
//        return couponsList;
//    }

    public void deleteTable() {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("delete from " + COUPON_TABLE);

    }
}
