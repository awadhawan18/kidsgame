package com.curieo.podcast.onboarding.payment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by saransh on 21/11/17.
 */

public class Coupon {

    public Coupon() {
    }


    public Coupon(String code, boolean _couponIsUsed) {
        this.code = code;
        this.used = _couponIsUsed;

    }

    public Coupon(int kid, String _couponCode, boolean _couponIsUsed) {
        this.kidId = kid;
        this.code = _couponCode;
        this.used = _couponIsUsed;

    }




    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("access_level")
    @Expose
    private String accessLevel;
    @SerializedName("validity")
    @Expose
    private String validity;
    @SerializedName("kid_id")
    @Expose
    private int kidId;
    @SerializedName("used")
    @Expose
    private Boolean used;

    @SerializedName("message")
    @Expose
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(String accessLevel) {
        this.accessLevel = accessLevel;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public int getKidId() {
        return kidId;
    }

    public void setKidId(int kidId) {
        this.kidId = kidId;
    }

    public Boolean getUsed() {
        return used;
    }

    public void setUsed(Boolean used) {
        this.used = used;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

