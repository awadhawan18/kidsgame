package com.curieo.podcast.onboarding.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.curieo.podcast.games.AndroidLauncher;
import com.curieo.podcast.R;
import com.curieo.podcast.radio.RadioActivity;
import com.curieo.podcast.onboarding.ForceUpdateChecker;
import com.curieo.podcast.onboarding.data.KidUser;
import com.curieo.podcast.onboarding.data.ParentUser;
import com.curieo.podcast.onboarding.data.SessionManager;
import com.curieo.podcast.onboarding.data.UserDatabaseHandler;

import java.util.List;

import io.fabric.sdk.android.Fabric;


public class BlackboardActivity extends AppCompatActivity implements View.OnClickListener, ForceUpdateChecker.OnUpdateNeededListener {


    private static final String TAG = "BlackboardActivity";
    public Button radio;
    TextView letsplay;
    Typeface typeface;
    ImageView englishLearning;
    UserDatabaseHandler userDatabaseHandler;
    ImageView profile_edit;

    ViewDialog viewDialog;
    List<ParentUser> parentUsers;
    List<KidUser> allKids;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blackboard);
        userDatabaseHandler = new UserDatabaseHandler(this);

        allKids = userDatabaseHandler.getAllKids();
        Log.e(TAG, "onCreate: all kids size " + allKids.size());
        viewDialog = new ViewDialog();
        SessionManager sessionManager = new SessionManager(this);
        Log.e(TAG, "onCreate: " + sessionManager.getCurrentKid());

        parentUsers = userDatabaseHandler.getParents();
        allKids = userDatabaseHandler.getAllKids();
//current kid

        LogData.logKidsData(allKids, "BlackboardActivity");
        LogData.logParentsData(parentUsers, "BlackboardActivity");
        typeface = Typeface.createFromAsset(getAssets(), "roboto-medium.ttf");


        profile_edit = findViewById(R.id.all_profile);
        englishLearning = findViewById(R.id.english_leaning_imagview);
        letsplay = findViewById(R.id.letsplay);
        profile_edit.setOnClickListener(this);
        radio = findViewById(R.id.radio_button_id);

        radio.setTypeface(typeface);
        letsplay.setTypeface(typeface);
        radio.setOnClickListener(this);
        englishLearning.setOnClickListener(this);


        ForceUpdateChecker.with(this).onUpdateNeeded(this).check();


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent a = new Intent(Intent.ACTION_MAIN);

        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(a);
        finish();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.radio_button_id:
                startActivity(new Intent(this, RadioActivity.class));

                break;

            case R.id.english_leaning_imagview:


                Intent intent = new Intent(BlackboardActivity.this, AndroidLauncher.class);
                startActivity(intent);
                break;

            case R.id.all_profile:
                viewDialog.showProfiles(this, allKids, 0);
                break;
        }
    }

    @Override
    public void onUpdateNeeded(final String updateUrl) {

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available").setCancelable(false)
                .setMessage("We have added various new exercises for your kid. Update the application to check them. ")
                .setPositiveButton("Update", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        redirectStore(updateUrl);
                    }
                }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).create();
        dialog.show();

    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}