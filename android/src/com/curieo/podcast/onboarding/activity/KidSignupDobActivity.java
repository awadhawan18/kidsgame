package com.curieo.podcast.onboarding.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import com.curieo.podcast.R;
import com.curieo.podcast.onboarding.data.SessionManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;



public class KidSignupDobActivity extends AppCompatActivity implements View.OnClickListener {


    private static final String TAG = "KidSignupDobActivity";
    EditText dob_picker;
    ImageView kid_signup;
    SessionManager sessionManager;
    Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kid_signup_dob_activity);


        sessionManager = new SessionManager(this);
        dob_picker = findViewById(R.id.kid_dob_picker);
        kid_signup = findViewById(R.id.signup_kid_dob);
        dob_picker.setOnClickListener(this);
        kid_signup.setOnClickListener(this);

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                view.setMaxDate(myCalendar.getTimeInMillis());

                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dob_picker.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signup_kid_dob:
                Intent intent = new Intent(this, KidSignupGenderActivity.class);
                sessionManager.setUserKidsDob(dob_picker.getText().toString());

                startActivity(intent);

                break;
            case R.id.kid_dob_picker:
                new DatePickerDialog(KidSignupDobActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        }

    }
}
