package com.curieo.podcast.onboarding.activity;

import android.util.Log;

import com.curieo.podcast.onboarding.data.KidUser;
import com.curieo.podcast.onboarding.data.ParentUser;
import com.curieo.podcast.onboarding.data.UserDatabaseHandler;

import java.util.List;

/**
 * Created by saransh on 26/4/18.
 */

public class LogData {


    public  static void logKidsData(List<KidUser> allKids, String tag) {
        for (int i = 0; i < allKids.size(); i++) {
            Log.e(tag, "onCreate: kid_id: " + allKids.get(i).getId() + " parent_id: " + allKids.get(i).getUid() + " kid_name: " + allKids.get(i).getKidName() + " gender: "
                    + allKids.get(i).getGender() + " dob: " + allKids.get(i).getDateOfBirth() + " grade: " + allKids.get(i).getGrade() + " school: " + allKids.get(i).getSchool()
                    + " lastanswertime: " + allKids.get(i).getLastExerciseTime() + " lastKid:  " + allKids.get(i).getLastExerciseAnswered() + "user: " + " " + allKids.get(i).isActive());


        }

    }

    public static void logParentsData(List<ParentUser> parentUsers, String tag) {
        for (int i = 0; i < parentUsers.size(); i++) {
            Log.e(tag, "onCreate: id " + parentUsers.get(i).getId() + " parent name" + parentUsers.get(i).getParentName());

        }

    }
}
