package com.curieo.podcast.onboarding.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.curieo.podcast.R;
import com.curieo.podcast.UrlService;
import com.curieo.podcast.onboarding.data.KidUser;
import com.curieo.podcast.onboarding.data.SessionManager;
import com.curieo.podcast.onboarding.data.UserDatabaseHandler;
import com.curieo.podcast.onboarding.payment.CouponActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by agarw on 21-12-2017.
 */

public class KidSignupClassGradeActivity extends AppCompatActivity implements View.OnClickListener {


    private static final String TAG = "KidSignupClassGrade";
    Button kid_class_one, kid_class_two, kid_class_three, kid_class_four, kid_class_five;
    Typeface typeface;
    SessionManager sessionManager;
    TextView textView;
    ImageView back_button;

    UserDatabaseHandler userDatabaseHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kid_signup_class_grade);
        initView();
    }


    public void initView() {
        typeface = Typeface.createFromAsset(getResources().getAssets(), "asparagussprouts.ttf");
        back_button = findViewById(R.id.back_button_image);
        userDatabaseHandler = new UserDatabaseHandler(this);
        sessionManager = new SessionManager(this);
        kid_class_five = findViewById(R.id.juniorschool_grade_5);
        kid_class_four = findViewById(R.id.juniorschool_grade_4);
        kid_class_three = findViewById(R.id.juniorschool_grade_3);
        kid_class_two = findViewById(R.id.juniorschool_grade_2);
        kid_class_one = findViewById(R.id.juniorschool_grade_1);
        textView = findViewById(R.id.juniorschool_textview);
        textView.setTypeface(typeface);
        kid_class_five.setTypeface(typeface);
        kid_class_four.setTypeface(typeface);
        kid_class_three.setTypeface(typeface);
        kid_class_two.setTypeface(typeface);
        kid_class_one.setTypeface(typeface);

        kid_class_one.setOnClickListener(this);
        kid_class_two.setOnClickListener(this);
        kid_class_three.setOnClickListener(this);
        kid_class_four.setOnClickListener(this);
        kid_class_five.setOnClickListener(this);
        back_button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.juniorschool_grade_1:
                addtoDatabase("1");
                break;
            case R.id.juniorschool_grade_2:
                addtoDatabase("2");

                break;
            case R.id.juniorschool_grade_3:
                addtoDatabase("3");

                break;
            case R.id.juniorschool_grade_4:
                addtoDatabase("4");

                break;
            case R.id.juniorschool_grade_5:

                addtoDatabase("5");
                break;
            case R.id.back_button_image:

                finish();
                break;
            default:
        }
    }

    public boolean hasSchool() {

        return false;
    }


    public void addtoDatabase(String grade) {


        sessionManager.setKidSignedUp(true);
        sessionManager.createKidsLoginSession(sessionManager.getChildDetails().get(SessionManager.USER_KIDS_NAME),
                sessionManager.getChildDetails().get(SessionManager.USER_KIDS_GENDER), grade, sessionManager.getChildDetails().get(SessionManager.USER_KIDS_SCHOOL), sessionManager.getChildDetails().get(SessionManager.USER_KIDS_DOB));
        postdata(sessionManager.getParent(), sessionManager.getChildDetails().get(SessionManager.USER_KIDS_NAME), sessionManager.getChildDetails().get(SessionManager.USER_KIDS_DOB),
                sessionManager.getChildDetails().get(SessionManager.USER_KIDS_GENDER), grade, sessionManager.getChildDetails().get(SessionManager.USER_KIDS_SCHOOL));
    }


    public void postdata(Integer uid, String kidname, String dob, String gender, String grade, String school) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        UrlService service = retrofit.create(UrlService.class);
        Call<KidUser> call = service.addKid(kidname, dob, gender, grade, school, uid);
        call.enqueue(new Callback<KidUser>() {
            @Override
            public void onResponse(Call<KidUser> call, Response<KidUser> response) {

                if (response.isSuccessful() && response.body() != null) {

                    userDatabaseHandler.addKid(new KidUser(response.body().getId(), response.body().getKidName(), response.body().getDateOfBirth(), response.body().getGender(), response.body().getGrade(), response.body().getSchool(), "F", response.body().getUid(), true));

                    userDatabaseHandler.updateActiveUser(response.body().getId());
                    sessionManager.setCurrentKid(response.body().getId());

                    if (userDatabaseHandler.getKidUserType(response.body().getId()).equals("F")) {
                        startActivity(new Intent(KidSignupClassGradeActivity.this, CouponActivity.class));
                    } else {
                        startActivity(new Intent(KidSignupClassGradeActivity.this, BlackboardActivity.class));

                    }


                }
            }

            @Override
            public void onFailure(Call<KidUser> call, Throwable t) {

            }
        });

    }


}
