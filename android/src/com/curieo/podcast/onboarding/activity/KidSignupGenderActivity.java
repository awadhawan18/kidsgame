package com.curieo.podcast.onboarding.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.curieo.podcast.R;
import com.curieo.podcast.onboarding.data.SessionManager;


public class KidSignupGenderActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView boy_imageView;
    private ImageView girlImageView;
    TextView selectgender_textview;
    SessionManager sessionManager;
    Typeface typeface;
    ImageView backbutton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kid_signup_gender);
        sessionManager = new SessionManager(this);
        typeface = Typeface.createFromAsset(getResources().getAssets(), "asparagussprouts.ttf");
        boy_imageView = (ImageView) findViewById(R.id.boy_gender_image);
        girlImageView = (ImageView) findViewById(R.id.girl_gender_image);
        selectgender_textview = (TextView) findViewById(R.id.select_gender);
        selectgender_textview.setTypeface(typeface);
        boy_imageView.setOnClickListener(this);
        girlImageView.setOnClickListener(this);
        backbutton = (ImageView) findViewById(R.id.back_button_image);
        backbutton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.boy_gender_image:
                sessionManager.setUserKidsGender("MALE");

                startActivity(new Intent(KidSignupGenderActivity.this, KidSignupSchoolActivity.class));

                break;

            case R.id.girl_gender_image:
                sessionManager.setUserKidsGender("FEMALE");
                startActivity(new Intent(KidSignupGenderActivity.this, KidSignupSchoolActivity.class));


                break;
            case R.id.back_button_image:
                finish();
                break;
            default:
        }
    }
}
