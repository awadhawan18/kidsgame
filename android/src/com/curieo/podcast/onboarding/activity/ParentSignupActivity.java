package com.curieo.podcast.onboarding.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.curieo.podcast.R;
import com.curieo.podcast.UrlService;
import com.curieo.podcast.onboarding.data.KidUser;
import com.curieo.podcast.onboarding.data.ParentUser;
import com.curieo.podcast.onboarding.data.SessionManager;
import com.curieo.podcast.onboarding.data.UserDatabaseHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ParentSignupActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "ParentSignupActivity";
    SessionManager sessionManager;

    String userId;
    EditText nameEditText;
    Button signupButton;
    Typeface typeface;
    TextView textView;
    Intent intent;
    String token;
    UserDatabaseHandler userDatabaseHandler;
    UrlService service;

    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_signup_activty);
        sessionManager = new SessionManager(this);
        userDatabaseHandler = new UserDatabaseHandler(this);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(UrlService.class);
        typeface = Typeface.createFromAsset(getResources().getAssets(), "asparagussprouts.ttf");
        textView = findViewById(R.id.verify_mobile);
        spinner = (ProgressBar) findViewById(R.id.progressBar1);
        signupButton = findViewById(R.id.signup_button);
        nameEditText = findViewById(R.id.name_edit_text);
        intent = getIntent();
        token = intent.getStringExtra("token");
        Log.e(TAG, "onCreate: " + token);
        signupButton.setOnClickListener(this);

        signupButton.setTypeface(typeface);
        nameEditText.setTypeface(typeface);
        textView.setTypeface(typeface);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signup_button:
                if (nameEditText.getText().toString().isEmpty()) {

                    Toast.makeText(ParentSignupActivity.this, "Please enter your name", Toast.LENGTH_SHORT).show();

                } else if (nameEditText.getText().toString().length() < 3) {
                    Toast.makeText(ParentSignupActivity.this, "Please enter your full name", Toast.LENGTH_SHORT).show();

                } else {

                    postdata(nameEditText.getText().toString(), token);

                }


                break;

        }
    }


    public void postdata(final String name, String token) {
        spinner.setVisibility(View.VISIBLE);
        Call<ParentUser> call = service.createParent(name, token);
        call.enqueue(new Callback<ParentUser>() {
            @Override
            public void onResponse(Call<ParentUser> call, Response<ParentUser> response) {

                if (response.isSuccessful() && response.body() != null) {

                    Log.e(TAG, "onResponse: id  " + response.body().getId());
                    Log.e(TAG, "onResponse: new user" + response.body().isNewuser());
                    Log.e(TAG, "onResponse: message" + response.body().getMessage());
                    spinner.setVisibility(View.GONE);

                    saveAndNext(response.body().getId(), name, response.body().isNewuser());


                }
            }

            @Override
            public void onFailure(Call<ParentUser> call, Throwable t) {

            }
        });

    }


    public void saveAndNext(Integer id, String name, Boolean isNewUser) {


        sessionManager.createParentLoginSession(id, "free", nameEditText.getText().toString());
        sessionManager.setIsParentSignedUp(true);
        userDatabaseHandler.addParent(new ParentUser(id, name));
        if (isNewUser) {

            Toast.makeText(ParentSignupActivity.this, "New user created", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ParentSignupActivity.this, BlackboardActivity.class);
            startActivity(intent);
        } else {
            spinner.setVisibility(View.VISIBLE);
            Toast.makeText(ParentSignupActivity.this, "Welcome back, Please wait while be load your data", Toast.LENGTH_SHORT).show();
            Call<List<KidUser>> call = service.getParentsKids(sessionManager.getParent(), "json");

            call.enqueue(new Callback<List<KidUser>>() {
                @Override
                public void onResponse(Call<List<KidUser>> call, Response<List<KidUser>> response) {
                    spinner.setVisibility(View.GONE);

                    if (response.isSuccessful() && response.body() != null) {
                        Log.e(TAG, "onResponse: " + new GsonBuilder().setPrettyPrinting().create().toJson(response));
                        List<KidUser> allKids = response.body();
                        if (allKids != null) {
                            for (KidUser kid : allKids) {
                                userDatabaseHandler.addKid(kid);

                            }
                        }
                        LogData.logKidsData(allKids, "ParentSignupActivity");

                        if (lastAnsweredTime(allKids) > 0) {
                            userDatabaseHandler.updateActiveUser(lastAnsweredTime(allKids));

                            sessionManager.setCurrentKid(lastAnsweredTime(allKids));
                        }

                        startActivity(new Intent(ParentSignupActivity.this, BlackboardActivity.class));

                    } else {
                        Toast.makeText(ParentSignupActivity.this, "Error, Please try again", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<List<KidUser>> call, Throwable t) {

                }
            });
        }


    }


    private static Integer lastAnsweredTime(List<KidUser> lastExerciseTime) {


        long highest = 0;
        Integer highestuserId = 0;
        for (int i = 0; i < lastExerciseTime.size(); i++) {
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
            try {
                if (lastExerciseTime.get(i).getLastExerciseTime() != null) {
                    Date d = sdf.parse(lastExerciseTime.get(i).getLastExerciseTime());
                    sdf.setTimeZone(TimeZone.getTimeZone("IST"));
                    if (d.getTime() > highest) {
                        highest = d.getTime();
                        highestuserId = lastExerciseTime.get(i).getId();
                        Log.e(TAG, "lastAnsweredTime: " + highestuserId + "  highest" + d.getTime());
                    }
                }

            } catch (ParseException e) {

                e.printStackTrace();
            }
        }


        return highestuserId;


    }
}