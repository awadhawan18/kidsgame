package com.curieo.podcast.onboarding.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.curieo.podcast.R;
import com.curieo.podcast.onboarding.data.SessionManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;


public class PhoneAuthActivity2 extends AppCompatActivity implements
        View.OnClickListener {

    private static final String TAG = "PhoneAuthActivity2";

    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";

    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_CODE_SENT = 2;
    private static final int STATE_VERIFY_FAILED = 3;
    private static final int STATE_VERIFY_SUCCESS = 4;
    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;
    String intentPhoneNumber;
    Intent intent;
    SharedPreferences pref;
    SessionManager sessionManager;
    Typeface typeface;
    ImageView backButton;
    FirebaseAuth firebaseAuth;
    TextView textView;
    private boolean flag = true;
    //   SweetAlertDialog pDialog;
    private FirebaseAuth mAuth;
    private boolean mVerificationInProgress = false;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private Button mVerifyButton;
    private TextView mResendButton, mChnageNumber;
    private EditText mVerificationField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_auth2);
        firebaseAuth = FirebaseAuth.getInstance();
        if (savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState);
        }



        textView = findViewById(R.id.verify_the_otp1);
        typeface = Typeface.createFromAsset(getResources().getAssets(), "asparagussprouts.ttf");
        backButton = findViewById(R.id.back_button_image);
        backButton.setOnClickListener(this);


        intentPhoneNumber = getIntent().getStringExtra("phone_number");


        //pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        //      pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        // pDialog.setTitleText("We are setting you up!");
        //  pDialog.setCancelable(false);

        sessionManager = new SessionManager(this);
        pref = getSharedPreferences("avatar", MODE_PRIVATE);
        mVerificationField = findViewById(R.id.field_verification_code1);
        mChnageNumber = findViewById(R.id.edit_number1);
        mVerifyButton = findViewById(R.id.button_verify_phone1);
        mResendButton = findViewById(R.id.button_resend1);


        textView.setTypeface(typeface);
        mChnageNumber.setTypeface(typeface);

        mVerifyButton.setTypeface(typeface);
        mResendButton.setTypeface(typeface);
        mVerificationField.setTypeface(typeface);

        mVerifyButton.setOnClickListener(this);
        mResendButton.setOnClickListener(this);
        mChnageNumber.setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                Log.d(TAG, "onVerificationCompleted:" + credential.getProvider() + " " + credential.getSmsCode() + " ");
                // [START_EXCLUDE silent
                mVerificationInProgress = false;
                updateUI(STATE_VERIFY_SUCCESS, credential);
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.w(TAG, "onVerificationFailed", e);
                mVerificationInProgress = false;

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    //   mPhoneNumberField.setError("Invalid phone intentPhoneNumber.");
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
                            Snackbar.LENGTH_SHORT).show();
                }

                updateUI(STATE_VERIFY_FAILED);
            }


            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                Log.d(TAG, "onCodeSent:" + verificationId);

                mVerificationId = verificationId;
                mResendToken = token;

                updateUI(STATE_CODE_SENT);
            }
        };


        startPhoneNumberVerification(intentPhoneNumber);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
/*        if (currentUser != null) {
            currentUser.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                @Override
                public void onComplete(@NonNull Task<GetTokenResult> task) {
                    Log.e(TAG, "onComplete: " + task.getResult().getToken());
                }
            });

        }*/
        updateUI(currentUser);

        // [START_EXCLUDE]
        if (mVerificationInProgress) {
            startPhoneNumberVerification(intentPhoneNumber);
        }
        // [END_EXCLUDE]
    }
    // [END on_start_check_user]

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_VERIFY_IN_PROGRESS, mVerificationInProgress);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mVerificationInProgress = savedInstanceState.getBoolean(KEY_VERIFY_IN_PROGRESS);
    }


    private void startPhoneNumberVerification(String phoneNumber) {

        // [START start_phone_auth]
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone intentPhoneNumber to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]


        mVerificationInProgress = true;

    }


    private void verifyPhoneNumberWithCode(String verificationId, String code) {

        try {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
            signInWithPhoneAuthCredential(credential);

        } catch (Exception credential) {
            Log.e(TAG, "verifyPhoneNumberWithCode:error ");
        }
    }

    // [START resend_verification]
    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        if (phoneNumber == null) {
            Toast.makeText(this, "Please Enter your phone phoneNumber", Toast.LENGTH_SHORT).show();
        } else if (phoneNumber.length() != 10) {
            Toast.makeText(this, "Please Enter a valid phone number", Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(this, "Resending OTP, please wait", Toast.LENGTH_SHORT).show();

            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    phoneNumber,        // Phone intentPhoneNumber to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    this,               // Activity (for callback binding)
                    mCallbacks,         // OnVerificationStateChangedCallbacks
                    token);             // ForceResendingToken from callbacks
        }
    }
    // [END resend_verification]


    // [START sign_in_with_phone]
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        if (!(PhoneAuthActivity2.this).isFinishing()) {
            //   pDialog.show();
        }


        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful() && flag) {
                            Toast.makeText(PhoneAuthActivity2.this, "Phone Verified Successfully", Toast.LENGTH_SHORT).show();
                            flag = false;
                            FirebaseUser user = task.getResult().getUser();
                        //    sessionManager.setUserId(user.getPhoneNumber());
                            user.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                                @Override
                                public void onComplete(@NonNull Task<GetTokenResult> task) {
                                    Intent intent = new Intent(PhoneAuthActivity2.this, ParentSignupActivity.class);
                                     Log.e(TAG, "onComplete: " + task.getResult().getToken());
                                    intent.putExtra("token", task.getResult().getToken());
                                    startActivity(intent);
                                    finish();
                                }
                            });


                            updateUI(STATE_SIGNIN_SUCCESS, user);
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            }
                            updateUI(STATE_SIGNIN_FAILED);
                            // [END_EXCLUDE]
                        }

                    }
                });
    }
    // [END sign_in_with_phone]

    private void signOut() {
        mAuth.signOut();
        updateUI(STATE_INITIALIZED);
    }

    private void updateUI(int uiState) {
        updateUI(uiState, mAuth.getCurrentUser(), null);
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            updateUI(STATE_SIGNIN_SUCCESS, user);
        } else {
            updateUI(STATE_INITIALIZED);
        }
    }

    private void updateUI(int uiState, FirebaseUser user) {
        updateUI(uiState, user, null);

    }


    private void updateUI(int uiState, PhoneAuthCredential cred) {
        updateUI(uiState, null, cred);
    }


    private void updateUI(int uiState, FirebaseUser user, PhoneAuthCredential cred) {
        switch (uiState) {
            case STATE_INITIALIZED:
                // Initialized state, show only the phone intentPhoneNumber field and start button
                //    disableViews(mResendButton, mVerificationField);

                break;
            case STATE_CODE_SENT:
                // Code sent state, show the verification field, the
                enableViews(mResendButton, mVerificationField);
//                disableViews();
                break;
            case STATE_VERIFY_FAILED:
                // Verification has failed, show all options
                enableViews(mVerifyButton, mResendButton,
                        mVerificationField);
                break;
            case STATE_VERIFY_SUCCESS:


                // Verification has succeeded, proceed to firebase sign in
                //        disableViews(mResendButton, mPhoneNumberField,
                //              mVerificationField);

                // Set the verification text based on the credential
                if (cred != null) {
                    if (cred.getSmsCode() != null) {
                        mVerificationField.setText(cred.getSmsCode());
                    }

                }


                break;
            case STATE_SIGNIN_FAILED:
                // No-op, handled by sign-in check
                break;
            case STATE_SIGNIN_SUCCESS:
                // Np-op, handled by sign-in check
                break;
        }

        if (user == null) {
            // Signed out


        } else {
            // Signed in

//            mVerificationField.setError("");

        }
    }


    private void enableViews(View... views) {
        for (View v : views) {
            v.setEnabled(true);
        }
    }

    private void disableViews(View... views) {
        for (View v : views) {
            v.setEnabled(false);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.button_verify_phone1:
// To dismiss the dialog

                String code = mVerificationField.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    return;
                }
                verifyPhoneNumberWithCode(mVerificationId, code);
                break;

            case R.id.button_resend1:
                resendVerificationCode(intentPhoneNumber, mResendToken);
                break;
            case R.id.edit_number1:

                Intent intent = new Intent(this, PhoneAuthActivity1.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case R.id.back_button_image:
                finish();
        }
    }

}



