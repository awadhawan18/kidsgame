package com.curieo.podcast.onboarding.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.curieo.podcast.R;
import com.curieo.podcast.UrlService;
import com.curieo.podcast.onboarding.ConnectivityReceiver;
import com.curieo.podcast.onboarding.data.KidUser;
import com.curieo.podcast.onboarding.data.ParentUser;
import com.curieo.podcast.onboarding.data.SessionManager;
import com.curieo.podcast.onboarding.data.UserDatabaseHandler;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class SplashActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    private static final String TAG = "SplashActivity";
    UrlService service;
    SessionManager sessionManager;
    UserDatabaseHandler userDatabaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sessionManager = new SessionManager(this);
        userDatabaseHandler = new UserDatabaseHandler(this);
        //toDeleteFunction();


    }


    public void toDeleteFunction() {

        sessionManager.createParentLoginSession(6, "free", "Saransh");
        sessionManager.setIsParentSignedUp(true);
        userDatabaseHandler.addParent(new ParentUser(6, "Saransh"));
        sessionManager.setCurrentKid(61);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(UrlService.class);

        Call<List<KidUser>> call = service.getParentsKids(6, "json");


        call.enqueue(new Callback<List<KidUser>>() {
            @Override
            public void onResponse(Call<List<KidUser>> call, Response<List<KidUser>> response) {


                if (response.isSuccessful() && response.body() != null) {
                    Log.e(TAG, "onResponse: " + new GsonBuilder().setPrettyPrinting().create().toJson(response));
                    List<KidUser> allKids = response.body();
                    if (allKids != null && userDatabaseHandler.getkidCount() <= 0) {
                        for (KidUser kid : allKids) {
                                userDatabaseHandler.addKid(kid);

                        }
                        userDatabaseHandler.updateActiveUser(61);

                    }
                    LogData.logKidsData(allKids, "ParentSignupActivity");


                    startActivity(new Intent(SplashActivity.this, BlackboardActivity.class));

                } else {
                    Toast.makeText(SplashActivity.this, "Error, Please try again", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<List<KidUser>> call, Throwable t) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

//removed for testing
        if (sessionManager.isParentSignedUp()) {

        }

        sessionManager.checkLogin();
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            Intent i = new Intent(this, NoInternetActivity.class);
            startActivity(i);

        }
    }


}