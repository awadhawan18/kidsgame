package com.curieo.podcast.onboarding.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.curieo.podcast.R;
import com.curieo.podcast.onboarding.data.SessionManager;


public class KidSignupNameActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    Typeface typeface;
    ImageView back_button;
    private SessionManager sessionManager;
    private EditText kidname;
    private ImageView image;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kids_signup_name);

        initView();

    }

    private void initView() {
        typeface = Typeface.createFromAsset(getResources().getAssets(), "asparagussprouts.ttf");
        TextView textView = findViewById(R.id.kids_signup);
        TextView textView1 = findViewById(R.id.enter_name);
        back_button = findViewById(R.id.back_button_image);
        back_button.setOnClickListener(this);
        textView.setTypeface(typeface);
        textView1.setTypeface(typeface);
        sessionManager = new SessionManager(this);
        kidname = findViewById(R.id.input_name_kid);
        image = findViewById(R.id.signup_kid);
        kidname.setTypeface(typeface);
        image.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.signup_kid:

                if (kidname.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please enter your kids name ", Toast.LENGTH_SHORT).show();
                } else if (kidname.getText().toString().length() < 3) {
                    Toast.makeText(this, "Please enter your kids full name ", Toast.LENGTH_SHORT).show();
                } else {
                    sessionManager.setUserKidsName(kidname.getText().toString());
                    startActivity(new Intent(this, KidSignupDobActivity.class));

                }
                break;
            case R.id.back_button_image:
                finish();
                break;

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
