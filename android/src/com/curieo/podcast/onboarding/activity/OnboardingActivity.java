package com.curieo.podcast.onboarding.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.curieo.podcast.R;

public class OnboardingActivity extends AppCompatActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {

    public Button button;
    private ViewPager mViewPager;
    private OnboardingViewPagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mViewPager = findViewById(R.id.intro_demo);
        mPagerAdapter = new OnboardingViewPagerAdapter(this);
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(this);

        button = findViewById(R.id.get_started);
        button.setOnClickListener(this);

    }

    @Override
    public void onClick(final View v) {

        switch (v.getId()) {
            case R.id.get_started:

                if (mViewPager.getCurrentItem() == 2) {
                    startActivity(new Intent(OnboardingActivity.this, PhoneAuthActivity1.class));
                    finish();
                } else {
                    mViewPager.setCurrentItem(getItem(1), true);
                }
                break;

        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private int getItem(int i) {
        return mViewPager.getCurrentItem() + i;
    }

    private static class OnboardingViewPagerAdapter extends PagerAdapter {


        Context ctx;
        private LayoutInflater layoutInflater;
        private int[] image_resources = {R.drawable.onboarding1, R.drawable.onboarding2, R.drawable.onboarding3};

        public OnboardingViewPagerAdapter(Context context) {
            this.ctx = context;

        }

        @Override
        public int getCount() {
            return image_resources.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View itemview = layoutInflater.inflate(R.layout.swipe_layout, container, false);
            ImageView imageView = itemview.findViewById(R.id.slider_imageview);
            imageView.setImageResource(image_resources[position]);
            container.addView(itemview);


            return itemview;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout) object);
        }


    }


}
