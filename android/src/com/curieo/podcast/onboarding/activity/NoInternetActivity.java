package com.curieo.podcast.onboarding.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.curieo.podcast.R;
import com.curieo.podcast.onboarding.ConnectivityReceiver;


public class NoInternetActivity extends AppCompatActivity implements View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener {
    Button checkConnectionButton;

    public static void Call(Activity activity) {
        boolean isConnected = ConnectivityReceiver.isConnected();
        if (!isConnected) {

            Intent intent = new Intent(activity, NoInternetActivity.class);
            activity.startActivity(intent);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet);
        checkConnectionButton = findViewById(R.id.retry);
        checkConnectionButton.setOnClickListener(this);

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }

    private void showSnack(boolean isConnected) {
        String message;
        if (isConnected) {
            message = "Good! Connected to Internet";
        } else {
            message = "Sorry! Not connected to internet";

        }
        Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onClick(View v) {
        if (v == checkConnectionButton) {
            boolean isConnected = ConnectivityReceiver.isConnected();
            if (isConnected) {
                Intent intent = new Intent(this, BlackboardActivity.class);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(this, "Sorry! Not connected to internet", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);

    }
}
