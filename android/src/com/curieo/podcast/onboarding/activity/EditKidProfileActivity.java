package com.curieo.podcast.onboarding.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.curieo.podcast.R;
import com.curieo.podcast.onboarding.MySpinnerAdapter;
import com.curieo.podcast.onboarding.data.KidUser;
import com.curieo.podcast.onboarding.data.UserDatabaseHandler;
import com.curieo.podcast.onboarding.payment.CouponActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


public class EditKidProfileActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = "EditKidProfileActivity";
    DatePickerDialog.OnDateSetListener date;
    Spinner genderSpinner, gradeSpinner, schoolSpinner;
    Calendar myCalendar = Calendar.getInstance();
    EditText kid_name, kid_dob;
    TextView user_type;
    KidUser kidUser;

    LinearLayout editLinearLaoyout, applyLinearLayour;
    Button editButton, cancelButton, updateButton;
    Intent intent;
    TextView textView;
    Typeface typeface;
    UserDatabaseHandler userDatabaseHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_kid_profile);
        intent = getIntent();
        kidUser = (KidUser) intent.getSerializableExtra("KidUser");
        kid_dob = findViewById(R.id.kid_dob);
        userDatabaseHandler = new UserDatabaseHandler(this);
        typeface = Typeface.createFromAsset(getResources().getAssets(), "asparagussprouts.ttf");
        initViews();

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                view.setMaxDate(myCalendar.getTimeInMillis());

                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };


    }

    private void initViews() {

        textView = findViewById(R.id.textview_edit_profile);
        editLinearLaoyout = findViewById(R.id.edit_ll);
        applyLinearLayour = findViewById(R.id.apply_ll);
        editButton = findViewById(R.id.edit_kid);
        cancelButton = findViewById(R.id.cancel_Kid);
        updateButton = findViewById(R.id.appl_kid);
        genderSpinner = findViewById(R.id.kid_gender);
        gradeSpinner = findViewById(R.id.kid_grade);
        schoolSpinner = findViewById(R.id.kid_school);
        kid_name = findViewById(R.id.kid_name);
        user_type = findViewById(R.id.usertype);

        user_type.setOnClickListener(this);

        editButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        updateButton.setOnClickListener(this);
        kid_dob.setOnClickListener(this);


        ;
        editButton.setTypeface(typeface);
        cancelButton.setTypeface(typeface);
        updateButton.setTypeface(typeface);
        textView.setTypeface(typeface);
        user_type.setTypeface(typeface);
        kid_name.setTypeface(typeface);
        kid_dob.setTypeface(typeface);
        spinnerInitialize();
        viewsAccess(false);

        changeData(kidUser);
    }

    List<String> schoolArray;

    public void viewsAccess(boolean b) {
        genderSpinner.setEnabled(b);
        gradeSpinner.setEnabled(b);
        schoolSpinner.setEnabled(b);
        kid_dob.setEnabled(b);
        kid_name.setEnabled(b);

    }

    private void spinnerInitialize() {

        schoolArray = new ArrayList<String>();
        schoolArray.add("Select School");
        MySpinnerAdapter schoolAdapter = new MySpinnerAdapter(this, R.layout.spinner_school_item, schoolArray);
        schoolAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        schoolSpinner.setAdapter(schoolAdapter);


        List<String> genderList = new ArrayList<String>();
        genderList.add("Male");
        genderList.add("Female");
        MySpinnerAdapter genderAdapter = new MySpinnerAdapter(this, R.layout.spinner_school_item, genderList);
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(genderAdapter);


        List<String> gradeSpinnerList = new ArrayList<>();
        gradeSpinnerList.add("Nursery");
        gradeSpinnerList.add("LKG");
        gradeSpinnerList.add("UKG");
        gradeSpinnerList.add("First");
        gradeSpinnerList.add("Second");
        gradeSpinnerList.add("Third");
        gradeSpinnerList.add("Fourth");
        gradeSpinnerList.add("Fifth");

        MySpinnerAdapter gradeAdapter = new MySpinnerAdapter(this, R.layout.spinner_school_item, gradeSpinnerList);
        gradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gradeSpinner.setAdapter(gradeAdapter);


    }


    public void changeData(KidUser kidUser) {

        kid_name.setText(kidUser.getKidName());
        kid_dob.setText(kidUser.getDateOfBirth());
        if (kidUser.getUserType().trim().equals("F")) {
            user_type.setText("User type: Free");
        } else {
            user_type.setText("User type: Paid");

            user_type.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

        }
        if (kidUser.getGender().toLowerCase().trim().contains("female")) {
            genderSpinner.setSelection(1);

        } else {
            genderSpinner.setSelection(0);
        }


        if (!kidUser.getSchool().contains("Select School")) {
            schoolArray.add(kidUser.getSchool());
            schoolSpinner.setSelection(schoolArray.size() - 1);
        }
        //to be added
        if (kidUser.getGrade().contains("Nursery")) {
            gradeSpinner.setSelection(0);

        } else if (kidUser.getGrade().contains("LKG")) {
            gradeSpinner.setSelection(1);

        } else if (kidUser.getGrade().contains("UKG")) {
            gradeSpinner.setSelection(2);

        } else if (kidUser.getGrade().contains("First")) {
            gradeSpinner.setSelection(3);

        } else if (kidUser.getGrade().contains("Second")) {

            gradeSpinner.setSelection(4);
        } else if (kidUser.getGrade().contains("Third")) {
            gradeSpinner.setSelection(5);

        } else if (kidUser.getGrade().contains("Fourth")) {
            gradeSpinner.setSelection(6);

        } else if (kidUser.getGrade().contains("Fifth")) {
            gradeSpinner.setSelection(7);

        }


        Log.e(TAG, "changeData: " + kidUser.getGender() + kidUser.getLastExerciseAnswered() + kidUser.getLastExerciseTime() + kidUser.getKidName() + kidUser.getGrade() + kidUser.getDateOfBirth() + +kidUser.getUid());
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        kid_dob.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.kid_dob:


                new DatePickerDialog(EditKidProfileActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.cancel_Kid:
                editLinearLaoyout.setVisibility(View.VISIBLE);
                applyLinearLayour.setVisibility(View.GONE);

                viewsAccess(false);
                break;
            case R.id.edit_kid:
                applyLinearLayour.setVisibility(View.VISIBLE);
                editLinearLaoyout.setVisibility(View.GONE);
                viewsAccess(true);
                break;
            case R.id.appl_kid:
                //update database
                editLinearLaoyout.setVisibility(View.VISIBLE);
                applyLinearLayour.setVisibility(View.GONE);
                viewsAccess(false);

                userDatabaseHandler.updateKid(new KidUser(kidUser.getId(), kid_name.getText().toString(), kid_dob.getText().toString(),
                        genderSpinner.getSelectedItem().toString(), gradeSpinner.getSelectedItem().toString(), schoolSpinner.getSelectedItem().toString()));


                break;
            case R.id.usertype:
                if (kidUser.getUserType().equals("F")) {
                    Intent intent = new Intent(this, CouponActivity.class);
                    intent.putExtra("KidUser", kidUser);
                    startActivity(intent);
                } else {
                    user_type.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                }
                break;


        }


    }
}

