package com.curieo.podcast.onboarding.activity;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.curieo.podcast.R;

public class PhoneAuthActivity1 extends AppCompatActivity implements View.OnClickListener {


    Typeface typeface;
    private EditText mPhoneNumberField;
    private Button signupParentButton;
    private TextView phoneAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_phone_auth1);
        initView();

    }

    private void initView() {

        typeface = Typeface.createFromAsset(getResources().getAssets(), "asparagussprouts.ttf");
        signupParentButton = findViewById(R.id.signup_parent_button);
        mPhoneNumberField = findViewById(R.id.field_phone_number);
        phoneAuth = findViewById(R.id.verify_mobile);
        phoneAuth.setTypeface(typeface);
        signupParentButton.setTypeface(typeface);
        mPhoneNumberField.setTypeface(typeface);
        signupParentButton.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signup_parent_button:
                if (mPhoneNumberField.getText().toString().isEmpty()) {
                    Toast.makeText(PhoneAuthActivity1.this, "Please enter your phone number", Toast.LENGTH_SHORT).show();

                } else if (mPhoneNumberField.getText().toString().length() != 10) {
                    Toast.makeText(PhoneAuthActivity1.this, "Please enter a valid phone number", Toast.LENGTH_SHORT).show();

                } else {
                    Intent intent = new Intent(PhoneAuthActivity1.this, PhoneAuthActivity2.class);
                    intent.putExtra("phone_number", mPhoneNumberField.getText().toString());
//                    intent.putExtra("parent_name", mNameField.getText().toString());
                    startActivity(intent);
                    finish();


                }
                break;
        }
    }
}