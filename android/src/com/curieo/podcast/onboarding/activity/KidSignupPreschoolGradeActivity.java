package com.curieo.podcast.onboarding.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.curieo.podcast.R;
import com.curieo.podcast.UrlService;
import com.curieo.podcast.onboarding.data.KidUser;
import com.curieo.podcast.onboarding.data.SessionManager;
import com.curieo.podcast.onboarding.data.UserDatabaseHandler;
import com.curieo.podcast.onboarding.payment.CouponActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by agarw on 21-12-2017.
 */

public class KidSignupPreschoolGradeActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "KidSignupPreschoolGrade";
    Button preschool_grade_a, preschool_grade_b, preschool_grade_c;
    ImageView back_button;
    Typeface typeface;
    SessionManager sessionManager;
    UserDatabaseHandler userDatabaseHandler;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kid_signup_prechool_grade);
        preschool_grade_a = findViewById(R.id.preschool_grade_a);
        preschool_grade_b = findViewById(R.id.preschool_grade_b);
        preschool_grade_c = findViewById(R.id.preschool_grade_c);
        back_button = findViewById(R.id.back_button);

        userDatabaseHandler = new UserDatabaseHandler(this);
        TextView textView = findViewById(R.id.preschool_textview);
        typeface = Typeface.createFromAsset(getResources().getAssets(), "asparagussprouts.ttf");
        textView.setTypeface(typeface);
        sessionManager = new SessionManager(this);


        preschool_grade_a.setTypeface(typeface);
        preschool_grade_b.setTypeface(typeface);

        preschool_grade_c.setTypeface(typeface);
        preschool_grade_a.setOnClickListener(this);
        preschool_grade_b.setOnClickListener(this);
        preschool_grade_c.setOnClickListener(this);
        back_button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.preschool_grade_a:
                addtoDatabase("N");

                break;
            case R.id.preschool_grade_b:

                addtoDatabase("L");
                break;

            case R.id.preschool_grade_c:
                addtoDatabase("U");
                break;
            case R.id.back_button:
                finish();
                break;
            default:
        }
    }


    public void addtoDatabase(String grade) {


        sessionManager.setKidSignedUp(true);
        sessionManager.createKidsLoginSession(sessionManager.getChildDetails().get(SessionManager.USER_KIDS_NAME),
                sessionManager.getChildDetails().get(SessionManager.USER_KIDS_GENDER), grade, sessionManager.getChildDetails().get(SessionManager.USER_KIDS_SCHOOL), sessionManager.getChildDetails().get(SessionManager.USER_KIDS_DOB));
        postdata(6, sessionManager.getChildDetails().get(SessionManager.USER_KIDS_NAME), sessionManager.getChildDetails().get(SessionManager.USER_KIDS_DOB),
                sessionManager.getChildDetails().get(SessionManager.USER_KIDS_GENDER), grade, sessionManager.getChildDetails().get(SessionManager.USER_KIDS_SCHOOL));


    }


    public void postdata(Integer uid, String kidname, String dob, String gender, String grade, String school) {


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        UrlService service = retrofit.create(UrlService.class);
        Call<KidUser> call = service.addKid(kidname, dob, gender, grade, school, uid);
        call.enqueue(new Callback<KidUser>() {
            @Override
            public void onResponse(Call<KidUser> call, Response<KidUser> response) {

                if (response.isSuccessful() && response.body() != null) {


                    userDatabaseHandler.addKid(new KidUser(response.body().getId(), response.body().getKidName(), response.body().getDateOfBirth(), response.body().getGender(), response.body().getGrade(), response.body().getSchool(), "F", response.body().getUid(), true));
                    Log.e(TAG, "onResponse: uid" + response.body().getId());
                    userDatabaseHandler.updateActiveUser(response.body().getId());
                    sessionManager.setCurrentKid(response.body().getId());
                    if (userDatabaseHandler.getKidUserType(response.body().getId()).equals("F")) {
                        startActivity(new Intent(KidSignupPreschoolGradeActivity.this, CouponActivity.class));
                    } else {
                        startActivity(new Intent(KidSignupPreschoolGradeActivity.this, BlackboardActivity.class));

                    }


                    //          startActivity(new Intent(getApplicationContext(), AndroidLauncher.class));
                } else {
                    Log.e(TAG, "onResponse: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<KidUser> call, Throwable t) {


            }
        });

    }

}
