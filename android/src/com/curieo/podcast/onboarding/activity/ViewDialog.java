package com.curieo.podcast.onboarding.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.curieo.podcast.R;
import com.curieo.podcast.onboarding.data.KidUser;
import com.curieo.podcast.onboarding.data.UserDatabaseHandler;

import java.util.List;

public class ViewDialog {


    KidUserAdapter kidUserAdapter;
    ImageView add_user;
    RecyclerView recyclerView;
    UserDatabaseHandler userDatabaseHandler;


    public void showProfiles(final Activity activity, final List<KidUser> kidUsers, final int a) {
        final Dialog dialog = new Dialog(activity);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        userDatabaseHandler = new UserDatabaseHandler(activity);
        dialog.setContentView(R.layout.dialogue_profiles);
        add_user = dialog.findViewById(R.id.add_profile);
      
        recyclerView = dialog.findViewById(R.id.list_profiles);


        add_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(activity, KidSignupNameActivity.class);
                activity.startActivity(intent);
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(layoutManager);


        kidUserAdapter = new KidUserAdapter(kidUsers, activity);
        recyclerView.setAdapter(kidUserAdapter);


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(activity, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
         /*       if (a == 0) {
                    Intent intent = new Intent(activity, EditKidProfileActivity.class);
                    intent.putExtra("KidUser", kidUsers.get(position));
                    dialog.dismiss();
                    activity.startActivity(intent);
                } else {
                    Intent intent = new Intent(activity, AndroidLauncher.class);
                    intent.putExtra("KidUser", kidUsers.get(position));
                    dialog.dismiss();
                    activity.startActivity(intent);


                }*/
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        dialog.show();

    }
}