package com.curieo.podcast.onboarding.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.curieo.podcast.R;
import com.curieo.podcast.onboarding.data.KidUser;
import com.curieo.podcast.onboarding.data.SessionManager;
import com.curieo.podcast.onboarding.data.UserDatabaseHandler;

import java.util.List;

public class KidUserAdapter extends RecyclerView.Adapter<KidUserAdapter.MyViewHolder> implements View.OnLongClickListener {

    private List<KidUser> dataSet;
    Context mContext;
    UserDatabaseHandler userDatabaseHandler;
    SessionManager sessionManager;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item, parent, false);


        sessionManager = new SessionManager(mContext);
        userDatabaseHandler = new UserDatabaseHandler(mContext);
        return new MyViewHolder(itemView, dataSet);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        KidUser kidUsers = dataSet.get(position);

        holder.kid_name.setText(kidUsers.getKidName());
        if (kidUsers.isActive())
            holder.isCurrent.setImageResource(R.drawable.correct);
        else {
            holder.isCurrent.setImageResource(R.drawable.incorrect);

        }


        holder.avatar.setOnLongClickListener(this);
        holder.kid_name.setOnLongClickListener(this);
        holder.isCurrent.setOnLongClickListener(this);

        holder.avatar.setTag(position);
        holder.isCurrent.setTag(position);
        holder.itemView.setTag(position);

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    // View lookup cache

    public KidUserAdapter(List<KidUser> data, Context context) {

        this.dataSet = data;
        this.mContext = context;

    }


    @Override
    public boolean onLongClick(View view) {

        Intent intent = new Intent(mContext, EditKidProfileActivity.class);
        intent.putExtra("KidUser", dataSet.get((Integer) view.getTag()));

        mContext.startActivity(intent);
        return true;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView kid_name;
        ImageView isCurrent;
        ImageView avatar;

        public MyViewHolder(View itemView, List<KidUser> kidUsers) {
            super(itemView);
            kid_name = (TextView) itemView.findViewById(R.id.kid_name);
            avatar = (ImageView) itemView.findViewById(R.id.avatar);
            isCurrent = (ImageView) itemView.findViewById(R.id.current_user);
            avatar.setOnClickListener(this);


        }


        @Override
        public void onClick(View view) {
            if (view == avatar) {
                Log.e("KidUserAdapter", "onClick: " + view.getTag() + " total " + getItemCount());
                userDatabaseHandler.updateActiveUser(dataSet.get((Integer) view.getTag()).getId());
                sessionManager.setCurrentKid(dataSet.get((Integer) view.getTag()).getId());
                isCurrent.setImageResource(R.drawable.correct);
            }
        }


    }
}