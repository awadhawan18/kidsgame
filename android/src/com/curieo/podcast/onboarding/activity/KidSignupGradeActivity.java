package com.curieo.podcast.onboarding.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.curieo.podcast.R;
import com.curieo.podcast.onboarding.data.SessionManager;


public class KidSignupGradeActivity extends AppCompatActivity implements View.OnClickListener {


    Button preSchoolButton, juniorSchoolButton;
    SessionManager sessionManager;
    Typeface typeface;
    TextView textView;
    ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kid_signup_grade);
        sessionManager = new SessionManager(this);
        preSchoolButton = findViewById(R.id.preschool_button);
        juniorSchoolButton = findViewById(R.id.juniorschool_button);
        textView = findViewById(R.id.grade_category);
        typeface = Typeface.createFromAsset(getResources().getAssets(), "asparagussprouts.ttf");
        backButton = findViewById(R.id.back_button_image);




        preSchoolButton.setOnClickListener(this);
        preSchoolButton.setTypeface(typeface);
        juniorSchoolButton.setTypeface(typeface);
        juniorSchoolButton.setOnClickListener(this);
        textView.setTypeface(typeface);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.preschool_button:
                startActivity(new Intent(this, KidSignupPreschoolGradeActivity.class));
                break;
            case R.id.juniorschool_button:
                startActivity(new Intent(this, KidSignupClassGradeActivity.class));

                break;
            case R.id.back_button_image:
                finish();
                break;
        }

    }
}
