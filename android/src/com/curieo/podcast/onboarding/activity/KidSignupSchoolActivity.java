package com.curieo.podcast.onboarding.activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.curieo.podcast.R;
import com.curieo.podcast.onboarding.MySpinnerAdapter;
import com.curieo.podcast.onboarding.data.SessionManager;
import java.util.ArrayList;
import java.util.List;



public class KidSignupSchoolActivity extends AppCompatActivity implements View.OnClickListener {


    SessionManager sessionManager;
    Typeface typeface;
    TextView textView;
    ImageView backButton;
    Spinner spinner;
    TextView kidsschool;
    Dialog dialog;
    ImageView next;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kid_signup_school);
        sessionManager = new SessionManager(this);
        next = findViewById(R.id.next);

        kidsschool = findViewById(R.id.kids_signup);
        spinner = findViewById(R.id.spinner_school);
        textView = findViewById(R.id.textview_school_not_found);
        typeface = Typeface.createFromAsset(getResources().getAssets(), "asparagussprouts.ttf");
        kidsschool.setTypeface(typeface);

        backButton = findViewById(R.id.back_button);
        List<String> spinnerArray = new ArrayList<String>();
        spinnerArray.add("Select School");


        MySpinnerAdapter
                adapter = new MySpinnerAdapter(
                this, R.layout.spinner_school_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        textView.setTypeface(typeface);
        backButton.setOnClickListener(this);
        textView.setOnClickListener(this);

        next.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.textview_school_not_found:
                showdialog();
                break;
            case R.id.next:
                if (!spinner.getSelectedItem().toString().equals("Select School")) {
                    sessionManager.setUserKidsSchool(spinner.getSelectedItem().toString());
                    startActivity(new Intent(this, KidSignupGradeActivity.class));

                } else {
                    Toast.makeText(KidSignupSchoolActivity.this, "Select your schoolSpinner", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.back_button:
                finish();
                break;
        }

    }

    public void showdialog() {

        dialog = new Dialog(this, android.R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.school_name_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP)
                    finish();

                return false;
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final TextInputEditText schoolEdittext = dialog.findViewById(R.id.edittext_schoolname);
        schoolEdittext.setTypeface(typeface);


        final Button cancelButton = dialog.findViewById(R.id.cancel_button);
        cancelButton.setTypeface(typeface);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view == cancelButton) {

                    dialog.dismiss();

                }
            }
        });
        Button applyButton = dialog.findViewById(R.id.apply_button);
        applyButton.setTypeface(typeface);
        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (schoolEdittext.getText().toString().isEmpty()) {
                    Toast.makeText(KidSignupSchoolActivity.this, "Enter the coupon  ", Toast.LENGTH_SHORT).show();
                } else {

                    sessionManager.setUserKidsSchool(schoolEdittext.getText().toString());
                    dialog.dismiss();
                    startActivity(new Intent(KidSignupSchoolActivity.this, KidSignupGradeActivity.class));

                }
            }
        });


        dialog.getWindow().setLayout(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
        if (!(KidSignupSchoolActivity.this).isFinishing()) {

            dialog.show();    //show dialog
        }


    }
}
