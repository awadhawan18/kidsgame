/*
 * Copyright (c) 2014.
 * Main Method Incorporated.
 */
package com.curieo.podcast.onboarding;

import android.app.Application;

import com.curieo.podcast.R;
import com.google.firebase.FirebaseApp;

public class BlackboardRadioApp extends Application {

    public static final String FLAG_IS_FIRST_SIGN_IN = "isFirstSignIn";
    private static final String TAG = BlackboardRadioApp.class.getSimpleName();
    public static String mVersionName;
    private static BlackboardRadioApp mInstance;

    public static String getVersionName() {
        return mVersionName;
    }

    public static synchronized BlackboardRadioApp getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        FirebaseApp.initializeApp(this);
        super.onCreate();


        mInstance = this;

        AppPrefHelper.getInstance(this).setUserHasOnboarded();
        FontsOverride.setDefaultFont(this, "MONOSPACE", getBaseContext().getResources().getString(R.string.main_font));

/*
        final FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        // set in-app defaults
        Map<String, Object> remoteConfigDefaults = new HashMap();
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_UPDATE_REQUIRED, false);
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_CURRENT_VERSION, "1.1.9");
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_UPDATE_URL,
                "https://play.google.com/store/apps/details?id=com.curieo.podcast");

        firebaseRemoteConfig.setDefaults(remoteConfigDefaults);
        firebaseRemoteConfig.fetch(1) // fetch every minutes
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, " check : remote config is fetched.");
                            firebaseRemoteConfig.activateFetched();
                        }
                    }
                });


        try {
            mVersionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Error retrieving app version name: ", e);
        }

*/


    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }


}