package com.curieo.podcast.onboarding.data;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.curieo.podcast.onboarding.activity.BlackboardActivity;
import com.curieo.podcast.onboarding.activity.OnboardingActivity;
import com.curieo.podcast.onboarding.activity.ParentSignupActivity;
import com.curieo.podcast.onboarding.activity.PhoneAuthActivity1;

import java.util.HashMap;

import static android.content.Context.MODE_PRIVATE;


public class SessionManager {

    public static final String USER_ID = "user_id";
    public static final String USER_TYPE = "user_type";
    public static final String USER_PARENT_NAME = "user_parent_name";
    public static final String USER_CITY = "user_city";
    public static final String IS_LOGIN_KIDS = "user_kids_login";
    public static final String USER_KIDS_NAME = "user_kids_name";
    public static final String USER_KIDS_AGE = "user_kids_age";
    public static final String USER_KIDS_GENDER = "user_kids_gender";
    public static final String USER_KIDS_DOB = "user_kids_dob";

    //kids_login
    public static final String USER_KIDS_GRADE = "user_kids_grade";
    public static final String USER_KIDS_SCHOOL = "user_kids_school";
    public static final String USER_SIGNUP_TIMESTAMP = "app_login_timestamp";
    public static final String NUMBER_OF_ATTEMPTS_FREE = "mumber_of_attempts";
    public static final String TODAYS_DATE = "last_attempted_question";
    public static final String LAST_ANSWERED_EXERCISE = "last_answered_exercise";
    public static final String LAST_GAME_ATTEMPTED = "last_game";
    public static final String CURRENT_KID = "current_kid";
    // Sharedpref file user Data
    public static final String GAME_FINISHED = "exercises_finished";
    public static final String CHECKED_DAILYEXERCISE = "daily_exercise";
    public static final String CHECKED_ONBOARDING = "onboarding";
    public static final String CHECKED_BING_EXERCISE = "bing_exercise";
    private static final String IS_PHONE_AUTH = "IsLoggedIn";
    private static final String IS_PARENT_SIGNEDUP = "isParentSgnedUp";
    //parent login
    private static String PREFS_FILE_NAME = "permissions";
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int MODE = 0;

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences("BBR", MODE);
        editor = pref.edit();
    }

    public static void firstTimeAskingPermission(Context context, String permission, boolean isFirstTime) {
        SharedPreferences sharedPreference = context.getSharedPreferences(PREFS_FILE_NAME, MODE_PRIVATE);
        sharedPreference.edit().putBoolean(permission, isFirstTime).apply();
    }

    public static boolean isFirstTimeAskingPermission(Context context, String permission) {
        return context.getSharedPreferences(PREFS_FILE_NAME, MODE_PRIVATE).getBoolean(permission, true);
    }

    public void exerciseFinished(String finished) {

    }

    public void setUserSignupTimestamp(long timestamp) {
        editor.putLong(USER_SIGNUP_TIMESTAMP, timestamp);
        editor.commit();

    }

    public void setUserParentName(String name) {
        editor.putString(USER_PARENT_NAME, name);
        editor.commit();

    }


    public void setCheckedDailyexercise(boolean checkedDailyexercise) {

        editor.putBoolean(CHECKED_DAILYEXERCISE, checkedDailyexercise);
        editor.commit();


    }

    public void setCurrentKid(int kidid) {
        editor.putInt(CURRENT_KID, kidid);
        editor.commit();
    }


    public void setUserKidsSchool(String kidsSchool) {
        editor.putString(USER_KIDS_SCHOOL, kidsSchool);
        editor.commit();
    }

    public void setUserKidsName(String kidsname) {
        editor.putString(USER_KIDS_NAME, kidsname);
        editor.commit();


    }

    public void setUserKidsAge(String kidsage) {

        editor.putString(USER_KIDS_AGE, kidsage);
        editor.commit();
    }


    public void setUserKidsGrade(String grade) {
        editor.putString(USER_KIDS_GRADE, grade);
        editor.commit();

    }


    public void setUserKidsGender(String gender) {
        editor.putString(USER_KIDS_GENDER, gender);
        editor.commit();
    }


    public void setUserKidsDob(String dob) {
        editor.putString(USER_KIDS_DOB, dob);
        editor.commit();
    }


/*
    public void setLastAnsweredQuestion(ArrayList<ArrayList<String>> lastAnswer) {
        editor.putInt(LAST_ANSWERED_EXERCISE, lastAnswer);
    }*/

    public void setUserId(int userId) {
        editor.putInt(USER_ID, userId);
        editor.commit();
    }

    public void setUserType(String userType) {
        editor.putString(USER_TYPE, userType);
        editor.commit();
    }


    public void setUserAttemptsToday(int userAttempts) {
        editor.putInt(NUMBER_OF_ATTEMPTS_FREE, userAttempts);
        editor.commit();
    }


    public void setLastAttemptGame(int lastAttemptGame) {
        editor.putInt(LAST_GAME_ATTEMPTED, lastAttemptGame);
        editor.commit();
    }

    public void setTodaysDate(String date) {
        editor.putString(TODAYS_DATE, date);
        editor.commit();
    }


    public void setHighScore(String score) {


    }

    public HashMap<String, String> getParentDetails() {
        HashMap<String, String> user = new HashMap<String, String>();

//        user.put(GROUP_ID, pref.getString(GROUP_ID, null));
        user.put(USER_ID, pref.getString(USER_ID, null));
        user.put(USER_TYPE, pref.getString(USER_TYPE, null));
        user.put(TODAYS_DATE, pref.getString(TODAYS_DATE, null));
        user.put(USER_PARENT_NAME, pref.getString(USER_PARENT_NAME, null));


        // return user
        return user;
    }

    public int getParent() {
        return pref.getInt(USER_ID, 0);
    }

    public int getCurrentKid() {
        return pref.getInt(CURRENT_KID, 0);
    }

    public long getUserTimestamp() {
        return pref.getLong(USER_SIGNUP_TIMESTAMP, 0);
    }

    public HashMap<String, String> getChildDetails() {
        HashMap<String, String> stringStringHashMap = new HashMap<>();
        stringStringHashMap.put(USER_KIDS_NAME, pref.getString(USER_KIDS_NAME, null));
        stringStringHashMap.put(USER_KIDS_AGE, pref.getString(USER_KIDS_AGE, null));
        stringStringHashMap.put(USER_KIDS_GENDER, pref.getString(USER_KIDS_GENDER, null));
        stringStringHashMap.put(USER_KIDS_GRADE, pref.getString(USER_KIDS_GRADE, null));
        stringStringHashMap.put(USER_KIDS_SCHOOL, pref.getString(USER_KIDS_SCHOOL, null));
        stringStringHashMap.put(USER_KIDS_DOB, pref.getString(USER_KIDS_DOB, null));


        return stringStringHashMap;
    }


    public HashMap<String, Integer> getSubmissionDetails() {
        HashMap<String, Integer> userSubmissions = new HashMap<String, Integer>();
        userSubmissions.put(NUMBER_OF_ATTEMPTS_FREE, pref.getInt(NUMBER_OF_ATTEMPTS_FREE, -1));
        userSubmissions.put(LAST_GAME_ATTEMPTED, pref.getInt(LAST_GAME_ATTEMPTED, -1));
        return userSubmissions;
    }

    public HashMap<String, Boolean> getSeenOnce() {
        HashMap<String, Boolean> seenOnce = new HashMap<String, Boolean>();
        seenOnce.put(CHECKED_DAILYEXERCISE, pref.getBoolean(CHECKED_DAILYEXERCISE, false));
        return seenOnce;
    }


    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
        Intent i = new Intent(_context, PhoneAuthActivity1.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);


        // Add new Flag to start new Activity


        // Staring Login Activity
        _context.startActivity(i);
        // After logout redirect user to Loing Activity
    }


    public void checkLogin() {


        // Check login status
        if (!this.isParentSignedUp() && !this.isPhoneAuth()) {
            //temporary start

            //temporary ends
            Intent i = new Intent(_context, OnboardingActivity.class);

            _context.startActivity(i);

        } else if (!this.isParentSignedUp()) {

            Intent intent = new Intent(_context, ParentSignupActivity.class);

            _context.startActivity(intent);


        } else {
            Intent i = new Intent(_context, BlackboardActivity.class);
            _context.startActivity(i);
        }

    }


    public void setKidSignedUp(boolean b) {
        editor.putBoolean(IS_LOGIN_KIDS, b);
        editor.commit();
    }

    public boolean iskidSignedUp() {
        return pref.getBoolean(IS_LOGIN_KIDS, false);
    }


    public boolean isPhoneAuth() {


        return pref.getBoolean(IS_PHONE_AUTH, false);
    }


    public void setIsPhoneAuth(boolean isPhoneAuth) {
        editor.putBoolean(IS_PHONE_AUTH, isPhoneAuth);
        editor.commit();
    }


    public boolean isParentSignedUp() {
        return pref.getBoolean(IS_PARENT_SIGNEDUP, false);
    }


    public void setIsParentSignedUp(boolean isParentSignedUp) {
        editor.putBoolean(IS_PARENT_SIGNEDUP, isParentSignedUp);
        editor.commit();
    }


    public void createParentLoginSession(int id, String userType, String parentName) {

        editor.putInt(USER_ID, id);
        editor.putString(USER_TYPE, userType);
        editor.putString(USER_PARENT_NAME, parentName);

        editor.commit();
    }


    public void createKidsLoginSession(String kidsName, String kidsGender, String grade, String school, String dob) {

        editor.putBoolean(IS_LOGIN_KIDS, true);
        editor.putString(USER_KIDS_NAME, kidsName);
        editor.putString(USER_KIDS_GENDER, kidsGender);
        editor.putString(USER_KIDS_GRADE, grade);
        editor.putString(USER_KIDS_SCHOOL, school);
        editor.putString(USER_KIDS_DOB, dob);
        editor.commit();

    }
}
