package com.curieo.podcast.onboarding.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.curieo.podcast.onboarding.activity.LogData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saransh on 21/4/18.
 */


public class UserDatabaseHandler extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String USER_DATABASE = "userDatabase";


    private static final String KID_TABLE = "kidTable";
    private static final String KID_ID = "id";
    private static final String KID_LAST_EXERCISE_TIME = "last_exercise_time";
    private static final String KID_NAME = "kid_name";
    private static final String KID_DOB = "date_of_birth";
    private static final String KID_GENDER = "gender";
    private static final String KID_GRADE = "grade";
    private static final String KID_SCHOOL = "school";
    private static final String KID_USER_TYPE = "user_type";
    private static final String KID_UID = "uid";
    private static final String KID_LAST_EXERCISE_ANSWERED = "last_exercise_answered";


    private static final String PARENT_TABLE = "parentTable";
    private static final String PARENT_ID = "id";
    private static final String PARENT_NAME = "parent_name";
    private static final String PARENT_UID = "uid";
    private static final String PARENT_PHONE_NUMBER = "phone_number";
    private static final String PARENT_CHANNEL_SUBSCRIPTIONS = "channel_subscriptions";
    private static final String KID_ACTIVE = "active";

    public UserDatabaseHandler(Context context) {
        super(context, USER_DATABASE, null, DATABASE_VERSION);
    }

    public UserDatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_KID_USER_TABLE = "CREATE TABLE " + KID_TABLE + "( " + KID_ID + " INTEGER, " + KID_NAME + " TEXT, "
                + KID_DOB + " TEXT," + KID_GENDER + " TEXT, " + KID_GRADE + " TEXT, " + KID_SCHOOL + " TEXT, " + KID_USER_TYPE + " TEXT, " + KID_UID + " INTEGER, "
                + KID_LAST_EXERCISE_TIME + " TEXT, " + KID_LAST_EXERCISE_ANSWERED + " INTEGER, " + KID_ACTIVE + " INTEGER, " + " FOREIGN KEY(" + KID_UID + ") REFERENCES  " + PARENT_TABLE + "(" + PARENT_ID + ") " + " )";

        String CREATE_PARENT_USER_TABLE = "CREATE TABLE " + PARENT_TABLE + "( " + PARENT_ID + " INTEGER, " + PARENT_NAME + " TEXT, " + PARENT_UID + " TEXT, " + PARENT_PHONE_NUMBER + " TEXT, " + PARENT_CHANNEL_SUBSCRIPTIONS + " TEXT " + ")";


        db.execSQL(CREATE_PARENT_USER_TABLE);
        db.execSQL(CREATE_KID_USER_TABLE);

    }


/////////////   parents


    public void addParent(ParentUser parent) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PARENT_ID, parent.getId());
        contentValues.put(PARENT_NAME, parent.getParentName());
        sqLiteDatabase.insert(PARENT_TABLE, null, contentValues);
    }


    public int updateParentName(ParentUser parentUser) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(PARENT_NAME, parentUser.getParentName());
        return sqLiteDatabase.update(PARENT_TABLE, contentValues, PARENT_ID + " = ?", new String[]{String.valueOf(parentUser.getId())});
    }


    public String getKidUserType(int kidid) {
        KidUser kidUser = null;
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM  " + KID_TABLE + " WHERE " + KID_ID + " = " + kidid;

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                kidUser = new KidUser(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getInt(7), cursor.getString(8), cursor.getInt(9), cursor.getInt(10) > 0);

            } while (cursor.moveToNext());
        }
        return kidUser != null ? kidUser.getUserType() : null;

    }


    public void setUserType(int kidid) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(KID_USER_TYPE, "P");
        sqLiteDatabase.update(KID_TABLE, contentValues, KID_ID + " = ?", new String[]{String.valueOf(kidid)});
    }

    public List<ParentUser> getParents() {
        List<ParentUser> parentUsers = new ArrayList<>();
        String coupon = "SELECT * FROM " + PARENT_TABLE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(coupon, null);
        if (cursor.moveToFirst()) {
            do {
                ParentUser parentUser = new ParentUser(cursor.getInt(0), cursor.getString(1));
                parentUsers.add(parentUser);
            } while (cursor.moveToNext());
        }

        return parentUsers;
    }


////////////// kids

    public void addKid(KidUser user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();


        contentValues.put(KID_ID, user.getId());
        contentValues.put(KID_NAME, user.getKidName());
        contentValues.put(KID_DOB, user.getDateOfBirth());
        contentValues.put(KID_GENDER, user.getGender());
        contentValues.put(KID_GRADE, user.getGrade());
        contentValues.put(KID_SCHOOL, user.getSchool());
        contentValues.put(KID_USER_TYPE, user.getUserType());
        contentValues.put(KID_UID, user.getUid());
        contentValues.put(KID_LAST_EXERCISE_TIME, user.getLastExerciseTime());
        contentValues.put(KID_LAST_EXERCISE_ANSWERED, user.getLastExerciseAnswered());
        contentValues.put(KID_ACTIVE, user.isActive());


        db.insert(KID_TABLE, null, contentValues);
    }


    public int updateKid(KidUser user) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KID_ID, user.getId());
        contentValues.put(KID_NAME, user.getKidName());
        contentValues.put(KID_DOB, user.getDateOfBirth());
        contentValues.put(KID_GENDER, user.getGender());
        contentValues.put(KID_GRADE, user.getGrade());
        contentValues.put(KID_SCHOOL, user.getSchool());
        return sqLiteDatabase.update(KID_TABLE, contentValues, KID_ID + " = ?", new String[]{String.valueOf(user.getId())});
    }


    public void updateActiveUser(int kid) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();


        ContentValues contentValues = new ContentValues();
        contentValues.put(KID_ACTIVE, true);
        sqLiteDatabase.update(KID_TABLE, contentValues, KID_ID + " = ?", new String[]{String.valueOf(kid)});
        sqLiteDatabase.execSQL("UPDATE " + KID_TABLE + " SET " + KID_ACTIVE + " = 0 " + " WHERE  " + KID_ID + "!= " + kid);
        LogData.logKidsData(getAllKids(), "KidUserAdapter");
    }

    public KidUser getCurrentKid() {
        KidUser kidUser = null;
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM  " + KID_TABLE + " WHERE " + KID_ACTIVE + " = 1";

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                kidUser = new KidUser(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getInt(7), cursor.getString(8), cursor.getInt(9), cursor.getInt(10) > 0);

            } while (cursor.moveToNext());
        }
        return kidUser;
    }

    public int setKidActive(KidUser kidUser) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KID_ACTIVE, true);
        return sqLiteDatabase.update(KID_TABLE, contentValues, KID_ID + " = ?", new String[]{String.valueOf(kidUser.getId())});

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + KID_TABLE);
        onCreate(db);
    }


    public int getkidCount() {
        String countQuery = "SELECT  * FROM " + KID_TABLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;

    }


    public void lastAnsweredExercise() {


    }

    public List<KidUser> getAllKids() {
        List<KidUser> kidUsers = new ArrayList<>();
        String coupon = "SELECT * FROM " + KID_TABLE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(coupon, null);
        if (cursor.moveToFirst()) {
            do {
                KidUser kidUser = new KidUser(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getInt(7), cursor.getString(8), cursor.getInt(9), cursor.getInt(10) > 0);
                kidUsers.add(kidUser);
            } while (cursor.moveToNext());
        }

        return kidUsers;
    }

    public void deleteTable() {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("delete from " + KID_TABLE);

    }

}
