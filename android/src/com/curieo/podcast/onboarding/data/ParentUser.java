package com.curieo.podcast.onboarding.data;

import com.curieo.podcast.radio.models.Channel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ParentUser {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("id")
    @Expose
    private Integer id;


    @SerializedName("parent_name")
    @Expose
    private String parentName;

    @SerializedName("newuser")
    @Expose
    private Boolean newuser;

    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("channel_subscription")
    @Expose
    private List<Channel> channelSubscription = null;

    public ParentUser(Integer id, String parentName) {
        this.id = id;
        this.parentName = parentName;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public List<Channel> getChannelSubscription() {
        return channelSubscription;
    }

    public void setChannelSubscription(List<Channel> channelSubscription) {
        this.channelSubscription = channelSubscription;
    }

    public Boolean isNewuser() {
        return newuser;
    }

    public void setNewuser(Boolean newuser) {
        this.newuser = newuser;
    }
}