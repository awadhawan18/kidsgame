
package com.curieo.podcast.onboarding.data;

import android.support.annotation.IntegerRes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class KidUser implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("last_exercise_time")
    @Expose
    private String lastExerciseTime;
    @SerializedName("kid_name")
    @Expose
    private String kidName;
    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("grade")
    @Expose
    private String grade;
    @SerializedName("school")
    @Expose
    private String school;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("uid")
    @Expose
    private Integer uid;
    @SerializedName("last_exercise_answered")
    @Expose
    private Integer lastExerciseAnswered;


    private boolean active;

    public KidUser(int id, String kidName, String dateOfBirth, String gender, String grade, String school, String userType, Integer uid, boolean acti) {
        this.id = id;
        this.kidName = kidName;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.grade = grade;
        this.school = school;
        this.userType = userType;
        this.uid = uid;
        this.active = acti;
    }



    public KidUser(Integer id, String kidName, String dateOfBirth, String gender, String grade, String school, String userType, Integer uid, String lastExerciseTime, Integer lastExerciseAnswered , boolean active) {

        this.id = id;
        this.kidName = kidName;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.grade = grade;
        this.school = school;
        this.userType = userType;
        this.uid = uid;
        this.lastExerciseTime = lastExerciseTime;
        this.lastExerciseAnswered = lastExerciseAnswered;
        this.active = active;

    }


    public KidUser(Integer id, String kidName, String dateOfBirth, String gender, String grade, String school) {
        this.id = id;
        this.kidName = kidName;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.grade = grade;
        this.school = school;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLastExerciseTime() {
        return lastExerciseTime;
    }

    public void setLastExerciseTime(String lastExerciseTime) {
        this.lastExerciseTime = lastExerciseTime;
    }

    public String getKidName() {
        return kidName;
    }

    public void setKidName(String kidName) {
        this.kidName = kidName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getLastExerciseAnswered() {
        return lastExerciseAnswered;
    }

    public void setLastExerciseAnswered(Integer lastExerciseAnswered) {
        this.lastExerciseAnswered = lastExerciseAnswered;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}