package com.curieo.podcast;


import com.badlogic.gdx.graphics.Texture;
import com.curieo.podcast.interfaces.ImageLoaderCore;
import com.curieo.podcast.interfaces.SpeechTextCore;
import com.curieo.podcast.interfaces.TextSpeechCore;
import com.curieo.podcast.interfaces.UrlHandler;

public class CommonObjects {
    public static TextSpeechCore textToSpeech;
    public static SpeechTextCore speechToText;
    public static ImageLoaderCore imageLoader;
    public static ExerciseData exerciseData;
    public static UrlHandler urlHandler;
    public static Texture loadedTexture;
    public static float screenWidth, screenHeight;
    public static String[] responses;
}
