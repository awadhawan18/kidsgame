package com.curieo.podcast.jumble;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.curieo.podcast.AnimatedFullScreen;
import com.curieo.podcast.CommonObjects;
import com.curieo.podcast.KidsGame;
import com.curieo.podcast.listeners.OnSpeechResults;

import java.util.Random;

import static com.curieo.podcast.CommonObjects.exerciseData;
import static com.curieo.podcast.CommonObjects.screenHeight;
import static com.curieo.podcast.CommonObjects.screenWidth;

public class JumbleGameScreen1 implements Screen {
    final KidsGame game;
    private Timer timer;
    private Stage stage;
    private Table table;
    private Texture backgroundTexture;
    private BitmapFont closeness;
    private Actor[] actors;  //holds the labels for placeholder
    private boolean[] completed;  //holds the position for corrected letter
    private Image wrong;
    private Image win;
    private int incorrect = 0; //holds no. of incorrect responses
    private int wordCounter = 0; //holds present word no.
    private int correctLetters = 0; // no. of letters corrected by user for a single word
    private String speak;
    private String[] words;
    private String[] meanings; //holds the meanings for landscape drag and drop
    private int type;

    //colors for completed letters
    private Color[] colors = {
            Color.valueOf("#FD4C9C"),
            Color.valueOf("#5C00AC"),
            Color.valueOf("#57AEEF"),
            Color.valueOf("#FF8900"),
            Color.valueOf("#7EFE00"),
            Color.valueOf("#FF2C90"),
            Color.valueOf("#5B00AC"),
            Color.valueOf("#2F89DE"),
            Color.valueOf("#FFA04F"),
            Color.valueOf("#93FF00"),
            Color.valueOf("#FF0089"),
            Color.valueOf("#6DD53A"),
            Color.valueOf("#54B5F3"),
            Color.valueOf("#FF248C"),
            Color.valueOf("#FFA500"),
            Color.valueOf("#EDEDEE"),
            Color.valueOf("#4E00A4"),
            Color.valueOf("#2C8EE3"),
            Color.valueOf("#FD3A95"),
            Color.valueOf("#54B5F3"),
            Color.valueOf("#FF0089"),
            Color.valueOf("#FFA500"),
            Color.valueOf("#4F00A6"),
            Color.valueOf("#54F800"),
            Color.valueOf("#EDEDEE"),
            Color.valueOf("#EDEDEE")
    };

    /*
        type 1 for normal drag and drop
        type 2 for landscape
     */
    public JumbleGameScreen1(KidsGame kids, int t) {
        this.game = kids;
        table = new Table();
        stage = new Stage(new FitViewport(game.screenWidth, game.screenHeight));
        timer = new Timer();
        this.type = t;
        Gdx.input.setInputProcessor(stage);


        //background for this screen
        if (type == 1) {
            backgroundTexture = new Texture(Gdx.files.internal("jumble_background1.png"));
            table.background(new TextureRegionDrawable(new TextureRegion(backgroundTexture)));
        } else {
            backgroundTexture = new Texture(Gdx.files.internal("big_jumble_background1.png"));
            table.background(new TextureRegionDrawable(new TextureRegion(backgroundTexture)));
            meanings = exerciseData.getSentences();
        }



        //font for scramble letters
        closeness = fontGenerator(100);

        //holds the scramble words
        words = exerciseData.getWords();


        CommonObjects.responses = new String[words.length + 1]; //+1 for sentence response;


        table.setName("table");
        table.setFillParent(true);
        stage.addActor(table);
        speak = words[0];
        CommonObjects.textToSpeech.speak(speak);
        setUpPlaceholders(words[0]);
        setUpActors(words[0]);
        initializeButtons();
        initializeTryAndWin();


        game.setSpeechListener(new OnSpeechResults() {
            @Override
            public void checkResult() {
                if (game.speechOutput.equalsIgnoreCase(speak)) {
                    if (type == 2) {
                        playMeaning();
                    }
                    CommonObjects.responses[wordCounter] = game.speechOutput;
                    winActor();
                    incorrect = 0;
                    timer.scheduleTask(new Timer.Task() {
                        @Override
                        public void run() {
                            wordCounter++;
                            if (wordCounter >= words.length) {

                                //next screen for scramble sentence
                                JumbleGameScreen2 jmb = new JumbleGameScreen2(game, type);
                                game.setScreen(jmb);
                                stage.clear();
                            } else {
                                setUpNextWord();
                            }
                        }
                    }, 2);

                } else {
                    incorrect++;
                    tryAgain();
                    timer.scheduleTask(new Timer.Task() {
                        @Override
                        public void run() {
                            if (incorrect % 2 == 0) {

                                if (type == 2) {
                                    playMeaning(); //play meaning for landscape game
                                }
                                CommonObjects.responses[wordCounter] = game.speechOutput;
                                wordCounter++;
                                game.showToast("We will try this later");
                                if (wordCounter >= words.length) {

                                    //next screen for scramble sentence
                                    JumbleGameScreen2 jmb = new JumbleGameScreen2(game, type);
                                    game.setScreen(jmb);
                                    stage.clear();

                                } else {
                                    setUpNextWord();
                                }
                            } else game.showToast("Try Again");
                        }
                    }, 2);

                }
                game.startBlinking = false;
            }
        });


    }

    //displays wrong answer image
    private void tryAgain() {
        game.wrongSound.play();
        stage.addActor(wrong);
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                stage.getRoot().findActor("wrong").remove();

            }
        }, 2);

    }

    //displays right answer image
    private void winActor() {
        game.correctSound.play();
        stage.addActor(win);
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                stage.getRoot().findActor("win").remove();

            }
        }, 2);

    }


    @Override
    public void show() {
        if (game.music.isPlaying()) {
            game.music.pause();
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
        stage.act();

        //blinks speak button when listening
        if (game.startBlinking) {
            if (TimeUtils.millis() - game.lastClick > 200) {
                if (game.blinkButton2.isVisible()) {
                    game.blinkButton2.setVisible(false);
                } else {
                    game.blinkButton2.setVisible(true);
                }
                game.lastClick = TimeUtils.millis();
            }
        } else {
            game.blinkButton2.setVisible(false);
        }
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    //displays placeholder letters in center
    private void setUpPlaceholders(String str) {
        String[] alphabets = str.split("(?!^)");
        Label.LabelStyle style = new Label.LabelStyle(closeness, Color.WHITE);
        actors = new Actor[alphabets.length];
        completed = new boolean[alphabets.length];

        float width = 0;

        //calculates the desired width of placeholder row
        for (String a : alphabets) {
            Label label = new Label(a, style);
            width += label.getWidth() + 20;
        }
        float startingPoint = (game.screenWidth - width) / 2;


        for (int i = 0; i < alphabets.length; i++) {
            Label label = new Label(alphabets[i], style);
            label.setName(alphabets[i] + "placeholder");
            label.setX(startingPoint);
            label.setY(game.screenHeight / 2 - label.getHeight() / 2);
            startingPoint += (label.getWidth() + 20);
            stage.addActor(label);
            actors[i] = label;
            completed[i] = false;

        }



    }

    //shuffles the words
    private String scramble(String inputString) {
        Random random = new Random();
        char a[] = inputString.toCharArray();

        // Scramble the letters using the standard Fisher-Yates shuffle,
        for (int i = 0; i < a.length; i++) {
            int j = random.nextInt(a.length);
            char temp = a[i];
            a[i] = a[j];
            a[j] = temp;
        }

        return new String(a);
    }

    //drag and drop letters with listeners
    private void setUpActors(String temp1) {
        final String str = scramble(temp1);
        final String[] alphabets = str.split("(?!^)");

        for (int i = 0; i < alphabets.length; i++) {

            //label for final corrected letter
            Label.LabelStyle style = new Label.LabelStyle(closeness, colors[(int) alphabets[i].toLowerCase().charAt(0) - 97]);
            final Label label = new Label(alphabets[i], style);
            final Image image = new Image(new Texture(Gdx.files.internal("alphabets/" + alphabets[i].toLowerCase() + ".png")));

            final int num = i;

            final float px = (screenWidth / (alphabets.length + 2)) * (i + 1);

            //for random height of actors, below and top of placeholders
            float[] temp = new float[2];
            if (type == 1) {
                temp[0] = (screenHeight / 9) * ((float) Math.random() * 2 + 1);
                temp[1] = (screenHeight / 9) * ((float) Math.random() * 1 + 6);
            } else {
                temp[0] = (screenHeight / 11) * ((float) Math.random() * 2 + 1);
                temp[1] = (screenHeight / 11) * ((float) Math.random() * 1 + 7);
            }

            final float py = temp[(int) (Math.random() * 2)];

            label.setName(alphabets[i] + "actor");
            label.setPosition(px, py);
            label.setVisible(false);
            image.setName(alphabets[i] + "image");
            image.setPosition(px, py);
            image.setScale((float) 0.75);


            //animated letter
            final AnimatedLabel animatedLabel = new AnimatedLabel(px - 50, py - 50, alphabets[i].toLowerCase());
            animatedLabel.setVisible(false);

            //listener for letter images
            image.addListener(new DragListener() {

                public void drag(InputEvent event, float x, float y, int pointer) {
                    label.setVisible(false);
                    animatedLabel.setVisible(true);
                    animatedLabel.startAnimation = true;
                    animatedLabel.updateLabel(event.getStageX() - 50, event.getStageY() - 50);
                    image.setVisible(false);

                }

                public void dragStop(InputEvent event, float x, float y, int pointer) {

                    //finds the placeholder actor in stage
                    Actor actor;
                    boolean flag = false;
                    for (int i = 0; i < actors.length; i++) {
                        actor = actors[i];
                        if (event.getStageX() + 75 >= actor.getX() && event.getStageX() <= actor.getX() + 75 &&
                                event.getStageY() + 75 >= actor.getY() && event.getStageY() <= actor.getY() + 75) {
                            if (actor.getName().equalsIgnoreCase(alphabets[num] + "placeholder") && !completed[i]) {

                                label.setVisible(true);
                                label.setPosition(actor.getX(), actor.getY());
                                animatedLabel.finalPositon(actor.getX(), actor.getY());
                                animatedLabel.startAnimation = false;
                                animatedLabel.setVisible(false);

                                image.removeListener(this);
                                flag = true;
                                completed[i] = true;
                                correctLetters++;

                                //displays play and speak button when all letters are correctly placed
                                if (correctLetters == alphabets.length) {
                                    Actor playButton = stage.getRoot().findActor("play");
                                    playButton.setVisible(true);
                                    Actor speakButton = stage.getRoot().findActor("speak");
                                    speakButton.setVisible(true);
                                }

                                //remove letter from screen
                                animatedLabel.remove();
                                image.remove();

                            }
                        }
                        if (flag) break;
                    }

                    if (!flag) {

                        //game.wrongSound.play();
                        label.setVisible(false);
                        animatedLabel.setVisible(false);
                        animatedLabel.originalPostion();
                        animatedLabel.startAnimation = false;
                        animatedLabel.setVisible(false);
                        image.setVisible(true);
                        image.setPosition(px, py);

                    }

                }

            });
            stage.addActor(animatedLabel);
            stage.addActor(label);
            stage.addActor(image);
        }
    }

    //clear the stage for next word
    private void setUpNextWord() {
        String[] alphabets = words[wordCounter - 1].split("(?!^)");
        for (String alphabet : alphabets) {
            Actor placeholder = stage.getRoot().findActor(alphabet + "placeholder");
            Actor actor = stage.getRoot().findActor(alphabet + "actor");
            placeholder.remove();
            actor.remove();
        }
        actors = null;
        completed = null;

        Actor playButton = stage.getRoot().findActor("play");
        playButton.setVisible(false);

        Actor speakButton = stage.getRoot().findActor("speak");
        speakButton.setVisible(false);

        setUpPlaceholders(words[wordCounter]);
        setUpActors(words[wordCounter]);
        correctLetters = 0;
        speak = words[wordCounter];
        CommonObjects.textToSpeech.speak(speak);
    }

    //play and speak buttons
    private void initializeButtons() {
        Texture playTexture = new Texture(Gdx.files.internal("actions/play_button.png"));
        Texture speakTexture = new Texture(Gdx.files.internal("actions/speak_button.png"));
        Image playButton = new Image(playTexture);
        final Image speakButton = new Image(speakTexture);
        playButton.setName("play");
        speakButton.setName("speak");

        float textureScaleX;
        float textureScaleY;
        if (type == 1) {
            textureScaleX = (game.screenWidth / 1080);
            textureScaleY = (game.screenHeight / 1920);
        } else {
            textureScaleX = (game.screenWidth / 1920);
            textureScaleY = (game.screenHeight / 1080);
        }


        playButton.setWidth(textureScaleX * playTexture.getWidth());
        playButton.setHeight(textureScaleY * playTexture.getHeight());
        speakButton.setWidth(textureScaleX * speakTexture.getWidth());
        speakButton.setHeight(textureScaleY * speakTexture.getHeight());

        playButton.setPosition(game.screenWidth / 2 - (game.screenWidth / 8) - playButton.getWidth(), 70 * (game.screenHeight / 1280));
        speakButton.setPosition(game.screenWidth / 2 + (game.screenWidth / 8), 70 * (game.screenHeight / 1280));
        game.blinkButton1.setPosition(game.screenWidth / 2 - (game.screenWidth / 8) - playButton.getWidth(), 70 * (game.screenHeight / 1280));
        game.blinkButton2.setPosition(game.screenWidth / 2 + (game.screenWidth / 8), 70 * (game.screenHeight / 1280));

        playButton.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                CommonObjects.textToSpeech.speak(speak);
                game.blinkButton1.setVisible(true);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                game.blinkButton1.setVisible(false);
            }
        });

        speakButton.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                game.checkPermissions();
                game.lastClick = TimeUtils.millis();
                CommonObjects.speechToText.promptSpeechInput();
                game.startBlinking = true;
                return true;
            }

        });

        playButton.setVisible(false);
        speakButton.setVisible(false);

        stage.addActor(playButton);
        stage.addActor(speakButton);
        stage.addActor(game.blinkButton1);
        stage.addActor(game.blinkButton2);
    }

    //try and win images
    private void initializeTryAndWin() {
        Texture winTexture = new Texture(Gdx.files.internal("actions/win_layer.png"));
        Texture wrongTexture = new Texture(Gdx.files.internal("actions/try_again.png"));
        win = new Image(winTexture);
        win.setName("win");

        if (type == 1) {
            float winScaleX = game.screenWidth / winTexture.getWidth();
            float winScaleY = game.screenHeight / winTexture.getHeight();
            win.setWidth(500 * winScaleX);
            win.setHeight(800 * winScaleY);
        } else {
            float winScaleX = game.screenWidth / winTexture.getHeight();
            float winScaleY = game.screenHeight / winTexture.getWidth();
            win.setWidth(600 * winScaleX);
            win.setHeight(600 * winScaleY);
        }

        win.setPosition(game.screenWidth / 2 - win.getWidth() / 2, game.screenHeight / 2 - win.getHeight() / 2);

        wrong = new Image(wrongTexture);
        wrong.setName("wrong");
        wrong.setWidth(600 * game.screenWidth / wrongTexture.getWidth());
        wrong.setHeight(600 * game.screenWidth / wrongTexture.getHeight());
        wrong.setPosition(game.screenWidth / 2 - wrong.getWidth() / 2, game.screenHeight / 2 - wrong.getHeight() / 2);

    }

    //generates bitmap font for labels
    private BitmapFont fontGenerator(int size) {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("KatahdinRound.otf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = size;
        return generator.generateFont(parameter);

    }

    private void playMeaning() {
        if (meanings != null) {
            CommonObjects.textToSpeech.speak(words[wordCounter] + " means " + meanings[wordCounter]);
        }
    }


}
