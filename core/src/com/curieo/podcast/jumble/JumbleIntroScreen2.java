package com.curieo.podcast.jumble;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.curieo.podcast.AnimatedFullScreen;
import com.curieo.podcast.KidsGame;
import com.curieo.podcast.listeners.OnAnimationComplete;

import java.util.ArrayList;

public class JumbleIntroScreen2 implements Screen {

    private final KidsGame game;
    private Stage stage;
    private Table table;
    private AnimatedFullScreen animation;
    private Texture backgroundTexture;


    public JumbleIntroScreen2(KidsGame kids) {
        this.game = kids;
        table = new Table();
        stage = new Stage(new FitViewport(game.screenWidth, game.screenHeight));
        Gdx.input.setInputProcessor(stage);


        //sets next screen when animation is completed
        animation = new AnimatedFullScreen("jar_animation", false,"");
        animation.setListener(new OnAnimationComplete() {
            @Override
            public void setNextScreen() {
                JumbleGameScreen1 jm = new JumbleGameScreen1(game, 1);
                game.setScreen(jm);
            }
        });
        backgroundTexture = new Texture(Gdx.files.internal("jumble_intro_background2.png"));
        table.background(new TextureRegionDrawable(new TextureRegion(backgroundTexture)));
        table.add(animation);


        table.setFillParent(true);
        stage.addActor(table);

    }


    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        stage.draw();
        stage.act();

    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
