package com.curieo.podcast.jumble;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;


// Animated letter for drag and drop games
public class AnimatedLabel extends Actor
{
    public Music letterSound;
    public boolean startAnimation = false;
    private Animation<TextureRegion> animation;
    private TextureAtlas atlas;
    private float px,py,tempX,tempY;
    private float showTime;


    public AnimatedLabel(float px, float py, String letter) {
        this.px = px;
        this.py = py;
        tempX = px;
        tempY = py;
        /*letterSound = Gdx.audio.newMusic(Gdx.files.internal("sounds/"+letter+"-long.mp3"));
        letterSound.setLooping(true);*/
        atlas = new TextureAtlas(Gdx.files.internal("animations/"+letter+".atlas"));
        this.animation = new Animation<TextureRegion>(1/10f,atlas.getRegions());
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        showTime += delta;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if(startAnimation){
            batch.draw(animation.getKeyFrame(showTime,true),tempX,tempY);
        }

    }

    public void update(float x, float y){
        tempX+= x;
        tempY+= y;
    }

    //use this function to update the position
    public void updateLabel(float x, float y){
        tempX = x;
        tempY = y;
    }

    //sets the letter to original position
    public void originalPostion(){
        tempX = px;
        tempY = py;
    }

    //sets the letter to final position
    public void finalPositon(float x, float y){
        px = x;
        py = y;
    }
}