package com.curieo.podcast.jumble;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.curieo.podcast.CommonObjects;
import com.curieo.podcast.commonScreens.CongratsScreen;
import com.curieo.podcast.KidsGame;
import com.curieo.podcast.listeners.OnImageLoaded;
import com.curieo.podcast.listeners.OnSpeechResults;

import static com.curieo.podcast.CommonObjects.exerciseData;

public class JumbleGameScreen3 implements Screen {
    final KidsGame game;
    private Timer timer;
    private Stage stage;
    private Table table;
    private Texture backgroundTexture;
    private BitmapFont closeness;
    private Image wrong;
    private Image win;
    private int incorrect = 0;
    private boolean textureLoaded = false;
    private String speak;
    private String sentences;
    private String[] urls;
    private float scaleX;
    private float scaleY;
    private int type;

    private Color[] colors = {
            Color.RED,
            Color.BROWN,
            Color.CYAN,
            Color.CORAL,
            Color.CHARTREUSE,
            Color.ORANGE,
            Color.FOREST,
            Color.DARK_GRAY
    };


    public JumbleGameScreen3(KidsGame kids, int t) {
        this.game = kids;
        table = new Table();
        stage = new Stage(new FitViewport(game.screenWidth, game.screenHeight));
        timer = new Timer();
        this.type = t;
        Gdx.input.setInputProcessor(stage);

        if (type == 1) {
            backgroundTexture = new Texture(Gdx.files.internal("jumble_background3.png"));
            table.background(new TextureRegionDrawable(new TextureRegion(backgroundTexture)));
        } else {
            backgroundTexture = new Texture(Gdx.files.internal("big_jumble_background3.png"));
            table.background(new TextureRegionDrawable(new TextureRegion(backgroundTexture)));
        }

        scaleX = game.screenWidth / backgroundTexture.getWidth();
        scaleY = game.screenHeight / backgroundTexture.getHeight();


        closeness = fontGenerator(70);

        sentences = exerciseData.getObject_string();

        urls = exerciseData.getImageUrls();

        CommonObjects.imageLoader.setOnImageLoadedListener(new OnImageLoaded() {
            @Override
            public void setTexture() {
                textureLoaded = true;

            }
        });

        CommonObjects.imageLoader.loadImage(urls[0]); //call to download image


        table.setFillParent(true);
        stage.addActor(table);
        speak = sentences;
        setUpPlaceholders(sentences);
        CommonObjects.textToSpeech.speak(speak);
        initializeTryAndWin();
        initializeButtons();

        game.setSpeechListener(new OnSpeechResults() {
            @Override
            public void checkResult() {
                if (game.speechOutput.equalsIgnoreCase(speak.toLowerCase().trim())) {
                    CommonObjects.responses[CommonObjects.responses.length - 1] = game.speechOutput;
                    winActor();
                    timer.scheduleTask(new Timer.Task() {
                        @Override
                        public void run() {
                            CongratsScreen CS = new CongratsScreen(game, type);
                            game.setScreen(CS);
                            stage.clear();
                        }
                    }, 2);
                } else {
                    incorrect++;
                    tryAgain();
                    if (incorrect % 2 == 0) {
                        CommonObjects.responses[CommonObjects.responses.length - 1] = game.speechOutput;
                        game.showToast("We will try this later");
                        timer.scheduleTask(new Timer.Task() {
                            @Override
                            public void run() {
                                game.setScreen(new CongratsScreen(game, type));
                                stage.clear();
                            }
                        }, 2);
                    } else game.showToast("Try Again");
                }
                game.startBlinking = false;
            }
        });


    }

    //wrong response image
    private void tryAgain() {
        game.wrongSound.play();
        stage.addActor(wrong);
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                stage.getRoot().findActor("wrong").remove();

            }
        }, 2);

    }

    //correct response image
    private void winActor() {
        game.correctSound.play();
        stage.addActor(win);
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                stage.getRoot().findActor("win").remove();

            }
        }, 2);

    }


    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
        stage.act();

        //checks if image is downloaded from server
        if (textureLoaded) {
            Texture texture = CommonObjects.imageLoader.getImage();
            Image image = new Image(texture);
            if (type == 1) {
                image.setHeight(350 * scaleY);
                image.setWidth(550 * scaleX);
                image.setPosition(90 * scaleX, 860 * scaleY);
            } else {
                image.setHeight(310 * scaleY);
                image.setWidth(610 * scaleX);
                image.setPosition(330 * scaleX, 350 * scaleY);
            }

            stage.addActor(image);
            textureLoaded = false;
        }

        //speak button starts blinking when true
        if (game.startBlinking) {
            if (TimeUtils.millis() - game.lastClick > 200) {
                if (game.blinkButton2.isVisible()) {
                    game.blinkButton2.setVisible(false);
                } else {
                    game.blinkButton2.setVisible(true);
                }
                game.lastClick = TimeUtils.millis();
            }
        } else {
            game.blinkButton2.setVisible(false);
        }

    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }


    //displays sentence
    private void setUpPlaceholders(String str) {
        String[] alphabets = str.split("\\s+");

        for (int i = 0; i < alphabets.length; i++) {
            Label.LabelStyle style = new Label.LabelStyle(closeness, colors[i]);
            Label label = new Label(alphabets[i], style);
            label.setName(alphabets[i] + "placeholder");
            table.add(label).padTop(game.screenHeight / 2).padBottom(game.screenHeight / 4).padLeft(10).padRight(10);
        }
    }

    //play and speak button
    private void initializeButtons() {
        Texture playTexture = new Texture(Gdx.files.internal("actions/play_button.png"));
        Texture speakTexture = new Texture(Gdx.files.internal("actions/speak_button.png"));
        Image playButton = new Image(playTexture);
        final Image speakButton = new Image(speakTexture);
        playButton.setName("play");
        speakButton.setName("speak");

        float textureScaleX;
        float textureScaleY;
        if (type == 1) {
            textureScaleX = (game.screenWidth / 1080);
            textureScaleY = (game.screenHeight / 1920);
        } else {
            textureScaleX = (game.screenWidth / 1920);
            textureScaleY = (game.screenHeight / 1080);
        }

        playButton.setWidth(textureScaleX * playTexture.getWidth());
        playButton.setHeight(textureScaleY * playTexture.getHeight());
        speakButton.setWidth(textureScaleX * speakTexture.getWidth());
        speakButton.setHeight(textureScaleY * speakTexture.getHeight());

        playButton.setPosition(game.screenWidth / 2 - (game.screenWidth / 8) - playButton.getWidth(), 70 * (game.screenHeight / 1280));
        speakButton.setPosition(game.screenWidth / 2 + (game.screenWidth / 8), 70 * (game.screenHeight / 1280));

        game.blinkButton1.setPosition(game.screenWidth / 2 - (game.screenWidth / 8) - playButton.getWidth(), 70 * (game.screenHeight / 1280));
        game.blinkButton2.setPosition(game.screenWidth / 2 + (game.screenWidth / 8), 70 * (game.screenHeight / 1280));

        playButton.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                CommonObjects.textToSpeech.speak(speak);
                game.blinkButton1.setVisible(true);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                game.blinkButton1.setVisible(false);
            }
        });

        speakButton.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                game.checkPermissions();
                game.startBlinking = true;
                CommonObjects.speechToText.promptSpeechInput();
                return true;
            }

        });

        stage.addActor(playButton);
        stage.addActor(speakButton);
        stage.addActor(game.blinkButton1);
        stage.addActor(game.blinkButton2);
    }


    private void initializeTryAndWin() {
        Texture winTexture = new Texture(Gdx.files.internal("actions/win_layer.png"));
        Texture wrongTexture = new Texture(Gdx.files.internal("actions/try_again.png"));
        win = new Image(winTexture);
        win.setName("win");

        if (type == 1) {
            float winScaleX = game.screenWidth / winTexture.getWidth();
            float winScaleY = game.screenHeight / winTexture.getHeight();
            win.setWidth(500 * winScaleX);
            win.setHeight(800 * winScaleY);
        } else {
            float winScaleX = game.screenWidth / winTexture.getHeight();
            float winScaleY = game.screenHeight / winTexture.getWidth();
            win.setWidth(600 * winScaleX);
            win.setHeight(600 * winScaleY);
        }

        win.setPosition(game.screenWidth / 2 - win.getWidth() / 2, game.screenHeight / 2 - win.getHeight() / 2);

        wrong = new Image(wrongTexture);
        wrong.setName("wrong");
        wrong.setWidth(600 * game.screenWidth / wrongTexture.getWidth());
        wrong.setHeight(600 * game.screenWidth / wrongTexture.getHeight());
        wrong.setPosition(game.screenWidth / 2 - wrong.getWidth() / 2, game.screenHeight / 2 - wrong.getHeight() / 2);

    }

    //generates bitmap font for labels
    private BitmapFont fontGenerator(int size) {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("futur.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = size;
        return generator.generateFont(parameter);

    }

}
