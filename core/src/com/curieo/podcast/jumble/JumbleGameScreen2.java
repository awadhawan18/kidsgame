package com.curieo.podcast.jumble;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.curieo.podcast.AnimatedFullScreen;
import com.curieo.podcast.CommonObjects;
import com.curieo.podcast.KidsGame;

import java.util.Random;

import static com.curieo.podcast.CommonObjects.exerciseData;
import static com.curieo.podcast.CommonObjects.screenHeight;
import static com.curieo.podcast.CommonObjects.screenWidth;

public class JumbleGameScreen2 implements Screen {
    final KidsGame game;
    private Timer timer;
    private Stage stage;
    private Table table;
    private Texture backgroundTexture;
    private BitmapFont closeness;
    private Image win;
    private int correctLetters = 0;
    private String speak;
    private String sentence;
    private String[] words;
    private int type;
    private Color fadedColor = Color.valueOf("#96727272");
    private Color[] colors = {
            Color.RED,
            Color.BROWN,
            Color.CYAN,
            Color.CORAL,
            Color.CHARTREUSE,
            Color.ORANGE,
            Color.FOREST,
            Color.DARK_GRAY
    };


    public JumbleGameScreen2(KidsGame kids, int t) {
        this.game = kids;
        table = new Table();
        stage = new Stage(new FitViewport(game.screenWidth, game.screenHeight));
        timer = new Timer();
        this.type = t;
        Gdx.input.setInputProcessor(stage);

        if (type == 1) {
            backgroundTexture = new Texture(Gdx.files.internal("jumble_background2.png"));
            table.background(new TextureRegionDrawable(new TextureRegion(backgroundTexture)));
        } else {
            backgroundTexture = new Texture(Gdx.files.internal("big_jumble_background2.png"));
            table.background(new TextureRegionDrawable(new TextureRegion(backgroundTexture)));
        }


        closeness = fontGenerator(70);

        sentence = exerciseData.getObject_string();
        words = exerciseData.getWords();


        table.setFillParent(true);
        stage.addActor(table);
        speak = sentence;
        setUpPlaceholders(sentence.toLowerCase());
        setUpActors(words);
        CommonObjects.textToSpeech.speak(speak);
        initializeTryAndWin();


    }


    private void winActor() {
        game.correctSound.play();
        stage.addActor(win);
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                stage.getRoot().findActor("win").remove();

            }
        }, 2);

    }


    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
        stage.act();

    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    private String[] scramble(String[] inputString) {
        Random random = new Random();


        // Scramble the letters using the standard Fisher-Yates shuffle,
        for (int i = 0; i < inputString.length; i++) {
            int j = random.nextInt(inputString.length);
            String temp = inputString[i];
            inputString[i] = inputString[j];
            inputString[j] = temp;
        }

        return inputString;
    }


    //placeholder sentence
    private void setUpPlaceholders(String str) {
        String[] alphabets = str.split("\\s+");


        for (String alphabet : alphabets) {
            Label.LabelStyle style = new Label.LabelStyle(closeness, Color.WHITE);
            for (String word : words) {
                if (alphabet.equalsIgnoreCase(word)) {
                    style = new Label.LabelStyle(closeness, fadedColor);
                    break;
                }
            }
            Label label = new Label(alphabet, style);
            label.setName(alphabet + "placeholder");
            table.add(label).padTop(game.screenHeight / 2).padBottom(game.screenHeight / 2).padLeft(10).padRight(10);
        }
    }

    //sets up labels for drag and drop
    private void setUpActors(String[] str) {

        final String[] alphabets = scramble(str);

        for (int i = 0; i < alphabets.length; i++) {
            Label.LabelStyle style = new Label.LabelStyle(closeness, colors[(int) (Math.random() * 7)]);
            final Label label = new Label(alphabets[i], style);
            final int num = i;
            final float px = (screenWidth / alphabets.length + 1) * i + 2;
            float[] temp = new float[2];
            temp[0] = (screenHeight / 9) * ((float) Math.random() * 2 + 1);
            temp[1] = (screenHeight / 9) * ((float) Math.random() * 2 + 6);
            final float py = temp[(int) (Math.random() * 2)];
            label.setName(alphabets[i] + "actor");
            label.setPosition(px, py);
            label.addListener(new DragListener() {

                public void drag(InputEvent event, float x, float y, int pointer) {
                    label.moveBy(x, y);

                }

                public void dragStop(InputEvent event, float x, float y, int pointer) {
                    Actor actor = stage.getRoot().findActor(alphabets[num].toLowerCase() + "placeholder");
                    if (label.getX() + 75 >= actor.getX() && label.getX() <= actor.getX() + 75 &&
                            label.getY() + 75 >= actor.getY() && label.getY() <= actor.getY() + 75) {
                        label.setPosition(actor.getX(), actor.getY());
                        label.removeListener(this);
                        correctLetters++;
                        if (correctLetters == alphabets.length) {
                            winActor();
                            timer.scheduleTask(new Timer.Task() {
                                @Override
                                public void run() {
                                    JumbleGameScreen3 jmb = new JumbleGameScreen3(game, type);
                                    game.setScreen(jmb);
                                }
                            }, 2);
                        }
                    } else {
                        game.wrongSound.play();
                        label.setPosition(px, py);
                    }


                }

            });

            stage.addActor(label);
        }
    }


    private void initializeTryAndWin() {
        Texture winTexture = new Texture(Gdx.files.internal("actions/win_layer.png"));
        win = new Image(winTexture);
        win.setName("win");

        if (type == 1) {
            float winScaleX = game.screenWidth / winTexture.getWidth();
            float winScaleY = game.screenHeight / winTexture.getHeight();
            win.setWidth(500 * winScaleX);
            win.setHeight(800 * winScaleY);
        } else {
            float winScaleX = game.screenWidth / winTexture.getHeight();
            float winScaleY = game.screenHeight / winTexture.getWidth();
            win.setWidth(600 * winScaleX);
            win.setHeight(600 * winScaleY);
        }

        win.setPosition(game.screenWidth / 2 - win.getWidth() / 2, game.screenHeight / 2 - win.getHeight() / 2);


    }

    //generates bitmap font for labels
    private BitmapFont fontGenerator(int size) {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("futur.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = size;
        return generator.generateFont(parameter);

    }

}
