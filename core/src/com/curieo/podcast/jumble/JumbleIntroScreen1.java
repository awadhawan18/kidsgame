package com.curieo.podcast.jumble;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.curieo.podcast.AnimatedFullScreen;
import com.curieo.podcast.KidsGame;
import com.curieo.podcast.listeners.OnAnimationComplete;

public class JumbleIntroScreen1 implements Screen {

    final KidsGame game;
    Timer timer;
    Stage stage;
    Table table;
    private AnimatedFullScreen animation;
    private AnimatedFullScreen secondAnimation;
    private Texture backgroundTexture;
    private JumbleIntroScreen2 jumbleIntroScreen2;
    private int type;


    /*
        type 2 for landscape
        type 1 for portrait
     */

    public JumbleIntroScreen1(KidsGame kids, int type) {
        this.game = kids;
        table = new Table();
        stage = new Stage(new FitViewport(game.screenWidth, game.screenHeight));
        timer = new Timer();
        this.type = type;
        Gdx.input.setInputProcessor(stage);


        if (type == 1) {
            jumbleIntroScreen2 = new JumbleIntroScreen2(game);
            backgroundTexture = new Texture(Gdx.files.internal("jumble_intro_background1.png"));
            animation = new AnimatedFullScreen("jumble_intro_animation", false, "");

            //sets second intro screen
            animation.setListener(new OnAnimationComplete() {
                @Override
                public void setNextScreen() {
                    game.setScreen(jumbleIntroScreen2);
                }
            });

            table.background(new TextureRegionDrawable(new TextureRegion(backgroundTexture)));
            table.add(animation);
        } else {
            backgroundTexture = new Texture(Gdx.files.internal("big_jumble_intro_background.png"));

            //animation runs when second animation is completed
            animation = new AnimatedFullScreen("big_jumble_starting", false,
                    game.screenWidth / 8, game.screenHeight / 4, false,
                    1, 300, 200);

            secondAnimation = new AnimatedFullScreen("big_jar_animation", false,
                    2 * game.screenWidth / 3, game.screenHeight / 5, true,
                    1, 200, 300);

            animation.setListener(new OnAnimationComplete() {
                @Override
                public void setNextScreen() {
                    game.setScreen(new JumbleGameScreen1(game, 2));
                }
            });
            secondAnimation.setListener(new OnAnimationComplete() {
                @Override
                public void setNextScreen() {
                    animation.startAnimation = true;
                }
            });

            table.background(new TextureRegionDrawable(new TextureRegion(backgroundTexture)));
            table.add(animation);
            table.add(secondAnimation);
        }


        table.setFillParent(true);
        stage.addActor(table);

    }


    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
        stage.act();

    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
