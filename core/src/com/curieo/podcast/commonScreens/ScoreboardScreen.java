package com.curieo.podcast.commonScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.curieo.podcast.CommonObjects;
import com.curieo.podcast.KidsGame;


public class ScoreboardScreen implements Screen {

    final KidsGame game;
    private Stage stage;
    private Table table;
    private Texture scoreCard;
    private Texture winningCard;
    private Texture texture;
    private int type;

    /*
        type 2 for landscape drag and drop
        type 1 other wise
     */

    public ScoreboardScreen(KidsGame game, int type) {
        this.game = game;
        table = new Table();
        stage = new Stage(new FitViewport(game.screenWidth, game.screenHeight));
        this.type = type;

        Gdx.input.setInputProcessor(stage);

        //no. of winning cards
        int wins = CommonObjects.textToSpeech.getWins();

        if (type == 1) {
            texture = new Texture(Gdx.files.internal("score_board_background.png"));
            table.background(new TextureRegionDrawable(new TextureRegion(texture)));
            table.padTop(380 * (game.screenHeight / texture.getHeight())); //scales the padding according to height of screen
            table.padBottom(1000 * (game.screenHeight / texture.getHeight())); //380 and 1000 are the points is background texture.
            setCards(3, 4, wins);
        } else {
            texture = new Texture(Gdx.files.internal("big_jumble_scoreboard.png"));
            table.background(new TextureRegionDrawable(new TextureRegion(texture)));
            table.padTop(200 * (game.screenHeight / texture.getHeight())); //scales the padding according to height of screen
            table.padBottom(170 * (game.screenHeight / texture.getHeight())); //200 and 170 are the points is background texture.
            table.padLeft(200 * (game.screenWidth / texture.getWidth()));
            table.padRight(200 * (game.screenWidth / texture.getWidth()));
            setCards(2, 6, wins);
        }


        table.setFillParent(true);
        stage.addActor(table);


        //setting up back button based on type
        Texture backTexture = new Texture(Gdx.files.internal("actions/stop_button.png"));
        Image backButton = new Image(backTexture);

        backButton.setName("back");

        float textureScaleX;
        float textureScaleY;
        if (type == 1) {
            textureScaleX = (game.screenWidth / 1080);
            textureScaleY = (game.screenHeight / 1920);

            backButton.setWidth(textureScaleX * backTexture.getWidth());
            backButton.setHeight(textureScaleY * backTexture.getHeight());
            backButton.setPosition(game.screenWidth / 2 - backButton.getWidth() / 2, 700 * game.screenHeight / texture.getHeight());

        } else {
            textureScaleX = (game.screenWidth / 1920);
            textureScaleY = (game.screenHeight / 1080);
            backButton.setWidth(textureScaleX * backTexture.getWidth());
            backButton.setHeight(textureScaleY * backTexture.getHeight());
            backButton.setPosition(game.screenWidth / 2 - backButton.getWidth() / 2, 10 * textureScaleY);

        }


        backButton.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Gdx.app.log("here", "label");
                CommonObjects.textToSpeech.pressBack();
                return true;
            }
        });

        stage.addActor(backButton);


    }


    @Override
    public void show() {
        /*for(String s :CommonObjects.responses){
            Gdx.app.log("response",s);
        }*/
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
        stage.act();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        scoreCard.dispose();
        winningCard.dispose();
    }

    //sets the cards with no. rows and columns
    private void setCards(int row, int columns, int wins) {
        for (int j = 0; j < row; j++) {
            for (int i = 1; i <= columns; i++) {
                Stack stack = new Stack();

                //adds winning card on the top of scorecard background
                if (j * columns + i <= wins) {
                    scoreCard = new Texture(Gdx.files.internal("cards/score_card.png"));
                    Image background = new Image(scoreCard);
                    stack.add(background);
                    winningCard = new Texture(Gdx.files.internal("cards/winning_card" + String.valueOf(j * columns + i) + ".png"));
                    Image image = new Image(winningCard);
                    stack.add(image);
                }
                //scorecard on top of winning card to keep the size same for every row
                else {
                    winningCard = new Texture(Gdx.files.internal("cards/winning_card" + String.valueOf(j * columns + i) + ".png"));
                    Image image = new Image(winningCard);
                    stack.add(image);
                    scoreCard = new Texture(Gdx.files.internal("cards/score_card.png"));
                    Image background = new Image(scoreCard);
                    stack.add(background);
                }

                table.add(stack).pad(10);

            }
            table.row();
        }
    }
}
