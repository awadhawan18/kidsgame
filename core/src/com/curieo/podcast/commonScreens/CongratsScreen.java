package com.curieo.podcast.commonScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.curieo.podcast.AnimatedFullScreen;
import com.curieo.podcast.CommonObjects;
import com.curieo.podcast.KidsGame;


public class CongratsScreen implements Screen {

    final KidsGame game;
    private Stage stage;
    private Table table;
    private Timer timer;
    private int type;
    private AnimatedFullScreen animation;
    private int wins;

    /*
        type 2 for landscape drag and drop
        type 1 other wise
     */

    public CongratsScreen(KidsGame kids, int t) {
        this.game = kids;
        table = new Table();
        stage = new Stage(new FitViewport(game.screenWidth, game.screenHeight));
        timer = new Timer();
        type = t;
        game.lastClick = TimeUtils.millis();

        // gets no. of wins from preferences and adds 1


        table.setFillParent(true);
        stage.addActor(table);


    }


    @Override
    public void show() {

        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                Gdx.app.log("Congrats screen", "here");
                game.setScreen(new FinalScreen(game, type));
            }
        }, 3);

        game.music.play();
        wins = CommonObjects.textToSpeech.getWins();
        wins++;
        CommonObjects.textToSpeech.writeWins(wins);

        //winning card texture
        Texture texture = new Texture(Gdx.files.internal("cards/winning_card" + String.valueOf(wins) + ".png"));
        Image finalScreen = new Image(texture);

        if (type == 1) {
            animation = new AnimatedFullScreen("congrats_animation", true, "");

            table.add(animation);
            table.add(finalScreen).padBottom(240 * (game.screenHeight / 1280)).padLeft(145 * (game.screenWidth / 720))
                    .padRight(145 * (game.screenWidth / 720)).padTop(380 * (game.screenHeight / 1280)).fill();
        } else {
            animation = new AnimatedFullScreen("congrats_animation_jumble", true, "");

            table.add(animation);
            table.add(finalScreen).padBottom(180 * (game.screenHeight / 720)).padLeft(500 * (game.screenWidth / 1280))
                    .padRight(500 * (game.screenWidth / 1280)).padTop(230 * (game.screenHeight / 720)).fill();
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
        stage.act();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
