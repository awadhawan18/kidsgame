package com.curieo.podcast.commonScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.curieo.podcast.CommonObjects;
import com.curieo.podcast.KidsGame;


public class FinalScreen implements Screen {

    final KidsGame game;
    private Stage stage;
    private Table table;
    private Timer timer;
    private Texture backgroundTexture;
    private int type;

    /*
        type 2 for landscape drag and drop
        type 1 other wise
     */

    public FinalScreen(KidsGame game, int type) {
        this.game = game;
        table = new Table();
        stage = new Stage(new FitViewport(game.screenWidth, game.screenHeight));
        timer = new Timer();
        this.type = type;

        //gets wins from preferences
        int wins = CommonObjects.textToSpeech.getWins();

        if (type == 1) {
            backgroundTexture = new Texture(Gdx.files.internal("final_screen.png"));
        } else {
            backgroundTexture = new Texture(Gdx.files.internal("big_jumble_final_screen.png"));
        }

        table.background(new TextureRegionDrawable(new TextureRegion(backgroundTexture)));

        float textureScaleX = (game.screenWidth / backgroundTexture.getWidth());
        float textureScaleY = (game.screenHeight / backgroundTexture.getHeight());

        Texture winning = new Texture(Gdx.files.internal("cards/winning_card" + String.valueOf(wins) + ".png"));
        Image winningCard = new Image(winning);

        //actions are performed in a sequence, first moves and then invisible

        if (type == 1) {
            winningCard.setWidth(200 * textureScaleX);
            winningCard.setHeight(300 * textureScaleY);
            winningCard.setPosition(4 * game.screenWidth / 7 - winningCard.getWidth() / 2, game.screenHeight - 100);
            winningCard.addAction(new SequenceAction(
                    Actions.moveTo(4 * game.screenWidth / 7 - winningCard.getWidth() / 2,
                            580 * textureScaleY, 1), Actions.visible(false)));
        } else {
            winningCard.setWidth(100 * textureScaleX);
            winningCard.setHeight(150 * textureScaleY);
            winningCard.setPosition(game.screenWidth / 2 - winningCard.getWidth() / 2,
                    game.screenHeight - winningCard.getHeight());
            winningCard.addAction(new SequenceAction(
                    Actions.moveTo(game.screenWidth / 2 - winningCard.getWidth() / 2,
                            game.screenHeight / 2, 1), Actions.visible(false)));
        }


        table.setFillParent(true);
        stage.addActor(table);
        stage.addActor(winningCard);


    }


    @Override
    public void show() {
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                Gdx.app.log("final screen", "here");
                game.setScreen(new ScoreboardScreen(game, type));
            }
        }, 3);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
        stage.act();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
