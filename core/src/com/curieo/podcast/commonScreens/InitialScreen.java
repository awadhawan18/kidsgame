package com.curieo.podcast.commonScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.curieo.podcast.AnimatedFullScreen;
import com.curieo.podcast.CommonObjects;
import com.curieo.podcast.KidsGame;
import com.curieo.podcast.gameScreens.ColorNumberFlashScreen;
import com.curieo.podcast.gameScreens.WordGameScreen;
import com.curieo.podcast.listeners.OnAnimationComplete;


public class InitialScreen implements Screen {

    final KidsGame game;
    Stage stage;
    Table table;
    AnimatedFullScreen animation;
    public Sound firstSound, secondSound;
    Sound sound;

    /*
        type 1 for color game
        type 2 for number game
        type 3 for flash card game
        type 4 for alphabet first animation
        type 5 for alphabet second animation
     */

    public InitialScreen(KidsGame kids, final int type) {
        this.game = kids;
        table = new Table();
        stage = new Stage(new FitViewport(game.screenWidth, game.screenHeight));
        sound = Gdx.audio.newSound(Gdx.files.internal("sounds/dialogues/name_is_tom.mp3"));


        if (type == 1) {
            animation = new AnimatedFullScreen("walking_animation", false, "name_is_tom");

       } else if (type == 2) {
            animation = new AnimatedFullScreen("walking_baloon_animation", false, "play_and_collect_more_cards");
            sound = Gdx.audio.newSound(Gdx.files.internal("sounds/dialogues/name_is_tom.mp3"));
        } else if (type == 3) {
            animation = new AnimatedFullScreen("walking_home_animation", false, "play_and_collect_more_cards");
        } else if (type == 4) {
            animation = new AnimatedFullScreen("alphabet_hello_animation", false, "play_and_collect_more_cards");
        } else if (type == 5) {
            animation = new AnimatedFullScreen("alphabet_intro_animation", false, "name_is_tom");
        }


        //on animation complete sets next screen
        animation.setListener(new OnAnimationComplete() {
            @Override
            public void setNextScreen() {
                if (type == 4) {
                    game.setScreen(new InitialScreen(game, 5));
                } else if (type == 5) {
                    game.setScreen(new WordGameScreen(game, 0, CommonObjects.exerciseData));
                } else {
                    ColorNumberFlashScreen FCS = new ColorNumberFlashScreen(game, type);
                    game.setScreen(FCS);
                }

            }
        });


        stage.addActor(animation);

    }


    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
        stage.act();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        sound.dispose();
    }
}
