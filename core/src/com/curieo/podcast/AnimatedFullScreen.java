package com.curieo.podcast;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.curieo.podcast.listeners.OnAnimationComplete;

//class for animations in full screen
public class AnimatedFullScreen extends Actor {

    private OnAnimationComplete mOnAnimationCompleterListener;
    private Animation<TextureRegion> animation;
    private TextureAtlas atlas;
    private float scaleX, scaleY;
    private float px = 0, py = 0;
    private float animationWidth, animationHeight;
    private int type = 0;
    private float showTime;
    private boolean loop; //animation looping or not
    public boolean startAnimation = true;
    public Sound sound;

    //default constructor
    public AnimatedFullScreen(String atlasName, boolean loop, String soundname) {
        atlas = new TextureAtlas(Gdx.files.internal("animations/" + atlasName + ".atlas"));

        this.animation = new Animation<TextureRegion>(2 / 10f, atlas.getRegions());
        this.scaleX = CommonObjects.screenWidth / atlas.getRegions().get(0).getRegionWidth();
        this.scaleY = CommonObjects.screenHeight / atlas.getRegions().get(0).getRegionHeight();
        this.loop = loop;
    }
    /*
        type 0 default for default animation width and height
        type 1 for specific width and height
        other parameter usual meaning
     */

    public AnimatedFullScreen(String atlasName, boolean loop, float px, float py, boolean startAnimation, int type,
                              float animationWidth, float animationHeight) {
        atlas = new TextureAtlas(Gdx.files.internal("animations/" + atlasName + ".atlas"));
        this.animation = new Animation<TextureRegion>(4 / 10f, atlas.getRegions());
        this.scaleX = CommonObjects.screenWidth / atlas.getRegions().get(0).getRegionWidth();
        this.scaleY = CommonObjects.screenHeight / atlas.getRegions().get(0).getRegionHeight();
        this.loop = loop;
        this.px = px;
        this.py = py;
        this.startAnimation = startAnimation;
        this.type = type;
        this.animationWidth = animationWidth;
        this.animationHeight = animationHeight;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        showTime += delta;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {

        if (startAnimation) {

            batch.draw(animation.getKeyFrame(showTime, loop), px, py, 0, 0,
                    type == 0 ? animation.getKeyFrame(showTime).getRegionWidth() : animationWidth,
                    type == 0 ? animation.getKeyFrame(showTime).getRegionHeight() : animationHeight,
                    type == 0 ? scaleX : scaleY,
                    type == 0 ? scaleY : scaleY, 0);
        }

        //shows still image when animation not started
        else {
            batch.draw(animation.getKeyFrame(0, loop), px, py, 0, 0,
                    type == 0 ? animation.getKeyFrame(showTime).getRegionWidth() : animationWidth,
                    type == 0 ? animation.getKeyFrame(showTime).getRegionHeight() : animationHeight,
                    type == 0 ? scaleX : scaleY,
                    type == 0 ? scaleY : scaleY, 0);
        }

        //fires listener when animation completed
        if (animation.isAnimationFinished(showTime)) {
            if (mOnAnimationCompleterListener != null) {
                mOnAnimationCompleterListener.setNextScreen();


            }
        }

    }

    public void setListener(OnAnimationComplete listener) {
        mOnAnimationCompleterListener = listener;
    }
}
