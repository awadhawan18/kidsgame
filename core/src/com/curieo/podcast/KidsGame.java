package com.curieo.podcast;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.curieo.podcast.commonScreens.CongratsScreen;
import com.curieo.podcast.interfaces.ImageLoaderCore;
import com.curieo.podcast.interfaces.UrlHandler;
import com.curieo.podcast.jumble.JumbleIntroScreen1;
import com.curieo.podcast.listeners.OnExerciseLoaded;
import com.curieo.podcast.listeners.OnSpeechResults;
import com.curieo.podcast.interfaces.SpeechTextCore;
import com.curieo.podcast.interfaces.TextSpeechCore;
import com.curieo.podcast.commonScreens.InitialScreen;
import com.curieo.podcast.gameScreens.ShapeGameScreen;

import static com.curieo.podcast.CommonObjects.exerciseData;

public class KidsGame extends Game {
    public SpriteBatch batch;
    public BitmapFont font; //fonts
    public BitmapFont font2;
    public int incorrect;
    public float screenWidth, screenHeight;
    public Image speakButton, playButton; //standard play and speak buttons
    public Image blinkButton1, blinkButton2; // blinks when user taps

    public String speechOutput;

    //2 standard screens
    public InitialScreen INS;

    public OnSpeechResults mOnSpeechResults; //listener for interfaces results
    public boolean backpressed = false;


    public long lastClick; //holds the time in millis

    //tells the render function when to start blinking speak button
    public boolean startBlinking = false;

    //true when first screen is set
    private boolean gameStarted = false;

    public Sound correctSound, wrongSound;
    //1001 alphabet game
    //   1. Hi friend  My name is Tom. 2. Lets play word game fill the car and win the jar
    //1002-1005
    // 1 dialogue
    //2001-3001
    //1.  hi friends abkwas welcome to play on the beach   2.  Lets play drag and drop game and win the cards and fill the jar
    //4001 -6001
    //1.  hi friends abkwas welcome to space  2.  Lets play drag and drop game and win the cards and fill the jar



    public Music music;


    public KidsGame(TextSpeechCore textToSpeech, SpeechTextCore speechToText, ImageLoaderCore imageLoader,
                    UrlHandler urlHandler, int width) {
        CommonObjects.textToSpeech = textToSpeech;
        CommonObjects.speechToText = speechToText;
        CommonObjects.imageLoader = imageLoader;
        CommonObjects.urlHandler = urlHandler;
        screenWidth = width;

        screenHeight = (float) 16 * screenWidth / 9; // fixed 16:9 resolution

        CommonObjects.screenHeight = screenHeight;
        CommonObjects.screenWidth = screenWidth;

        //testing data when server is not available
        CommonObjects.exerciseData = new ExerciseData();
        exerciseData.setExType(1003);
        exerciseData.setGrade("N");
        exerciseData.setObject_string("black Lives in water");
        exerciseData.setSentences(new String[]{"School lives in water", "school lives in water", "Fish lives in water"});
        exerciseData.setWords(new String[]{"black",
                "blue",
                "green",
                "yellow",
                "red"});
        exerciseData.setImage_urls(new String[]{"https://s3.ap-south-1.amazonaws.com/blackboardradioandroidcontent/UKG/Drag+and+Drop+Game/1.+Fish+lives+in+water..png",
                "https://s3.ap-south-1.amazonaws.com/blackboardradioandroidcontent/Nursery/Fish+Alphabet+Game/Final/aeroplane.png",
                "https://s3.ap-south-1.amazonaws.com/blackboardradioandroidcontent/Nursery/Fish+Alphabet+Game/Final/alligator.png",
                "https://s3.ap-south-1.amazonaws.com/blackboardradioandroidcontent/Nursery/Fish+Alphabet+Game/Final/acorn.png"});

    }

    @Override
    public void create() {

        batch = new SpriteBatch();


        //font for shape and alphabet games
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("JelleeRoman.otf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 300;
        font2 = generator.generateFont(parameter);
        parameter.size = 50;
        font = generator.generateFont(parameter);
        font2.setColor(Color.PURPLE);


        incorrect = 0; //holds the no. of incorrect responses

        //sounds for user responses.
        correctSound = Gdx.audio.newSound(Gdx.files.internal("sounds/correct_sound.mp3"));
        wrongSound = Gdx.audio.newSound(Gdx.files.internal("sounds/wrong_sound.mp3"));

        //background music
        music = Gdx.audio.newMusic(Gdx.files.internal("sounds/all_time_sound.mp3"));
        music.setLooping(true);
        //music.play();

        //calls server for exercise and listener
        CommonObjects.urlHandler.setOnExerciseLoadedListener(new OnExerciseLoaded() {
            @Override
            public void setScreen() {
                exerciseData = CommonObjects.urlHandler.getExercise();
                gameStarted = true;
            }
        });

        Gdx.app.log("screen", String.valueOf(screenHeight));
        CommonObjects.urlHandler.callServer();


        //initializes play and speak button
        initializeButtons();


    }

    //sets the screen for color, number, flash card, and alphabet game.
    /*
        type 1 for color game
        type 2 for number game
        type 3 for flash card game
        type 4 for alphabet first animation
        type 5 for alphabet second animation
     */
    private void screen(int type) {
        INS = new InitialScreen(this, type);
        this.setScreen(INS);
    }

    @Override
    public void render() {
        super.render(); //important!

        //true when exercise is loaded from server
        if (gameStarted) {
            if (exerciseData != null) {

                if (exerciseData.getExType() == 2001 || exerciseData.getExType() == 3001) {
                    this.setScreen(new JumbleIntroScreen1(this, 1));
                } else if (exerciseData.getExType() == 4001 || exerciseData.getExType() == 5001 || exerciseData.getExType() == 6001) {
                    CommonObjects.textToSpeech.changeOrientation(); //changes orientation to landscape
                    changeScreenMetrics();
                    this.setScreen(new JumbleIntroScreen1(this, 2));

                } else if (exerciseData.getExType() == 1005) {
                    screen(3);
                } else if (exerciseData.getExType() == 1004) {
                    screen(2);
                } else if (exerciseData.getExType() == 1003) {
                    screen(1);
                } else if (exerciseData.getExType() == 1002) {
                    this.setScreen(new ShapeGameScreen(this, 0, exerciseData));
                } else if (exerciseData.getExType() == 1001) {
                    screen(4);
                }

            }
            gameStarted = false;

        }

    }

    @Override
    public void dispose() {
        batch.dispose();
        correctSound.dispose();
        wrongSound.dispose();
    }

    public void showToast(String message) {
        CommonObjects.speechToText.showToast(message, 4000);
    }

    public void setTextFieldText(String text) {
        speechOutput = text;
        if (mOnSpeechResults != null) {
            mOnSpeechResults.checkResult();
        }
    }

    //listener for speech results
    public void setSpeechListener(OnSpeechResults onSpeechResults) {
        mOnSpeechResults = onSpeechResults;
    }

    public void checkPermissions() {
        CommonObjects.speechToText.checkRecord();
    }


    //initializes play speak and blink buttons for games
    private void initializeButtons() {
        Texture playTexture = new Texture(Gdx.files.internal("actions/play_button.png"));
        Texture speakTexture = new Texture(Gdx.files.internal("actions/speak_button.png"));
        Texture blinkTexture = new Texture(Gdx.files.internal("actions/blink_button.png"));


        playButton = new Image(playTexture);
        speakButton = new Image(speakTexture);
        playButton.setName("play");
        speakButton.setName("speak");

        blinkButton1 = new Image(blinkTexture);
        blinkButton2 = new Image(blinkTexture);

        float textureScaleX = (screenWidth / 1080);
        float textureScaleY = (screenHeight / 1920);

        playButton.setWidth(textureScaleX * playTexture.getWidth());
        playButton.setHeight(textureScaleY * playTexture.getHeight());
        speakButton.setWidth(textureScaleX * speakTexture.getWidth());
        speakButton.setHeight(textureScaleY * speakTexture.getHeight());

        blinkButton1.setWidth(textureScaleX * playTexture.getWidth());
        blinkButton1.setHeight(textureScaleY * playTexture.getHeight());
        blinkButton2.setWidth(textureScaleX * speakTexture.getWidth());
        blinkButton2.setHeight(textureScaleY * speakTexture.getHeight());


        playButton.setPosition(screenWidth / 2 - (screenWidth / 8) - playButton.getWidth(), 70 * (screenHeight / 1280));
        speakButton.setPosition(screenWidth / 2 + (screenWidth / 8), 70 * (screenHeight / 1280));

        blinkButton1.setPosition(screenWidth / 2 - (screenWidth / 8) - playButton.getWidth(), 70 * (screenHeight / 1280));
        blinkButton2.setPosition(screenWidth / 2 + (screenWidth / 8), 70 * (screenHeight / 1280));

        blinkButton1.setVisible(false); //only visible when user taps the screen
        blinkButton2.setVisible(false); //starts blinking when listening for interfaces
    }

    //exchanges width and height variables for landscape game
    private void changeScreenMetrics() {
        float temp = screenHeight;
        screenHeight = screenWidth;
        screenWidth = temp;
        CommonObjects.screenWidth = screenWidth;
        CommonObjects.screenHeight = screenHeight;
    }
}
