package com.curieo.podcast.gameScreens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.curieo.podcast.CommonObjects;
import com.curieo.podcast.KidsGame;
import com.curieo.podcast.commonScreens.CongratsScreen;

import java.util.ArrayList;

public class ImageQuizScreen implements Screen {

    private final KidsGame game;
    private int incorrect = 0; //holds no of incorrect responses
    private ArrayList<Texture> textures;
    private Stage stage;
    private Table table;
    private String speak;
    private Timer timer;
    private int imageCounter = 0;
    private Image progressBarCat;
    private float duration = 1;

    /*
        texture array from flash card game
     */

    public ImageQuizScreen(KidsGame kids, ArrayList<Texture> textureArray, final String[] words) {

        this.game = kids;
        table = new Table();
        stage = new Stage(new FitViewport(game.screenWidth, game.screenHeight));
        textures = textureArray;
        timer = new Timer();
        duration = 1;
        Gdx.input.setInputProcessor(stage); //important!!

        final Texture texture = new Texture(Gdx.files.internal("image_quiz_game_background.jpg"));
        table.background(new TextureRegionDrawable(new TextureRegion(texture)));

        progressBarCat = new Image(new Texture(Gdx.files.internal("progress_bar_cat.png")));
        progressBarCat.setWidth(160 * (game.screenWidth / texture.getWidth()));
        progressBarCat.setHeight(300 * (game.screenHeight / texture.getHeight()));
        progressBarCat.setPosition(50 * (game.screenWidth / texture.getWidth()), 50 * (game.screenHeight / texture.getHeight()));

        table.padTop(100 * (game.screenHeight / texture.getHeight()));
        table.padBottom(850 * (game.screenHeight / texture.getHeight()));
        speak = words[imageCounter];


        /*
         * background card and image
         * and try again, wrong and correct response cards
         * in a single stack
         * aligned in the table with 4 other stacks
         */

        for (int i = 0; i < 4; i++) {
            Texture backgroundCard = new Texture(Gdx.files.internal("cards/background_card.png"));
            Texture right = new Texture(Gdx.files.internal("cards/right_answer_card.png"));
            final Texture tryAgain = new Texture(Gdx.files.internal("cards/try_again_card.png"));
            final Texture wrong = new Texture(Gdx.files.internal("cards/wrong_answer_card.png"));

            final Image background = new Image(backgroundCard);
            Image rightCard = new Image(right);
            Image wrongCard = new Image(wrong);
            final Image tryAgainCard = new Image(tryAgain);

            background.setName(words[i] + "background");
            rightCard.setName(words[i] + "right");
            wrongCard.setName(words[i] + "wrong");
            tryAgainCard.setName(words[i] + "try");

            rightCard.setVisible(false);
            wrongCard.setVisible(false);
            tryAgainCard.setVisible(false);

            final Image image = new Image(textures.get(i));
            image.setName(words[i]);


            final Stack stack = new Stack();
            stack.add(background);
            stack.add(image);
            stack.add(rightCard);
            stack.add(wrongCard);
            stack.add(tryAgainCard);
            stack.setName(words[i]);

            //action reduces the image with in 1 second to give the impression of rotaion
            //find each card from stage with name
            //progress bar cat moves when correct or wrong answer
            stack.addListener(new InputListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    Gdx.app.log("listener", stack.getName());
                    if (speak.equalsIgnoreCase(stack.getName())) {
                        game.correctSound.play();
                        Actor actor = stage.getRoot().findActor(speak + "right");
                        actor.addAction(Actions.scaleTo(0, 1));
                        background.addAction(Actions.scaleTo(0, 1, duration));
                        image.addAction(Actions.scaleTo(0, 1, duration));
                        actor.addAction(Actions.scaleTo(1, 1, duration));
                        actor.setVisible(true);
                        imageCounter++;
                        progressBarCat.addAction(Actions.moveBy(game.screenWidth / 5, 0, 1));
                        if (imageCounter >= words.length) {
                            timer.scheduleTask(new Timer.Task() {
                                @Override
                                public void run() {
                                    game.setScreen(new CongratsScreen(game, 1));
                                }
                            }, 2);
                        } else {
                            speak = words[imageCounter];
                        }
                        stack.removeListener(this);
                        incorrect = 0;
                    } else {
                        game.wrongSound.play();
                        incorrect++;
                        if (incorrect % 2 == 0) {
                            Stack stack1 = stage.getRoot().findActor(speak);
                            Actor actor = stage.getRoot().findActor(speak + "wrong");
                            actor.addAction(Actions.scaleTo(0, 1));
                            Actor background = stage.getRoot().findActor(speak + "background");
                            Actor image = stage.getRoot().findActor(speak);
                            background.addAction(Actions.scaleTo(0, 1, duration));
                            image.addAction(Actions.scaleTo(0, 1, duration));
                            actor.addAction(Actions.scaleTo(1, 1, duration));
                            actor.setVisible(true);
                            stack1.clearListeners();
                            imageCounter++;
                            progressBarCat.addAction(Actions.moveBy(game.screenWidth / 5, 0, 1));
                            if (imageCounter >= words.length) {
                                timer.scheduleTask(new Timer.Task() {
                                    @Override
                                    public void run() {
                                        Actor actor = stage.getRoot().findActor("play");
                                        actor.remove();
                                        game.setScreen(new CongratsScreen(game, 1));
                                    }
                                }, 2);
                            } else {
                                speak = words[imageCounter];
                            }
                            incorrect = 0;
                        } else {
                            tryAgainCard.addAction(Actions.scaleTo(0, duration));
                            background.addAction(Actions.scaleTo(0, 1, duration));
                            image.addAction(Actions.scaleTo(0, 1, duration));
                            tryAgainCard.addAction(Actions.scaleTo(1, 1, duration));
                            tryAgainCard.setVisible(true);
                            timer.scheduleTask(new Timer.Task() {
                                @Override
                                public void run() {
                                    background.addAction(Actions.scaleTo(0, 1));
                                    image.addAction(Actions.scaleTo(0, 1));
                                    tryAgainCard.addAction(Actions.scaleTo(0, 1, duration));
                                    background.addAction(Actions.scaleTo(1, 1, duration));
                                    image.addAction(Actions.scaleTo(1, 1, duration));

                                }
                            }, 2);
                        }
                    }
                    return true;
                }
            });


            table.add(stack).pad(10);
            if (i % 2 == 1) {
                table.row();
            }

        }

        //Center play button
        Texture playTexture = new Texture(Gdx.files.internal("actions/play_button.png"));
        Image playButton = new Image(playTexture);
        playButton.setName("play");
        float textureScaleX = (game.screenWidth / 1080);
        float textureScaleY = (game.screenHeight / 1920);

        playButton.setWidth(textureScaleX * playTexture.getWidth());
        playButton.setHeight(textureScaleY * playTexture.getHeight());

        playButton.setPosition(game.screenWidth / 2 - playButton.getWidth() / 2, 230 * (game.screenHeight / 1280));
        game.blinkButton1.setPosition(game.screenWidth / 2 - playButton.getWidth() / 2, 230 * (game.screenHeight / 1280));
        playButton.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                CommonObjects.textToSpeech.speak(speak);
                game.blinkButton1.setVisible(true);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                game.blinkButton1.setVisible(false);
            }
        });



        table.setFillParent(true);
        stage.addActor(table);
        stage.addActor(playButton);
        stage.addActor(game.blinkButton1);
        stage.addActor(progressBarCat);


    }


    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
        stage.act();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
