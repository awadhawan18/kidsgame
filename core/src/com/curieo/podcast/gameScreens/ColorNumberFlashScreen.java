package com.curieo.podcast.gameScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.curieo.podcast.AnimatedFullScreen;
import com.curieo.podcast.CommonObjects;
import com.curieo.podcast.KidsGame;
import com.curieo.podcast.commonScreens.CongratsScreen;
import com.curieo.podcast.listeners.OnImageLoaded;
import com.curieo.podcast.listeners.OnSpeechResults;

import java.util.ArrayList;

import static com.curieo.podcast.CommonObjects.exerciseData;


public class ColorNumberFlashScreen implements Screen {

    private final KidsGame game;
    private Timer timer;
    private Stage stage;
    private Table table;
    private Stack stack;
    private ArrayList<Texture> textures;
    private Image wrong, win; // correct answer image and wrong answer image
    private boolean textureLoaded = false;
    private String[] words;
    private int wordNum = 0; //for color and number game
    private String[] url;  //holds urls for flash game
    private int imageCounter; //image counter for flash game
    private String speak;
    private int type;
    private Image backgroundCard; //background card for flash game
    private AnimatedFullScreen animation;
    private Texture backgroundTexture;
    private FitViewport viewport;

    /*
        type 1 for color game
        type 2 for number game
        type 3 for flash card game
     */

    public ColorNumberFlashScreen(KidsGame kids, final int type) {
        this.game = kids;
        table = new Table();
        viewport = new FitViewport(game.screenWidth, game.screenHeight);
        stage = new Stage();
        stage.setViewport(viewport);
        stack = new Stack();
        timer = new Timer();
        this.type = type;
        Gdx.input.setInputProcessor(stage);


        words = exerciseData.getWords();
        CommonObjects.responses = new String[words.length];

        //sets up game for color, number game
        if (type == 1) {
            animation = new AnimatedFullScreen(words[0] + "_fish", false, "");
            animation.setName(words[0]);
            backgroundTexture = new Texture(Gdx.files.internal("fish_game_background.jpg"));
            table.background(new TextureRegionDrawable(new TextureRegion(backgroundTexture)));
            table.setFillParent(true);
            stage.addActor(table);
            stage.addActor(animation);
            speak = words[0];
            CommonObjects.textToSpeech.speak(speak);
        } else if (type == 2) {
            convertNumbers();
            animation = new AnimatedFullScreen(words[0], false, "");
            animation.setName(words[0]);
            backgroundTexture = new Texture(Gdx.files.internal("number_game_background.png"));
            table.background(new TextureRegionDrawable(new TextureRegion(backgroundTexture)));
            table.setFillParent(true);
            stage.addActor(table);
            stage.addActor(animation);
            speak = words[0];
            CommonObjects.textToSpeech.speak(speak);
        } else {
            backgroundTexture = new Texture(Gdx.files.internal("flashcard_game_background.jpg"));
            table.background(new TextureRegionDrawable(new TextureRegion(backgroundTexture)));
            table.setFillParent(true);
            stage.addActor(table);
        }


        initializeTryAndWin();  // for correct and wrong image

        game.setSpeechListener(new OnSpeechResults() {
            @Override
            public void checkResult() {
                if (game.speechOutput.equalsIgnoreCase(speak)) {
                    game.correctSound.play();
                    game.incorrect = 0;
                    stage.addActor(win);

                    //delay for correct answer image to get over
                    timer.scheduleTask(new Timer.Task() {
                        @Override
                        public void run() {
                            if (type == 3) {
                                setUpFlashScreen(); //for flash card game

                            } else {
                                CommonObjects.responses[wordNum] = game.speechOutput;
                                setUpNextWord(); //for color and number game

                            }
                            stage.getRoot().findActor("win").remove();
                        }
                    }, 2);

                } else {
                    tryAgain();
                    game.incorrect++;
                    if (game.incorrect == 2) {
                        game.showToast("We will try this word later");
                        game.incorrect = 0;

                        //delay for try again image to get over
                        timer.scheduleTask(new Timer.Task() {
                            @Override
                            public void run() {
                                if (type == 3) {
                                    setUpFlashScreen();

                                } else {
                                    CommonObjects.responses[wordNum] = game.speechOutput;
                                    setUpNextWord();
                                }
                            }
                        }, 2);
                    } else {
                        game.showToast("Try Again");
                    }
                }
                game.startBlinking = false; //stops the blinking button

            }
        });


        //play button listener
        game.playButton.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                CommonObjects.textToSpeech.speak(speak);
                game.blinkButton1.setVisible(true);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                game.blinkButton1.setVisible(false);
            }
        });

        //speak button listener
        game.speakButton.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                game.checkPermissions();
                game.lastClick = TimeUtils.millis();
                CommonObjects.speechToText.promptSpeechInput();
                game.startBlinking = true;
                return true;
            }
        });



        stage.addActor(game.playButton);
        stage.addActor(game.speakButton);
        stage.addActor(game.blinkButton1);
        stage.addActor(game.blinkButton2);


        //Sets up screen for flash card gamge
        if (type == 3) {
            Texture cardTexture = new Texture(Gdx.files.internal("cards/background_card.png"));
            backgroundCard = new Image(cardTexture);
            float cardScaleX = game.screenWidth / cardTexture.getWidth();
            float cardScaleY = game.screenHeight / cardTexture.getHeight();
            backgroundCard.setName("card");
            backgroundCard.setWidth((cardTexture.getWidth() - 100) * cardScaleX);
            backgroundCard.setHeight((cardTexture.getHeight() - 600) * cardScaleY);
            backgroundCard.setPosition(game.screenWidth / 2 - backgroundCard.getWidth() / 2,
                    game.screenHeight / 2 - backgroundCard.getHeight() / 2
                            + 100 * (game.screenHeight / backgroundTexture.getHeight()));

            words = exerciseData.getWords();
            url = exerciseData.getImageUrls();
            imageCounter = 0;
            textures = new ArrayList<Texture>();

            //texture loaded is true when image downloads
            CommonObjects.imageLoader.setOnImageLoadedListener(new OnImageLoaded() {
                @Override
                public void setTexture() {
                    Gdx.app.log("here", "texture loaded");
                    textureLoaded = true;


                }
            });
            CommonObjects.imageLoader.loadImage(url[imageCounter]);
            imageCounter++;


            stage.addActor(backgroundCard);
        }
    }

    //Shows wrong answer image for 2 seconds
    private void tryAgain() {
        game.wrongSound.play();
        stage.addActor(wrong);
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                stage.getRoot().findActor("wrong").remove();

            }
        }, 2);

    }

    //sets up next animation for color and number game
    private void setUpNextWord() {
        Actor actor = stage.getRoot().findActor(words[wordNum]);
        actor.remove();
        wordNum++;
        if (wordNum < words.length) {
            if (type == 1) {
                animation = new AnimatedFullScreen(words[wordNum] + "_fish", false, "");
            } else animation = new AnimatedFullScreen(words[wordNum], false, "");

            animation.setName(words[wordNum]);
            speak = words[wordNum];
            CommonObjects.textToSpeech.speak(speak);
            stage.addActor(animation);
        } else {
            game.setScreen(new CongratsScreen(game, 1));
            Actor play = stage.getRoot().findActor("play");
            play.remove();
            Actor speak = stage.getRoot().findActor("speak");
            speak.remove();
        }
    }

    //sets up next image for flash card game
    private void setUpFlashScreen() {
        CommonObjects.responses[imageCounter - 1] = game.speechOutput;
        Actor actor = stage.getRoot().findActor(speak);
        actor.remove();

        if (imageCounter < words.length) {
            CommonObjects.imageLoader.loadImage(url[imageCounter]);
            imageCounter++;
        } else {
            ImageQuizScreen IQS = new ImageQuizScreen(game, textures, words);
            game.setScreen(IQS);
            Actor play = stage.getRoot().findActor("play");
            play.remove();
            Actor speak = stage.getRoot().findActor("speak");
            speak.remove();
        }
    }

    @Override
    public void show() {
        if (game.music.isPlaying()) {
            game.music.pause();
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.draw();
        stage.act();

        //true when image is downloaded
        if (textureLoaded) {
            Texture texture = CommonObjects.imageLoader.getImage();
            textures.add(texture);
            Image image = new Image(textures.get(imageCounter - 1));
            image.setName(words[imageCounter - 1]);
            speak = words[imageCounter - 1];
            CommonObjects.textToSpeech.speak(speak);
            Actor actor = stage.getRoot().findActor("card");
            image.setScale(backgroundCard.getWidth() / texture.getWidth(), 1);
            image.setX(actor.getX());
            image.setY(actor.getY() + actor.getHeight() / 2);

            stage.addActor(image);
            textureLoaded = false;
        }

        //renders blinking button for speak button
        if (game.startBlinking) {
            if (TimeUtils.millis() - game.lastClick > 200) {
                if (game.blinkButton2.isVisible()) {
                    game.blinkButton2.setVisible(false);
                } else {
                    game.blinkButton2.setVisible(true);
                }
                game.lastClick = TimeUtils.millis();
            }
        } else {
            game.blinkButton2.setVisible(false);
        }
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    // for correct and wrong image
    private void initializeTryAndWin() {
        Texture winTexture = new Texture(Gdx.files.internal("actions/win_layer.png"));
        Texture wrongTexture = new Texture(Gdx.files.internal("actions/try_again.png"));

        win = new Image(winTexture);
        win.setName("win");
        float winScaleX = game.screenWidth / winTexture.getWidth();
        float winScaleY = game.screenHeight / winTexture.getHeight();

        win.setWidth(500 * winScaleX);
        win.setHeight(800 * winScaleY);
        win.setPosition(game.screenWidth / 2 - win.getWidth() / 2, game.screenHeight / 2 - win.getHeight() / 2);

        wrong = new Image(wrongTexture);
        wrong.setName("wrong");
        wrong.setWidth(500 * game.screenWidth / wrongTexture.getWidth());
        wrong.setHeight(600 * game.screenWidth / wrongTexture.getHeight());
        wrong.setPosition(game.screenWidth / 2 - wrong.getWidth() / 2, game.screenHeight / 2 - wrong.getHeight() / 2);
    }

    //convertion string numbers to integers using Numbers enum
    private void convertNumbers() {
        for (int i = 0; i < words.length; i++) {
            Numbers shape = Numbers.valueOf(words[i].toLowerCase());
            switch (shape) {
                case one:
                    words[i] = "1";
                    break;
                case two:
                    words[i] = "2";
                    break;
                case three:
                    words[i] = "3";
                    break;
                case four:
                    words[i] = "4";
                    break;
                case five:
                    words[i] = "5";
                    break;
                case six:
                    words[i] = "6";
                    break;
                case seven:
                    words[i] = "7";
                    break;
                case eight:
                    words[i] = "8";
                    break;
                case nine:
                    words[i] = "9";
                    break;

            }
        }
    }

    public enum Numbers {
        one,
        two,
        three,
        four,
        five,
        six,
        seven,
        eight,
        nine
    }
}
