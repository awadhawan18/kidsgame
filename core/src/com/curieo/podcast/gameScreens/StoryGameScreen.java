package com.curieo.podcast.gameScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.curieo.podcast.CommonObjects;
import com.curieo.podcast.ExerciseData;
import com.curieo.podcast.KidsGame;


public class StoryGameScreen implements Screen {
    public final KidsGame game;
    public static int imageCounter;
    private String speak;
    private boolean listen =false;
    private boolean getText = true;
    private String[] animals;
    private String[] storylines;
    private String[] url;
    private Texture texture;
    private Array<Texture> textures;
    private Stage stage;
    private Image image;
    private boolean drawAll = false;
    private ExerciseData exercise;


    public StoryGameScreen(final KidsGame game,ExerciseData exerciseData){
        this.game = game;
        this.exercise = exerciseData;
        storylines = exercise.getSentences();
        animals = exercise.getWords();
        url = exercise.getImageUrls();
        stage = new Stage(new ScreenViewport());
        Gdx.input.setInputProcessor(stage);


        textures = new Array<Texture>();

        imageCounter = 0;
        nextImage();
    }

    private void nextImage() {
        if(imageCounter<url.length){
            CommonObjects.imageLoader.loadImage(url[imageCounter], (int) game.screenWidth, (int) game.screenHeight);
            speak = storylines[imageCounter];
            getText = true;
        }

        else {
            getImageArray();
        }
        imageCounter++;


    }

    @Override
    public void show() {
        if (game.music.isPlaying()) {
            game.music.pause();
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        game.batch.begin();
        if (texture != null && !drawAll && imageCounter<storylines.length) {
            game.batch.draw(texture,0 ,0);
            game.font.draw(game.batch,speak,50,100);
            game.font.draw(game.batch,"Play",game.screenWidth-150,game.screenHeight-50);
            game.font.draw(game.batch,"Speak",50,game.screenHeight-50);

        } else if(!drawAll) {
            game.font.draw(game.batch, "Downloading...", 100,100);
        }


        else if(drawAll){
            stage.act();
            stage.draw();
        }


        game.batch.end();
        if(getText){
            if (CommonObjects.imageLoader.getImage() != null) {
                texture = CommonObjects.imageLoader.getImage();
                getText = false;
            }
        }


        if (Gdx.input.isTouched() && (TimeUtils.millis() - game.lastClick) > 500 ) {
            int x = Gdx.input.getX();
            int y = Gdx.input.getY();
            if(x>=game.screenWidth-150 && x<=game.screenWidth && y<=100){
                CommonObjects.textToSpeech.speak(speak);
            }
            else if(x>=50 && x<=200 && y<=100){
                game.checkPermissions();
                CommonObjects.speechToText.promptSpeechInput();
                listen = true;
            }
            game.lastClick = TimeUtils.millis();
        }
        if(listen){
            if(drawAll){
                if(game.speechOutput!=null){
                    game.showToast("Good Job");
                    game.speechOutput = null;
                    Actor actor = stage.getRoot().findActor(speak);
                    actor.remove();

                    listen = false;
                }
            }
            if(game.speechOutput!=null){
                    game.showToast("Good Job");
                    game.speechOutput = null;
                    nextImage();


                listen = false;
            }
        }

        if (Gdx.input.isKeyPressed(Input.Keys.BACK) && (TimeUtils.millis() - game.lastClick) > 500){
            game.lastClick = TimeUtils.millis();
        }



    }

    private void getImageArray(){
        Image background = new Image(new Texture(Gdx.files.internal("background.jpg")));
        stage.addActor(background);
        texture = new Texture(Gdx.files.internal("cat.jpg"));
        textures.add(texture);
        texture = new Texture(Gdx.files.internal("dog.jpg"));
        textures.add(texture);
        texture = new Texture(Gdx.files.internal("tiger.jpg"));
        textures.add(texture);
        texture = new Texture(Gdx.files.internal("lion.jpg"));
        textures.add(texture);
        float width = game.screenWidth/textures.size+1;
        for(int i=0;i<textures.size;i++){
            final int num = i;
            image = new Image(textures.get(i));
            image.setPosition((i+1)*width-image.getWidth()/2,game.screenHeight/2-image.getHeight()/2);
            image.setName(animals[i]);
            image.addListener(new InputListener(){
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    speak = animals[num];
                    CommonObjects.speechToText.promptSpeechInput();
                    listen = true;
                    return true;
                }
            });
            drawAll = true;
            stage.addActor(image);

        }

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        texture.dispose();
    }
}
