package com.curieo.podcast.interfaces;


import com.curieo.podcast.ExerciseData;
import com.curieo.podcast.listeners.OnExerciseLoaded;

public interface UrlHandler {
    void callServer();
    void postServer();

    void setOnExerciseLoadedListener(OnExerciseLoaded listener);
    ExerciseData getExercise();
}
