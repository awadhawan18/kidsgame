package com.curieo.podcast.interfaces;


import com.badlogic.gdx.graphics.Texture;
import com.curieo.podcast.listeners.OnImageLoaded;

public interface ImageLoaderCore {
    void loadImage(String url);
    void loadImage(String url,int width,int height);
    void setOnImageLoadedListener(OnImageLoaded listener);
    Texture getImage();
}
