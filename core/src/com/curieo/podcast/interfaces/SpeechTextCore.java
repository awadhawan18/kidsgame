package com.curieo.podcast.interfaces;


public interface SpeechTextCore {
    void promptSpeechInput();
    void showToast(CharSequence toastMessage, int toastDuration);
    void checkRecord();
}
