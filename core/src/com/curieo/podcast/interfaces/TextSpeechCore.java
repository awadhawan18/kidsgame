package com.curieo.podcast.interfaces;


public interface TextSpeechCore {

    void speak(String text);
    void pressBack();
    void changeOrientation();

    int getWins();

    void writeWins(int wins);

}
